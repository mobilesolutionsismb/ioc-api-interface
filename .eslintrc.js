module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    jest: true
  },
  parser: 'babel-eslint',
  extends: 'eslint:recommended',
  installedESLint: true,
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    sourceType: 'module'
  },
  globals: {
    Windows: true,
    cordova: true,
    //Global Config
    TITLE: true,
    VERSION: true,
    PKG_NAME: true,
    DESCRIPTION: true,
    ENVIRONMENT: true,
    BUILD_DATE: true,
    SUPPORTED_LANGUAGES: true,
    IREACT_BACKEND: true,
    SOCIAL_BACKEND: true
  },
  plugins: ['jest'],
  rules: {
    'no-unused-vars': 'warn',
    'no-console': 'off',
    'no-fallthrough': 'off',
    'no-inner-declarations': 'off',
    indent: ['off', 2],
    'linebreak-style': ['off'],
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true
      }
    ],
    semi: ['error', 'always']
  }
};
