import wellknown from 'wellknown';
import turfBuffer from '@turf/buffer';
import transformScale from '@turf/transform-scale';
import bbox from '@turf/bbox';
import bboxPolygon from '@turf/bbox-polygon';
// import booleanContains from '@turf/boolean-contains';
import envelope from '@turf/envelope';
import { point } from '@turf/helpers';
import { getType, getCoords } from '@turf/invariant'; // When upgrading to invariant@5.0.0 and boolean-contains@5.0.0 altogether
// import { LngLat, LngLatBounds } from 'mapbox-gl'; // avoid importing mapbox which creates a mess

export const EUROPE_BBOX_WKT =
  'POLYGON ((-26.19140625 33.87041555094183, 42.01171875 33.87041555094183, 42.01171875 71.52490903732816, -26.19140625 71.52490903732816, -26.19140625 33.87041555094183))';
// const MAX_VALID_BOUNDS = wellknown.parse(EUROPE_BBOX_WKT);
const DEFAULT_RADIUS = 50; //km

/**
 * Checks and if necessary convert a point
 * Supported formats are WKT string, array, mapboxgl.LngLat, GeoJSON Point feature
 * @param {Array|mapboxgl.LngLat|Feature} pointFeatureLike - a point feature
 * @returns Feature
 */
function _validatePoint(pointFeatureLike) {
  let pos = null;
  try {
    // WKT String
    if (typeof pointFeatureLike === 'string') {
      pos = wellknown.parse(pointFeatureLike);
    } else if (Array.isArray(pointFeatureLike) && pointFeatureLike.length === 2) {
      // Array [longitude, latitude]
      pos = point(pointFeatureLike);
    } else if (typeof pointFeatureLike.toArray === 'function') {
      // is a mapbox.LngLat (e.g. from map.getCenter() or related mapbox api)
      //(pointFeatureLike instanceof LngLat) {
      pos = point(pointFeatureLike.toArray());
    } else if (
      typeof pointFeatureLike.coords !== 'undefined' &&
      typeof pointFeatureLike.coords.latitude === 'number' &&
      typeof pointFeatureLike.coords.longitude === 'number'
    ) {
      // Geolocation position, from navigator.getCurrentPosition() and related api
      pos = point([pointFeatureLike.coords.longitude, pointFeatureLike.coords.latitude]);
    } else {
      // Assume it's a GeoJSON Point already - TODO add more robust validation against concave bbox and other glitches
      pos = pointFeatureLike;
    }
  } catch (err) {
    // throw 3rd party libraries errors
    throw err;
  }
  if (getType(pos) !== 'Point') {
    // throw validation failure
    throw new Error('User Position must be a point');
  }
  return pos;
}

/**
 * Returns true if the Feature is a rectangular Polygon
 * @param {Feature} feature - GeoJSON Feature
 * @return boolean
 */
function _isRectangularBBox(feature) {
  try {
    const isPolygon = getType(feature) === 'Polygon';
    return isPolygon && getCoords(feature)[0].length === 5;
  } catch (err) {
    return false;
  }
}

/**
 * Return a formatted bounding box in all used formats
 * boundsObject: {
 *   feature: GeoJSON Polygon Feature of the bounding box (used by mapbox)
 *   wktString: string (WKT, used for requests)
 *   bounds: Array (used for redux state management)
 * }
 * @param {String|Array, Feature, String, LatLngBounds} bounds - bounding box
 * @return {Object} boundsObject
 */
export function makeBoundsObject(boundsFeatureLike) {
  let feature = _validateBBox(boundsFeatureLike);
  // feature = booleanContains(MAX_VALID_BOUNDS, feature) ? feature : MAX_VALID_BOUNDS; // don't allow areas greather than/outside this
  const wktString = wellknown.stringify(feature);
  const bounds = bbox(feature);
  return {
    feature, // for mapbox layers or turf functions
    wktString, // for usage with server-side api
    bounds // for mapbox map bounds [West, South, East, North]
  };
}

/**
 * Checks and if necessary convert a point
 * Supported formats are WKT string, array, mapboxgl.LngLatBounds, GeoJSON Polygon feature
 * @param {String|Array|mapboxgl.LngLatBounds|Feature} boundsFeatureLike - a bounds like feature (representing a box)
 * @returns Feature
 */
function _validateBBox(boundsFeatureLike) {
  let feature = null;
  try {
    // assume it's a wkt string
    if (typeof boundsFeatureLike === 'string') {
      feature = wellknown.parse(boundsFeatureLike);
    } else if (Array.isArray(boundsFeatureLike) && boundsFeatureLike.length === 4) {
      // assume it's 4 number array [West, South, East, North]
      // TODO ? implement a case with [[W,S], [EN]] ? bbox 2 LngLatBoundsLike => [[bbox[0], bbox[1]],[bbox[2], bbox[3]]]
      feature = bboxPolygon(boundsFeatureLike);
      // } else if (boundsFeatureLike instanceof LngLatBounds) {
    } else if (
      typeof boundsFeatureLike.getWest === 'function' &&
      typeof boundsFeatureLike.getSouth === 'function' &&
      typeof boundsFeatureLike.getEast === 'function' &&
      typeof boundsFeatureLike.getNorth === 'function'
    ) {
      // is a mapbox.LngLatBounds (e.g. from map.getBounds() or related mapbox api call)
      feature = bboxPolygon([
        boundsFeatureLike.getWest(),
        boundsFeatureLike.getSouth(),
        boundsFeatureLike.getEast(),
        boundsFeatureLike.getNorth()
      ]);
    } else {
      // hope is a GeoJSON and compute it's envelope (whatever polygon it is)
      feature = boundsFeatureLike;
    }
    feature = envelope(feature); // Let's make it a square
  } catch (err) {
    // throw 3rd party libraries errors
    throw err;
  }
  if (!_isRectangularBBox(feature)) {
    // throw validation failure
    throw new Error(
      'bounds must be a Polygon, or a valid mapbox.LngLatBounds object or a valid array representing a bounding box'
    );
  }

  return feature;
}

/**
 * Compute the bounding box of a circle of radius km around pointFeatureLike
 * @param {String|Array|GeoJSON Point Feature} pointFeatureLike - must contain longitude and latitude
 * @param {Number} radius - radius in Km of a circle around pointFeatureLike
 * @param {String} units - 'kilometres' or 'miles' (@see turf js doc)
 * @return {Object} containing the bounding box as Polygon GeoJSON Feature of the bounding box, WKT POLYGON string and Array
 */
export function getFeatureAreaOfInterestFromPointPosition(
  pointFeatureLike,
  radius = DEFAULT_RADIUS,
  units = 'kilometres'
) {
  const _pos = _validatePoint(pointFeatureLike);
  const _units = units === 'km' ? 'kilometres' : units;
  const bboxFeature = envelope(turfBuffer(_pos, radius, { units: _units }));
  return makeBoundsObject(bboxFeature);
}

/**
 * Pad a bounding box in metric unit
 * @param {String|Array, Feature, String, LatLngBounds} bounds - bounding box
 * @param {Number} amount
 * @return {Object} containing the bounding box as Polygon GeoJSON Feature of the bounding box, WKT POLYGON string and Array
 */
function _padBoundingBoxMetric(bounds, amount, units) {
  const _bounds = _validateBBox(bounds);
  const bboxFeature = envelope(turfBuffer(_bounds, amount, { units }));
  return makeBoundsObject(bboxFeature);
}

/**
 *
 * @param {Array, Feature, String, LatLngBounds} bounds - bounding box
 * @param {Number} amount
 * @return {Object} containing the bounding box as Polygon GeoJSON Feature of the bounding box, WKT POLYGON string and Array
 */
function _padBoundingBoxInPercent(bounds, amount, measureUnits) {
  const feature = _validateBBox(bounds);
  const scaleFactor = measureUnits === '%' ? amount / 100 : amount;
  const bboxFeature = transformScale(feature, scaleFactor);
  return makeBoundsObject(bboxFeature);
}

/**
 * Pad bunding box by a given amount of km or by % of its area
 * @param {Array, Feature, String, LatLngBounds} bounds - bounding box
 * @param {Number} amount
 * @param {String} measureUnits - 'km', '%', null or the others supported by @turf/buffer. Null means we are passing a scale factor
 * @return {Object} containing the bounding box as Polygon GeoJSON Feature of the bounding box, WKT POLYGON string and Array
 */
export function padBoundingBox(bounds, amount = 1, measureUnits = null) {
  if (typeof amount !== 'number' || amount <= 0) {
    throw new Error('Padding must be positive number > 0');
  }
  if (measureUnits === '%' || amount === null) {
    return _padBoundingBoxInPercent(bounds, amount, measureUnits);
  } else {
    const _units = measureUnits.toLowerCase() === 'km' ? 'kilometres' : measureUnits;
    return _padBoundingBoxMetric(bounds, amount, _units);
  }
}
