import moment from 'moment-timezone';
const tz = moment.tz.guess();
import isPojo from 'is-pojo';

export const DEFAULT_TIME_WINDOW = {
    quantity: 1,
    unit: 'months'
};
export const DEFAULT_PADDING_IN_FUTURE = {
    quantity: 2,
    unit: 'weeks'
};

// Pad event TW in the past and in the future
export const DEFAULT_EVENT_PADDING = {
    quantity: 10,
    unit: 'days'
};

export const VALID_TIME_UNITS = ['years', 'months', 'weeks', 'days'];

/**
 * Return the date formatted in the selected locale
 * @param {Date} date the date to be formatted
 * @param {String} locale - string, eg, 'en'
 * @param {String} format - date formatting option, default to 'lll' (see https://momentjs.com/docs/)
 * @return {String} formatted date
 */
export function localizeDate(date, locale, format = 'lll') {
    if (locale && moment.locale() !== locale) {
        moment.locale(locale);
    }
    return moment(date).tz(tz).format(format);
}

/**
 * Returns a new date (as moment object) according to the params,
 * in the current time zone.
 * If no params is supplied, then the current date is returned
 * @param {Date|String|undefined} date 
 * @param {Number} diff difference in 'unit' either in the past or in the future (positive)
 * @param {String} unit 'seconds', 'minutes', 'hours', 'days', ... (see https://momentjs.com/docs/)
 * @return {moment} the date
 */
export function getLocalDate(date, diff, unit = 'days') {
    let m = moment((date instanceof Date || typeof date === 'string') ? date : new Date()).tz(tz);
    if (typeof diff === 'number') {
        m = diff > 0 ? m.add(diff, unit) : m.subtract(-diff, unit);
    }
    return m;
}

/**
 * Check if extent is valid (positive quatity and allowed time unit)
 * @param {any} extent 
 */
function isValidExtent(extent) {
    return isPojo(extent) &&
        typeof extent.quantity === 'number' &&
        extent.quantity > 0 &&
        VALID_TIME_UNITS.indexOf(extent.unit) > -1;
}

export class TimeWindow {
    /**
     * 
     * @param {String|Date|moment} dateStart - date start
     * @param {String|Date|moment} dateEnd 
     * @param {Object} extentBackInTime - {quantity, unit}, quantity positive number, unit = 'hours', 'days', 'months'...
     * @param {Boolean} exact - if false (default) it will pad dateStart to 00:00:00:000 of dateStart day and dateEnd to 23:59:59:999 of dateEnd day
     * @param {Object} extentInFuture - {quantity, unit}, quantity positive number, unit = 'hours', 'days', 'months'... useful for padding the time window in the future
     */
    constructor(dateStart = null, dateEnd = null, extentBackInTime = DEFAULT_TIME_WINDOW, exact = false, extentInFuture = null) {
        const momentEnd = getLocalDate(dateEnd);
        let _dateEnd = exact === true ? momentEnd : momentEnd.hours(23).minutes(59).seconds(59).milliseconds(999);

        const hasDateStart = dateStart !== null;
        if (!hasDateStart && !isValidExtent(extentBackInTime)) {
            throw new Error(`Invalid extent: quantitiy ${extentInFuture.quantity}, unit ${extentInFuture.unit}`);
        }

        const momentStart = hasDateStart ? getLocalDate(dateStart, null) : getLocalDate(dateEnd, -extentBackInTime.quantity, extentBackInTime.unit);
        const _dateStart = exact === true ? momentStart : momentStart.hours(0).minutes(0).seconds(0).milliseconds(0);
        if (_dateEnd.diff(_dateStart) <= 0) {
            throw new Error('Invalid dates: dateStart >= dateEnd');
        }

        if (isValidExtent(extentInFuture)) {
            _dateEnd = getLocalDate(_dateEnd, extentInFuture.quantity, extentInFuture.unit);
        }

        Object.defineProperties(this, {
            dateStart: {
                get: () => _dateStart.toDate().toISOString(),
                enumerable: true,
                configurable: true
            },
            dateEnd: {
                get: () => _dateEnd.toDate().toISOString(),
                enumerable: true,
                configurable: true
            }
        });
    }

    toPojo() {
        return {
            dateStart: this.dateStart,
            dateEnd: this.dateEnd
        };
    }

    static fromEvent(event, padding = DEFAULT_EVENT_PADDING) {
        // PAD TW as declared in padding (default 10 days)
        const { quantity, unit } = padding;
        const start = typeof event.properties === 'undefined' ? event.start : event.properties.start;
        const end = typeof event.properties === 'undefined' ? event.end : event.properties.end;
        const eventStart = moment(start).subtract(quantity, unit).toDate();
        const eventEnd = moment(end).add(quantity, unit).toDate();
        return new TimeWindow(eventStart, eventEnd);
    }
}