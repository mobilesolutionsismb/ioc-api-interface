import axios from 'axios';
import {/* APIError, */
  apiFailure
} from './APIError';
import {getBackendURL, getSocialBackendURL, getPresets} from './getBackendURL';
import qs from 'qs';
import * as modules from './modules';
import localForage from 'localforage';
import {logMain, logWarning} from '../utils/log';
import WorkerWrapper from './worker/workerWrapper';

const workerHandlersMap = {
  ready: () => {
    logMain('Worker successfully initialized');
  },
  tokenClear: () => {
    logMain('Token cleared');
  },
  tokenSet: () => {
    logMain('Token updated');
  },
  cultureSet: () => {
    logMain('Culture updated');
  }
  // 'featuresLoaded': result => { logMain(`${result.itemType}s updated`, result);
  // }
};

export const API_INTERNAL_STORAGE_NAME = 'api-inner-storage';
export const API_DATA_KEY = '_api-data';

const apiInternalStore = localForage.createInstance({name: API_INTERNAL_STORAGE_NAME, storeName: API_INTERNAL_STORAGE_NAME, description: 'storage for api internal data such as tokens'});

const DEFAULT_HEADERS = {
  Accept: 'application/json'
};

// For each module, dive into its functions and generate a method of
// api.moduleName.methodName
function _mapModuleToApi(moduleObject) {
  return Object
    .keys(moduleObject)
    .filter(moduleName => moduleName !== 'default')
    .map(moduleFunctionName => {
      const moduleFunction = moduleObject[moduleFunctionName];
      return {
        [moduleFunctionName]: (...args) => {
          let failureMessage = undefined;
          let params = args;
          // last param should always be failureMessage The Function.length is the number
          // of params expected by a function !! Caution: if using default params, only
          // the one before the first default arguments are taken into count for
          // Function.length If the args supplied are more than the expected, the last is
          // assumed to be the failure message @see
          // https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects
          // / Function/length
          if (args.length - moduleFunction.length > 0) {
            failureMessage = args.splice(args.length - 1, 1)[0];
            params = args;
          }
          return this
            ._axios
            .request(moduleFunction.apply(this, params))
            .then(response => response.data)
            .catch(error => {
              return Promise.reject(apiFailure(error, failureMessage));
            });
        }
      };
    })
    .reduce((module, method) => {
      module = {
        ...module,
        ...method
      };
      return module;
    }, {});
}

/**
 * Check if the user has some given permission with permissionName
 * For fine grained permission checking
 * @param {Object} user - the user object
 * @param {String} permissionName - the single permission
 * @return true if the permission is found in grantedPermissions
 */
export function hasPermissions(user, permissionName) {
  // return Object.keys(user.grantedPermissions).findIndex(p => p ===
  // permissionName) > -1;
  return user
    .grantedPermissions
    .hasOwnProperty(permissionName);
}

/**
 * Map user permissions to user type string
 * @param {Object} user
 * @return String - 'authority' or 'citizen'
 */
export function getUserTypeString(roles) {
  if (Array.isArray(roles) && roles.length > 0) {
    // Warning: this is provisional until roles and permissions became more clear
    // TODO: check check user roles with backend and understand access permissions
    if (roles.indexOf('Pro') > -1 || roles.indexOf('Admin') > -1) {
      return 'authority';
    } else {
      return 'citizen';
    }
  } else {
    // should never happen
    throw new Error('Unknown user role', JSON.stringify(roles));
  }
}
export class API {
  // For use with JEST/NODE
  static setAdapter(adapter) {
    axios.defaults.adapter = adapter;
  }

  static getInstance() {
    return api;
  }

  constructor(baseURL, socialURL, presetsFromEnv, culture = 'en', defaultHeaders = DEFAULT_HEADERS) {
    if (typeof api !== 'undefined') {
      throw new Error('API class is singleton. Use API.getInstance()');
    }

    let _baseURL = baseURL;
    let _socialBaseURL = socialURL;
    let _presets = presetsFromEnv;
    let _culture = culture;
    let _defaultHeaders = {
      ...defaultHeaders,
      'Accept-Language': _culture
    };

    let _rememberCredentials = false;
    // WARNING: STORING THE PASSWORD IS WRONG BUT WE HAVE TO WAIT FOR REFRESH TOKEN
    // SUPPORT
    let _last_password = null;
    let _token = null;
    let _profile = null; // redundant user profile
    let _enc_token = null;
    let _expire_in_seconds = null;
    let _last_time_logged_in = null;
    let _apiModules = modules;

    const onResumeError = err => {
      logWarning(`Cannot store ${API_DATA_KEY} data or no data stored`, err);
    };

    const setAuthenticationVariables = appData => {
      if (appData) {
        _token = appData._token;
        _profile = appData._profile;
        axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${_token}`;
        _enc_token = appData._enc_token;
        _expire_in_seconds = appData._expire_in_seconds;
        // WARNING: STORING THE PASSWORD IS WRONG BUT WE HAVE TO WAIT FOR REFRESH TOKEN
        // SUPPORT
        _last_password = appData._last_password;
        _rememberCredentials = appData._rememberCredentials;
        _last_time_logged_in = appData._last_time_logged_in;
        _profile = appData._profile;
      }
      return appData;
    };

    // TODO fix this
    const axiosInstance = axios.create({
      adapter: axios.defaults.adapter,
      baseURL: _baseURL,
      headers: {
        common: _defaultHeaders
      },
      // `paramsSerializer` is an optional function in charge of serializing `params`
      // (e.g. https://www.npmjs.com/package/qs, http://api.jquery.com/jquery.param/)
      paramsSerializer: params => qs.stringify(params, {arrayFormat: 'repeat'})
    });
    Object.defineProperties(this, {
      rememberCredentials: {
        get: () => _rememberCredentials,
        set: v => (_rememberCredentials = v === true),
        enumerable: true
      },
      baseURL: {
        get: () => _baseURL,
        enumerable: true,
        configurable: true
      },
      socialURL: {
        get: () => _socialBaseURL,
        enumerable: true,
        configurable: true
      },
      defaultHeaders: {
        enumerable: true,
        get: () => axiosInstance.defaults.headers.common,
        set: headers => {
          axiosInstance.defaults.headers.common = {
            ...axiosInstance.defaults.headers.common,
            ...headers
          };
        }
      },
      culture: {
        enumerable: true,
        get: () => _culture,
        set: newCulture => {
          if (typeof newCulture === 'string' && newCulture.length === 2 && _culture !== newCulture) {
            _culture = newCulture;
            axiosInstance.defaults.headers.common['Accept-Language'] = _culture;
            if (this._workerWrapper) {
              this
                ._workerWrapper
                .send('setCulture', {culture: _culture});
            }
          }
        }
      },
      presets: {
        get: () => _presets,
        enumerable: true,
        configurable: true
      },
      userProfile: {
        get: () => _profile,
        enumerable: true,
        configurable: true
      },
      token: {
        get: () => _token,
        enumerable: true,
        configurable: true
      },
      encryptedAccessToken: {
        get: () => _enc_token,
        enumerable: true,
        configurable: true
      },
      lastTimeLoggedIn: {
        get: () => (_last_time_logged_in !== null
          ? new Date(_last_time_logged_in)
          : null),
        enumerable: true,
        configurable: true
      },
      expireInSeconds: {
        get: () => _expire_in_seconds,
        enumerable: true,
        configurable: true
      },
      moduleVersion: {
        get: () => ({version: VERSION, pkgName: PKG_NAME, description: DESCRIPTION, environment: ENVIRONMENT, buildDate: BUILD_DATE}),
        enumerable: true,
        configurable: false
      },
      // Private read only
      _modules: {
        value: _apiModules,
        enumerable: false,
        configurable: false,
        writable: false
      },
      // Private read only
      _axios: {
        value: axiosInstance,
        enumerable: false,
        configurable: false,
        writable: false
      }
    });

    // TODO store these info as encrypted
    // https://stackoverflow.com/questions/18279141/javascript-string-encryption-and
    // - decryption
    const _serializeTokens = (accessToken, encryptedAccessToken, expireInSeconds, lastPassword, rememberCredentials, lastTimeLoggedIn) => {
      /* const appData =  */
      setAuthenticationVariables({
        _token: accessToken, _enc_token: encryptedAccessToken, _expire_in_seconds: expireInSeconds,
        // WARNING: STORING THE PASSWORD IS WRONG BUT WE HAVE TO WAIT FOR REFRESH TOKEN
        // SUPPORT
        _last_password: lastPassword,
        _rememberCredentials: rememberCredentials,
        _last_time_logged_in: lastTimeLoggedIn
      });
      // this._workerWrapper.send('setToken', { token: appData ? appData._token : null
      // });
      return apiInternalStore.setItem(API_DATA_KEY, {
        _token, _enc_token, _expire_in_seconds,
        // WARNING: STORING THE PASSWORD IS WRONG BUT WE HAVE TO WAIT FOR REFRESH TOKEN
        // SUPPORT
        _last_password,
        _rememberCredentials,
        _last_time_logged_in
        // _profile
      });
    };

    const _serializeUserProfile = async profile => {
      const data = await apiInternalStore.getItem(API_DATA_KEY);
      await apiInternalStore.setItem(API_DATA_KEY, {
        ...data,
        _profile: profile
      });
      _profile = profile;
      return;
    };

    const _clearTokens = () => {
      return apiInternalStore.removeItem(API_DATA_KEY);
    };

    // For each module, dive into its functions and generate a method of
    // api.moduleName.methodName
    for (let moduleName of Object.keys(modules).filter(moduleName => moduleName !== 'default')) {
      if (!this.moduleName) {
        this[moduleName] = moduleName;
      }
      const module = modules[moduleName];
      this[moduleName] = _mapModuleToApi.call(this, module);
    }
    // apiInstances.push(this);

    apiInternalStore
      .getItem(API_DATA_KEY)
      .then(setAuthenticationVariables)
      .then(appData => {
        // logMain('Read App Data from storage', appData);

        this._workerWrapper = new WorkerWrapper({
          baseURL,
          socialURL,
          clientPresets: presetsFromEnv,
          culture: _culture,
          token: appData
            ? appData._token
            : null,
          user: appData
            ? appData._profile
            : null,
          handlersMap: workerHandlersMap
        });
      })
      .catch(onResumeError);

    // private
    const _authenticate = async(email, password, failureMessage) => {
      try {
        const response = await this
          ._axios
          .post('/api/TokenAuth/Authenticate', {
            usernameOrEmailAddress: email,
            password
          });
        if (_rememberCredentials === true) {
          // save password
          _last_password = password;
        }
        const loginData = response.data.result;
        const {accessToken, userId, expireInSeconds, encryptedAccessToken} = loginData;
        _last_time_logged_in = Date.now();
        await _serializeTokens(accessToken, encryptedAccessToken, expireInSeconds, _last_password, _rememberCredentials, _last_time_logged_in);
        return userId;
      } catch (exception) {
        throw apiFailure(exception, failureMessage);
      }
    };

    // Special case - needs 2 calls and post processes data
    this.login = async(email, password, failureMessage = '_login_failure') => {
      try {
        const _password = password
          ? password
          : _rememberCredentials && _last_password
            ? _last_password
            : '';
        const id = await _authenticate(email, _password, failureMessage);
        const userProfileResponse = await this
          ._axios
          .get('/api/services/app/Session/GetCurrentLoginInformation');
        const {
          user,
          auth,
          roles,
          organizationUnits,
          services,
          teams,
          ...rest
        } = userProfileResponse.data.result;
        if (!user) {
          this.logout();
          throw apiFailure({
            response: {
              status: 500,
              statusText: 'The server did not return user information',
              data: {}
            }
          }, 'unknown error');
        }
        const grantedPermissions = auth.grantedPermissions;
        const userType = getUserTypeString(roles);
        const rolesForValidation = ['OrganizationResponsible', 'DecisionMaker', 'TeamLeader'];
        const canValidateReport = roles && roles
          .filter(value => -1 !== rolesForValidation.indexOf(value))
          .length > 0;
        const profile = {
          userType,
          ...user,
          profilePictureId: typeof user.profilePictureId === 'number'
            ? user.profilePictureId
            : 0,
          id,
          roles,
          services,
          teams,
          organizationUnits,
          grantedPermissions,
          canValidateReport,
          other: {
            ...rest,
            allPermissions: auth.allPermissions
          }
        };
        await _serializeUserProfile(profile);
        this
          ._workerWrapper
          .send('setToken', {
            token: _token,
            user: profile
          });
        return profile;
      } catch (exception) {
        throw apiFailure(exception, failureMessage);
      }
    };

    // Special case - clear token
    this.logout = () => {
      _token = null;
      this
        ._workerWrapper
        .send('clearToken');
      delete axiosInstance.defaults.headers.common['Authorization'];
      _enc_token = null;
      _expire_in_seconds = null;
      _profile = null;
      _last_password = null;
      _last_time_logged_in = null;
      const p = new Promise(resolve => setTimeout(resolve(null), 500));
      p.then(_clearTokens);
    };

    this.itemTypesUpdateHandlers = {};

    workerHandlersMap['featuresLoaded'] = result => {
      // Result: {itemType: string, data: object| null, error: Error|null}
      // result.data: {totalFeaturesCount: number, newFeaturesCount: number,
      // lastModificationTime: date, featuresURL: url} || null
      logMain(`${result.itemType}s updated`, result);
      const itemTypeHandler = this.itemTypesUpdateHandlers[result.itemType];
      if (typeof itemTypeHandler === 'function') {
        itemTypeHandler(result.data, result.error);
      }
    };

    this._checkExpired = () => {
      if (_expire_in_seconds === null || _last_time_logged_in === null) {
        return true;
      }
      const tokenExpiryDate = new Date(_last_time_logged_in + 1000 * _expire_in_seconds);
      const isExpired = new Date() >= tokenExpiryDate;
      return isExpired;
    };

    // Send a loadFeatures request message to the api worker for given itemType(s)
    // if lastModificationTime is null then is a full reload
    this.requestIreactFeaturesUpdate = async(attemptRelogin, itemType, dateStart, dateEnd, areaOfInterest, lastModificationTime, ...rest) => {
      const isExpired = this._checkExpired();
      const doRequest = () => {
        const request = {
          itemTypes: Array.isArray(itemType)
            ? itemType
            : [itemType],
          dateStart,
          dateEnd,
          areaOfInterest,
          lastModificationTime: lastModificationTime || null,
          forceReload: lastModificationTime === null
            ? true
            : false,
          ...rest
        };
        this
          ._workerWrapper
          .send('loadFeatures', request);
      };

      if (isExpired) {
        if (typeof attemptRelogin === 'function') {
          const reloginSuccess = await attemptRelogin(this.reduxStore.dispatch, this.reduxStore.getState);
          if (reloginSuccess) {
            doRequest();
          } else {
            throw apiFailure(new Error('Login Token Expired'));
          }
        } else {
          throw apiFailure(new Error('Login Token Expired'));
        }
      } else {
        doRequest();
      }
    };

    //
    this.registerHandlerForItemType = (itemType, handler) => {
      this.itemTypesUpdateHandlers[itemType] = handler;
    };

    this.reduxStore = {
      // Fake store, the true reference must be passed by the app
      getState: () => ({}),
      dispatch: () => {}
    };

    this.clearInnerStorage = () => {
      return apiInternalStore.clear();
    };
  }
}

const api = new API(getBackendURL(), getSocialBackendURL(), getPresets());

export default API;
