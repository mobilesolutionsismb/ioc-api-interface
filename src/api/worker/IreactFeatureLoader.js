import axios from 'axios';
import { apiFailure } from '../APIError';
import qs from 'qs';
import { logWorker } from '../../utils/log';
import { makeDataURL, clearDataURL } from '../../utils/dataURL';
import { mergeData } from './featurePostProcessing/_mergeData';
import localForage from 'localforage';
import { getAll as reportsGetList } from '../modules/report';
import { getAllEmergencyCommunications as emcommsGetList } from '../modules/emergencyCommunication';
import { getAll as missionsGetList } from '../modules/mission';
import { getAll as mapRequestsGetList } from '../modules/mapRequest';
import { getInformation } from '../modules/agentLocation';
import { getTweets } from '../socialapi_modules/tweets';

import {
  sanitizeReports,
  removeReportsApp,
  removeReportsFE,
  getReportsFromAxiosResponse
} from './featurePostProcessing/reports';
import {
  sanitizeEmergencyCommunications,
  getEmergencyCommunicationsFromAxiosResponse,
  removeEmcommsFE,
  removeEmcommsApp
} from './featurePostProcessing/emergencyCommunications';
import {
  sanitizeMissions,
  getMissionsFromAxiosResponse,
  mergeMissions
} from './featurePostProcessing/missions';

import {
  sanitizeMapRequests,
  getMapRequestsFromAxiosResponse
} from './featurePostProcessing/mapRequests';

import {
  sanitizeAgentLocations,
  getAgentLocationsFromAxiosResponse
} from './featurePostProcessing/agentLocations';

import { sanitizeTweets, getTweetsFromAxiosResponse } from './featurePostProcessing/tweets';

/**
 * @param {Mission} m mission
 * filter overall completed missions, regardles of other params
 */
const completedMissionsFilter = m => m.properties.isCompleted;

/**
 * @param {Array<number>} thisUserTeamIds - user team ids
 * @param {number} thisUserId - user id
 * @param {Mission} m mission
 * filter missions belonging to this user or this user's team
 */
const thisUserTeamMissionsFilter = (thisUserTeamIds, thisUserId, m) =>
  typeof m.properties.teamId === 'number'
    ? thisUserTeamIds.indexOf(m.properties.teamId) > -1
    : typeof m.properties.firstResponderId === 'number'
    ? m.properties.firstResponderId === thisUserId
    : false;

/**
 * @param {Array<number>}thisUserTeamIds - user team ids
 * @param {number} thisUserId - user id
 * @param {Mission} m mission
 * filter missions belonging to this user or this user's team which are completed
 */
const thisUserTeamMissionsCompletedFilter = (thisUserTeamIds, thisUserId, m) =>
  m.properties.isCompleted && thisUserTeamMissionsFilter(thisUserTeamIds, thisUserId, m);

function _findRROngoing(feature) {
  return feature && feature.properties.itemType === 'reportRequest' && feature.properties.isOngoing;
}

const FEATURES_STORAGE_NAME = 'ireact-features';

const featureStore = localForage.createInstance({
  name: FEATURES_STORAGE_NAME,
  storeName: FEATURES_STORAGE_NAME,
  description: 'storage for ireact GeoJSON features'
});

const INITIAL_DATA_URLS = {
  report: null,
  mission: null,
  missionTask: null,
  emergencyCommunication: null,
  // reportRequest: null,
  mapRequest: null,
  agentLocation: null,
  // SOCIAL
  tweet: null
};

/*
    Each loader must accept in this order:
    dateStart: Date,
    dateEnd: Date, 
    areaOfInterest: string,
    lastModificationTime: : Date, 
    culture: string,
    optional arguments: presets: Object, otherArguments: Object
 */
const DATA_LOADERS_API_MAP = {
  report: reportsGetList,
  emergencyCommunication: emcommsGetList,
  // reportRequest: emcommsGetList, //TODO check
  mission: missionsGetList,
  // missionTask: null,
  mapRequest: mapRequestsGetList,
  agentLocation: getInformation,
  tweet: getTweets
};

const SANITIZERS_MAP = {
  report: sanitizeReports,
  emergencyCommunication: sanitizeEmergencyCommunications,
  // reportRequest: null,
  mission: sanitizeMissions,
  // missionTask: null,
  mapRequest: sanitizeMapRequests,
  agentLocation: sanitizeAgentLocations,
  // SOCIAL
  tweet: sanitizeTweets
};

const RESPONSE_PARSERS = {
  report: getReportsFromAxiosResponse,
  emergencyCommunication: getEmergencyCommunicationsFromAxiosResponse,
  // reportRequest: getEmergencyCommunicationsFromAxiosResponse,
  mission: getMissionsFromAxiosResponse,
  missionTask: null,
  mapRequest: getMapRequestsFromAxiosResponse,
  agentLocation: getAgentLocationsFromAxiosResponse,
  // SOCIAL
  tweet: getTweetsFromAxiosResponse
};

const notNull = it => it !== null;

class IreactFeatureLoader {
  constructor(baseURL, socialURL, clientPresets, culture = 'en', token = null, user = null) {
    this._baseURL = baseURL;
    this._socialURL = socialURL;
    this._token = token;
    this._user = user;
    this._culture = culture;
    this._clientPresets = clientPresets;

    this.REMOVERS_MAP = {
      report: clientPresets === 'frontend' ? removeReportsFE : removeReportsApp,
      emergencyCommunication: clientPresets === 'frontend' ? removeEmcommsFE : removeEmcommsApp,
      mission: undefined // undefined means DEFAULT REMOVER IS OK
      // missionTask: null,
      // reportRequest: null,
      // mapRequest: null,
      // agentLocation: null
    };

    this.LOADERS_PRESETS = {
      report: { Status: clientPresets === 'frontend' ? 'all' : 'submitted,validated' },
      emergencyCommunication: { [clientPresets]: true },
      mission: {},
      mapRequest: {},
      agentLocation: {}
    };

    // AXIOS INSTANCES
    this.ireactBEInstance = axios.create({
      adapter: axios.defaults.adapter,
      baseURL: this._baseURL,
      headers: {
        common: {
          Accept: 'application/json',
          'Accept-Language': this._culture,
          Authorization: this._token ? `Bearer ${this._token}` : undefined
        }
      },
      paramsSerializer: params =>
        qs.stringify(params, {
          arrayFormat: 'repeat'
        })
    });

    this.sociaInstance = axios.create({
      adapter: axios.defaults.adapter,
      baseURL: this._socialURL,
      headers: {
        common: {
          Accept: 'application/json'
        }
      },
      paramsSerializer: params =>
        qs.stringify(params, {
          arrayFormat: 'repeat'
        })
    });

    /**
     *
     * @param {Object} params - params for the loader
     * @param {Function} onItemTypeLoaded - handler that will be called on each successful update
     * @param {boolean} forceReload - if true wil not send lastModificationTime
     */
    this.loadFeatures = async (params, onItemTypeLoaded, forceReload = false) => {
      try {
        const {
          itemTypes,
          dateStart,
          dateEnd,
          areaOfInterest,
          lastModificationTime,
          ...rest
        } = params;
        const _lmt = forceReload === true ? null : lastModificationTime;
        const _apiSpecificPresets = forceReload === true ? this.LOADERS_PRESETS[itemTypes] : {};
        const loaders = itemTypes
          .map(async itemType => {
            const loader = DATA_LOADERS_API_MAP[itemType];
            if (typeof loader === 'function') {
              let config;
              const args = Object.values(rest);
              const aoi = itemType === 'tweet' ? areaOfInterest.bounds : areaOfInterest.wktString;
              const loaderArguments = [dateStart, dateEnd, aoi, _lmt];
              config = loader.apply(
                this,
                itemType === 'tweet'
                  ? [...loaderArguments, ...args]
                  : [...loaderArguments, this._culture, _apiSpecificPresets, ...args]
              );
              const updateResult = await this._loadFeaturesInner(
                itemType,
                config,
                forceReload,
                areaOfInterest
              );
              onItemTypeLoaded(updateResult);
              return updateResult;
            } else {
              return null;
            }
          })
          .filter(notNull);
        return await Promise.all(loaders);
      } catch (e) {
        return { error: apiFailure(e) };
      }
    };

    /**
     * Actually does the request for each itemType using the appropriate config
     * @param {string} itemType - itemType
     * @param {Object} config - axios api config object
     * @param {boolean} forceReload - if true wil not merge items, but overwrite
     * @param {AOI} areaOfInterest - Area Of Interest, needed only for tweets
     * @return {Object} loadResult {itemType: string, data: object| null, error: Error|null}
     * The data Object will have {totalFeaturesCount: number, newFeaturesCount: number, lastModificationTime: date, featuresURL: url}
     */
    this._loadFeaturesInner = async (itemType, config, forceReload, areaOfInterest) => {
      try {
        const apiResponseParser = RESPONSE_PARSERS[itemType];
        if (typeof apiResponseParser !== 'function') {
          return { itemType, data: null, error: new Error(`Invalid parser for ${itemType}`) };
        }
        // TODO sanitize incoming, get current from localForage,  store merged and then return
        const apiInstance = itemType === 'tweet' ? this.sociaInstance : this.ireactBEInstance;
        const response = await apiInstance.request(config);
        const itemsFromServer = apiResponseParser(response);
        const data = await this._sanitize(
          itemType,
          itemsFromServer,
          forceReload,
          areaOfInterest.feature
        );
        return { itemType, data, error: null };
      } catch (e) {
        return { itemType, data: null, error: apiFailure(e) };
      }
    };

    // TODO (AS soon as it's finished)
    // Check if it's better to use blob urls or localForage directly
    this.dataURLS = {
      ...INITIAL_DATA_URLS
    };

    this.init = async onItemTypeLoaded => {
      if (this._token === null) {
        this.clearAll(onItemTypeLoaded);
      } else {
        const itemTypes = Object.keys(this.dataURLS).filter(it => it !== 'missionTask');
        for (const itemType of itemTypes) {
          if (itemType === 'mission') {
            logWorker(`Loading initial features ${itemType} and missionTasks`);
          } else {
            logWorker(`Loading initial features ${itemType}`);
          }
          const storedItems = await featureStore.getItem(itemType);
          if (storedItems) {
            let lastModificationTime = await featureStore.getItem(
              `${itemType}-lastModificationDate`
            );
            this.dataURLS[itemType] = makeDataURL(this.dataURLS[itemType], storedItems);
            logWorker(`${itemType} features loaded`);
            const result = {
              itemType,
              data: {
                totalFeaturesCount: storedItems.length,
                newFeaturesCount: 0,
                lastModificationTime: lastModificationTime || new Date(0),
                featuresURL: this.dataURLS[itemType]
              },
              error: null
            };
            if (itemType === 'mission' || itemType === 'missionTask') {
              let storedMissionTaskItems = await featureStore.getItem('missionTask');
              storedMissionTaskItems = storedMissionTaskItems || {
                type: 'FeatureCollection',
                features: []
              };
              this.dataURLS['missionTask'] = makeDataURL(
                this.dataURLS['missionTask'],
                storedMissionTaskItems
              );
              result.data['missionTaskFeaturesURL'] = this.dataURLS['missionTask'];
              result.data['completedMissionsCount'] = storedItems.filter(
                completedMissionsFilter
              ).length;
              const thisUserTeamIds =
                this._user && Array.isArray(this._user.teams)
                  ? this._user.teams.map(t => t.id)
                  : [];
              const thisUserId = this._user ? this._user.id : null;
              result.data['thisUserTeamMissionsCount'] = storedItems.filter(
                thisUserTeamMissionsFilter.bind(this, thisUserTeamIds, thisUserId)
              ).length;
              result.data['thisUserTeamCompletedMissionsCount'] = storedItems.filter(
                thisUserTeamMissionsCompletedFilter.bind(this, thisUserTeamIds, thisUserId)
              ).length;
            }
            // Stats relevant only for report request
            if (itemType === 'emergencyCommunication') {
              const lastReportRequest = storedItems.find(_findRROngoing);
              const lastActiveReportRequestID = lastReportRequest
                ? lastReportRequest.properties.id
                : null;
              result.data['lastActiveReportRequestID'] = lastActiveReportRequestID;
            }

            onItemTypeLoaded(result);
          } else {
            logWorker(`No features stored for ${itemType}`);
          }
        }
      }
    };

    this.clearAll = async (doneFn = null) => {
      try {
        await featureStore.clear();
        logWorker('All stored features cleared');
        const itemTypes = Object.keys(this.dataURLS);
        for (const itemType of itemTypes) {
          this.dataURLS[itemType] = clearDataURL(this.dataURLS[itemType]);
          if (doneFn) {
            const result = {
              itemType,
              data: {
                totalFeaturesCount: 0,
                newFeaturesCount: 0,
                lastModificationTime: new Date(0),
                featuresURL: this.dataURLS[itemType]
              },
              error: null
            };
            if (itemType === 'mission' || itemType === 'missionTask') {
              result.data['missionTaskFeaturesURL'] = this.dataURLS['missionTask'];
              result.data['completedMissionsCount'] = 0;
              result.data['thisUserTeamMissionsCount'] = 0;
              result.data['thisUserTeamCompletedMissionsCount'] = 0;
            }
            if (itemType === 'emergencyCommunication') {
              result.data['lastActiveReportRequestID'] = null;
            }
            doneFn(result);
          }
        }
      } catch (e) {
        throw e;
      }
    };
  }

  async _sanitize(itemType, itemsFromServer, forceReload, areaOfInterest) {
    let newFeaturesCount = 0;
    let totalFeaturesCount = 0;
    // missions only stats
    let completedMissionsCount = 0;
    let thisUserTeamMissionsCount = 0;
    let thisUserTeamCompletedMissionsCount = 0;
    let thisUserTeamIds = [];
    let thisUserId = null;
    // reportRequestOnly stats
    let lastActiveReportRequestID = null;

    const sanitizer =
      itemType === 'tweet'
        ? SANITIZERS_MAP[itemType].bind(this, areaOfInterest)
        : SANITIZERS_MAP[itemType];
    let sanitizedItems = [];
    let missionTasks = [];
    if (itemType === 'mission') {
      thisUserTeamIds =
        this._user && Array.isArray(this._user.teams) ? this._user.teams.map(t => t.id) : [];
      thisUserId = this._user ? this._user.id : null;
      const results = itemsFromServer.map(sanitizer).filter(notNull);
      sanitizedItems = results.map(res => res.feature);
      missionTasks = results.reduce((tasks, next) => {
        tasks = [...tasks, ...next.tasks];
        return tasks;
      }, []);
    } else {
      sanitizedItems = itemsFromServer.map(sanitizer).filter(notNull);
      if (itemType === 'emergencyCommunication') {
        //assuming items are already sorted from source
        const lastReportRequest = sanitizedItems.find(_findRROngoing);
        lastActiveReportRequestID = lastReportRequest ? lastReportRequest.properties.id : null;
      }
    }

    newFeaturesCount = sanitizedItems.length;
    // Item are now GeoJSON feature with itemType set properly
    if (forceReload) {
      await featureStore.setItem(itemType, sanitizedItems);
      this.dataURLS[itemType] = makeDataURL(this.dataURLS[itemType], sanitizedItems);
      totalFeaturesCount = sanitizedItems.length;
      if (itemType === 'mission') {
        completedMissionsCount = sanitizedItems.filter(completedMissionsFilter).length;
        thisUserTeamMissionsCount = sanitizedItems.filter(
          thisUserTeamMissionsFilter.bind(this, thisUserTeamIds, thisUserId)
        ).length;
        thisUserTeamCompletedMissionsCount = sanitizedItems.filter(
          thisUserTeamMissionsCompletedFilter.bind(this, thisUserTeamIds, thisUserId)
        ).length;
        // let's add also tasks
        const missionTasksFC = {
          type: 'FeatureCollection',
          features: missionTasks
        };
        this.dataURLS['missionTask'] = makeDataURL(this.dataURLS['missionTask'], missionTasksFC);
        await featureStore.setItem('missionTask', missionTasksFC);
      }
    } else {
      let oldItems = await featureStore.getItem(itemType);
      let mergedItems;
      let mergedTasks;
      if (itemType === 'mission') {
        if (oldItems) {
          let oldMissionTasks = await featureStore.getItem('missionTask');
          if (!oldMissionTasks) {
            oldMissionTasks = {
              type: 'FeatureCollection',
              features: []
            };
          }
          const mergeResult = mergeMissions(
            {
              items: oldItems,
              tasks: oldMissionTasks.features
            },
            {
              items: sanitizedItems,
              tasks: missionTasks
            }
          );
          mergedItems = mergeResult.items;
          mergedTasks = mergeResult.tasks;
          const mergedMissionTasksFC = {
            type: 'FeatureCollection',
            features: mergedTasks
          };

          this.dataURLS['missionTask'] = makeDataURL(
            this.dataURLS['missionTask'],
            mergedMissionTasksFC
          );
          await featureStore.setItem('missionTask', mergedMissionTasksFC);
        } else {
          mergedItems = sanitizedItems;
        }
        completedMissionsCount = mergedItems.filter(completedMissionsFilter).length;
        thisUserTeamMissionsCount = mergedItems.filter(
          thisUserTeamMissionsFilter.bind(this, thisUserTeamIds, thisUserId)
        ).length;
        thisUserTeamCompletedMissionsCount = mergedItems.filter(
          thisUserTeamMissionsCompletedFilter.bind(this, thisUserTeamIds, thisUserId)
        ).length;
      } else {
        if (oldItems) {
          const remover = this.REMOVERS_MAP[itemType];
          mergedItems = mergeData(oldItems, sanitizedItems, remover);
        } else {
          mergedItems = sanitizedItems;
        }
        if (itemType === 'emergencyCommunication') {
          //assuming items are already sorted from source
          const lastReportRequest = mergedItems.find(_findRROngoing);
          lastActiveReportRequestID = lastReportRequest ? lastReportRequest.properties.id : null;
        }
      }
      totalFeaturesCount = mergedItems.length;
      await featureStore.setItem(itemType, mergedItems);
      this.dataURLS[itemType] = makeDataURL(this.dataURLS[itemType], mergedItems);
    }

    const lastModificationTime = new Date();
    await featureStore.setItem(`${itemType}-lastModificationDate`, lastModificationTime);
    const stats = {
      totalFeaturesCount,
      newFeaturesCount,
      lastModificationTime,
      featuresURL: this.dataURLS[itemType]
    };
    // Stats relevant only for mission and missionTask
    if (itemType === 'mission' || itemType === 'missionTask') {
      stats['missionTaskFeaturesURL'] = this.dataURLS['missionTask'];
      stats['completedMissionsCount'] = completedMissionsCount;
      stats['thisUserTeamMissionsCount'] = thisUserTeamMissionsCount;
      stats['thisUserTeamCompletedMissionsCount'] = thisUserTeamCompletedMissionsCount;
    }
    // Stats relevant only for report request
    if (itemType === 'emergencyCommunication') {
      stats['lastActiveReportRequestID'] = lastActiveReportRequestID;
    }
    return stats;
  }

  setToken(token, user) {
    this._token = token;
    this._user = user;
    this.ireactBEInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    logWorker('Token updated');
  }

  async clearToken(done) {
    this._token = null;
    this._user = null;
    delete this.ireactBEInstance.defaults.headers.common['Authorization'];
    logWorker('Token cleared, now clearing storage');
    await this.clearAll(done);
  }

  setCulture(culture) {
    this._culture = culture;
    this.ireactBEInstance.defaults.headers.common['Accept-Language'] = this._culture;
    // this.sociaInstance.defaults.params['language'] = culture;
    logWorker('Culture updated', culture);
  }
}

export default IreactFeatureLoader;
