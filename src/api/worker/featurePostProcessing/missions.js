const itemType = 'mission';
import { feature } from '@turf/helpers';
import wellknown from 'wellknown';
import moment from 'moment';
import { logSevereWarningWk } from '../../../utils/log';

// Each API returns different result format here...
export function getMissionsFromAxiosResponse(axiosResponse) {
  if (axiosResponse.data && axiosResponse.data.success === true) {
    return axiosResponse.data.result;
  } else {
    return [];
  }
}

/**
 * Transform server items to GeoJSON features if needed
 * add missing fields
 * tune existing fields
 * convert wkt geometries
 * @param {Object} result - Array.reduce accumulator
 * @param {Object} itemFromServer - item object from server
 */
export function sanitizeMissions(itemFromServer) {
  let result = {}; //To be passed as input parameter
  let hasError = false;
  let missionPoint, /* missionAOI, */ _tasks;

  try {
    // NOTE: missionStatus = valid | aborted We copy missionStatus in all items so
    // we can delete the misssion, missionAOIs and missionTasks if it becomes
    // !=='valid'
    const {
      coordinates,
      //   areaOfInterest,
      tasks,
      id,
      creationTime,
      lastModificationTime,
      missionStatus,
      address,
      ...properties
    } = itemFromServer;
    const lastModified = new Date(lastModificationTime ? lastModificationTime : creationTime);

    const taskIds = tasks.map(t => t.id); // ids of the tasks of this mission
    const numOfCompletedTask = tasks.filter(task => task.isCompleted).length;
    const today = new Date();
    const numOfOngoingTask = tasks.filter(
      task => task.start && moment(task.start) <= today && task.end && moment(task.end) >= today
    ).length;
    const missionPointGeometry = wellknown.parse(coordinates);
    missionPoint = feature(missionPointGeometry, {
      ...properties,
      id,
      taskIds,
      lastModified,
      numOfCompletedTask,
      numOfOngoingTask,
      address,
      itemType,
      missionStatus
    });

    const missionAddress = address; // fall back to this if task is missing address
    _tasks = tasks.map(t => {
      try {
        const { location, address, ...taskProperties } = t;
        const taskPointGeometry =
          typeof location === 'undefined' ? missionPointGeometry : wellknown.parse(location);
        const taskPoint = feature(taskPointGeometry, {
          ...taskProperties,
          itemType: `${itemType}Task`,
          missionId: id,
          missionStatus,
          address: address || missionAddress
        });
        return taskPoint;
      } catch (err) {
        hasError = true;
        logSevereWarningWk(`Cannot remap Task of Mission with id ${id}:`, err, itemFromServer);
        // console.log(`%c(!) Cannot remap Task of Mission with id ${id}:`, 'color:
        // gold; background-color: orangered', err, itemFromServer);
        return null;
      }
    });
  } catch (err) {
    logSevereWarningWk(`Cannot remap Mission with id ${itemFromServer.id}:`, err, itemFromServer);
    // console.log(`%c(!) Cannot remap Mission with id ${itemFromServer.id}:`,
    // 'color: gold; background-color: orangered', err, itemFromServer);
    hasError = true;
  }
  if (!hasError) {
    result.feature = missionPoint; //.items.push(missionPoint);
    // result.itemAOIs.push(missionAOI);
    result.tasks = _tasks; //[...result.tasks, ..._tasks];

    return result;
  } else {
    return null;
  }
}

export function mergeMissions(currentMissions, nextMissions) {
  const result = {
    items: [...currentMissions.items],
    // itemAOIs: [...currentMissions.itemAOIs],
    tasks: [...currentMissions.tasks]
  };
  for (let i = 0; i < nextMissions.items.length; i++) {
    const m = nextMissions.items[i];
    const cm = currentMissions.items.find(f => f.properties.id === m.properties.id);
    if (cm) {
      //mission already exists
      // DELETE cm, cmAOI and related tasks
      if (m.properties.missionStatus === 'aborted') {
        // remove all from result.tasks where task.id in cm.taskIDs
        // (store in result.task only those tasks which do not belong to cm)
        result.tasks = result.tasks.filter(
          task => cm.properties.taskIds.indexOf(task.properties.id) === -1
        );
        // remove from items where id == cm.id
        let missionIndex = result.items.findIndex(
          mission => mission.properties.id === m.properties.id
        );
        result.items.splice(missionIndex, 1);
        // remove from itemAOIs where id == cm.id
        // missionIndex = result.itemAOIs.findIndex(
        //   mission => mission.properties.id === m.properties.id
        // );
        // result.itemAOIs.splice(missionIndex, 1);
      } else {
        // Update existing missions and its tasks
        // remove all from result.tasks where task.id in cm.taskIDs
        const otherMissionsTasks = result.tasks.filter(
          task => cm.properties.taskIds.indexOf(task.properties.id) === -1
        );
        // add all to result.tasks from nextMissions.tasks where task.id in m.taskIDs
        const updatedTasks = nextMissions.tasks.filter(
          t => m.properties.taskIds.indexOf(t.properties.id) > -1
        );
        result.tasks = [...otherMissionsTasks, ...updatedTasks];
        // replace in items where id == cm.id
        let missionIndex = result.items.findIndex(
          mission => mission.properties.id === m.properties.id
        );
        result.items.splice(missionIndex, 1, m);
        // replace in itemAOIs where id == cm.id
        // missionIndex = result.itemAOIs.findIndex(
        //   mission => mission.properties.id === m.properties.id
        // );
        // result.itemAOIs.splice(missionIndex, 1, nextMissions.itemAOIs[i]);
      }
    } else {
      //new Mission
      result.items.push(m);
      //   result.itemAOIs.push(nextMissions.itemAOIs[i]);
      const newTasks = nextMissions.tasks.filter(
        task => m.properties.taskIds.indexOf(task.properties.id) > -1
      );
      result.tasks = [...result.tasks, ...newTasks];
    }
  }
  return result;
}
