import { feature } from '@turf/helpers';
import wellknown from 'wellknown';

import { logSevereWarningWk } from '../../../utils/log';

const emCommItemType = 'emergencyCommunication';
const reportReqItemType = 'reportRequest';

export function sanitizeEmergencyCommunications(itemFromServer) {
  try {
    const {
      /* targetAreaOfInterest, targetLocation, */ targetAddress, // if these are null or undefined
      /* areaOfInterest, */ location,
      address, // they've to be set equal to these
      type,
      level,
      creationTime,
      lastModificationTime,
      ...properties
    } = itemFromServer;
    const lastModified = new Date(lastModificationTime ? lastModificationTime : creationTime);
    const itemType = type === 'reportRequest' ? reportReqItemType : emCommItemType;
    const contentTypeArray = properties.contentType
      ? properties.contentType.split(',').map(item => item.trim())
      : [];
    const featureProperties = {
      ...properties,
      itemType,
      type, // reportRequest || warning || alert
      level,
      lastModified,
      address,
      targetAddress: targetAddress || address,
      // TODO Check with Luca! right this is the WKT
      // targetLocation: targetLocation || location,
      // targetAreaOfInterest: targetAreaOfInterest || areaOfInterest,
      hasMeasure: contentTypeArray.indexOf('measure') > -1,
      hasPeople: contentTypeArray.indexOf('people') > -1,
      hasDamage: contentTypeArray.indexOf('damage') > -1,
      hasResource: contentTypeArray.indexOf('resource') > -1
    };
    return feature(wellknown.parse(location), featureProperties);
  } catch (err) {
    logSevereWarningWk(
      `Cannot remap Emergency Communication with id ${itemFromServer.properties.id}:`,
      err,
      itemFromServer
    );
    return null;
  }
}

// Each API returns different result format here...
export function getEmergencyCommunicationsFromAxiosResponse(axiosResponse) {
  if (axiosResponse.data && axiosResponse.data.success === true) {
    return axiosResponse.data.result.items;
  } else {
    return [];
  }
}

export function removeEmcommsFE() {
  return false;
}

export function removeEmcommsApp(oldFeat, newFeat) {
  return new Date(newFeat.properties.date.end) < new Date();
}
