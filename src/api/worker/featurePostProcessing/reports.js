const itemType = 'report';
import { feature } from '@turf/helpers';
import { logSevereWarningWk } from '../../../utils/log';
/**
 * Transform server items to GeoJSON features if needed
 * add missing fields
 * tune existing fields
 * convert wkt geometries
 * @param {Object} itemFromServer - item object from server
 */
export function sanitizeReports(itemFromServer) {
    try {
        const { organization, creationTime, lastModificationTime } = itemFromServer.properties;
        const reporterType = organization ? 'authority' : 'citizen';
        const lastModified = new Date(lastModificationTime ? lastModificationTime : creationTime);
        const properties = { ...itemFromServer.properties, itemType, reporterType, lastModified };
        return feature(itemFromServer.geometry, properties);
    }
    catch (err) {
        logSevereWarningWk(`Cannot remap Report with id ${itemFromServer.properties.id}:`, err, itemFromServer);
        return null;
    }
}

export function removeReportsFE() {
    return false;
}

export function removeReportsApp(oldFeat, newFeat) {
    return ['submitted', 'validated'].indexOf(newFeat.properties.status) === -1;
}

// Each API returns different result format here...
export function getReportsFromAxiosResponse(axiosResponse) {
    if (axiosResponse.data && axiosResponse.data.success === true) {
        return axiosResponse.data.result.featureCollection.features;
    }
    else {
        return [];
    }
}


