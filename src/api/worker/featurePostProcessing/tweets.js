const itemType = 'tweet';
import { feature } from '@turf/helpers';
import bboxPolygon from '@turf/bbox-polygon';
import center from '@turf/center';
import { logSevereWarningWk } from '../../../utils/log';
import booleanContains from '@turf/boolean-contains';
/**
 * Example bbox
 * "bbox": {
		"southWest": {
			"latitude": 37.861252,
			"longitude": -122.577002
		},
		"northEast": {
			"latitude": 37.909976,
			"longitude": -122.577002
		}
	},
 *
 * @param {TwitterBBox} twitterBBox
 * @returns center coordinates
 */
function getBBoxCenter(twitterBBox) {
  const S = twitterBBox.southWest.latitude;
  const W = twitterBBox.southWest.longitude;
  const N = twitterBBox.northEast.latitude;
  const E = twitterBBox.northEast.longitude;

  return center(bboxPolygon([W, S, E, N])).geometry.coordinates;
}

function extractPictureUrlFromOriginalTweet(tweet) {
  let pictureURL = null;
  if (Array.isArray(tweet.mediaEntities) && tweet.mediaEntities.length > 0) {
    const firstMediaEntity = tweet.mediaEntities[0];
    if (firstMediaEntity.type === 'photo') {
      pictureURL = firstMediaEntity.mediaURLHttps || null;
    }
  } else if (Array.isArray(tweet.extendedMediaEntities) && tweet.extendedMediaEntities.length > 0) {
    // WARNING: THERE COULD BE UP TO 4 Pictures - currenty not handled at UI level
    const firstMediaEntity = tweet.extendedMediaEntities[0];
    if (firstMediaEntity.type === 'photo') {
      pictureURL = firstMediaEntity.mediaURLHttps || null;
    }
  }

  return pictureURL;
}

// Converts tweet to point GeoJSON features - caution: they may report NaNs value when there are no locations available
export function sanitizeTweets(areaOfInterest, itemFromServer) {
  try {
    const {
      geoLocation,
      bbox,
      entity_locations,
      tweetId,
      originalTweet,
      ...properties
    } = itemFromServer;
    // Search for picture
    const pictureURL = extractPictureUrlFromOriginalTweet(originalTweet);
    // Get coordinates: it search in this order: geoLocation (by Twitter), bbox (by Twitter), entities (by Text Analysis)
    // we add positionSource in case we want later filter out less reliable methods
    let positionSource = 'none';
    let coordinates = [NaN, NaN];
    const hasGeolocation = geoLocation !== null;
    if (hasGeolocation) {
      const { longitude, latitude } = geoLocation;
      coordinates = [longitude, latitude];
      positionSource = 'geolocation';
    } else if (bbox !== null) {
      // bbox
      coordinates = getBBoxCenter(bbox);
      positionSource = 'bbox';
    } else if (Array.isArray(entity_locations) && entity_locations.length > 0) {
      const place = entity_locations.find(el =>
        booleanContains(
          areaOfInterest,
          feature(
            {
              type: 'Point',
              coordinates: [el.longitude, el.latitude]
            },
            {}
          )
        )
      );
      const { longitude, latitude } = place;
      coordinates = [longitude, latitude];
      positionSource = 'entities';
    }
    const geometry = {
      type: 'Point',
      coordinates
    };
    return feature(geometry, {
      ...properties,
      geoLocation,
      tweetId,
      pictureURL,
      id: tweetId,
      itemType,
      originalTweet,
      positionSource,
      entity_locations,
      bbox
    });
  } catch (err) {
    logSevereWarningWk(
      `Cannot remap Tweet with id ${itemFromServer.properties.id}:`,
      err,
      itemFromServer
    );
    return null;
  }
}

export function getTweetsFromAxiosResponse(axiosResponse) {
  if (axiosResponse.data && axiosResponse.status < 300) {
    return axiosResponse.data;
  } else {
    return [];
  }
}
