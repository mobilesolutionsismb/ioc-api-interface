const itemType = 'agentLocation';
import { feature } from '@turf/helpers';
import { logSevereWarningWk } from '../../../utils/log';
/**
 * Transform server items to GeoJSON features if needed
 * add missing fields
 * tune existing fields
 * convert wkt geometries
 * @param {Object} itemFromServer - item object from server
 */
export function sanitizeAgentLocations(itemFromServer) {
  try {
    const { locationLastModificationTime, wearableInfo, agentId } = itemFromServer.properties;
    const lastModified = new Date(locationLastModificationTime);
    const dataOrigin = wearableInfo ? 'wearable' : 'mobile';
    const properties = {
      ...itemFromServer.properties,
      id: agentId,
      itemType,
      agentId,
      dataOrigin,
      lastModified
    };
    return feature(itemFromServer.geometry, properties);
  } catch (err) {
    logSevereWarningWk(
      `Cannot remap Agent location with id ${itemFromServer.properties.id}:`,
      err,
      itemFromServer
    );
    return null;
  }
}

// Each API returns different result format here...
export function getAgentLocationsFromAxiosResponse(axiosResponse) {
  if (axiosResponse.data && axiosResponse.data.success === true) {
    return axiosResponse.data.result.features;
  } else {
    return [];
  }
}
