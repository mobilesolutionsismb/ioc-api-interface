const itemType = 'mapRequest';
import { feature } from '@turf/helpers';
import { logSevereWarningWk } from '../../../utils/log';
import wellknown from 'wellknown';
/**
 * Transform server items to GeoJSON features if needed
 * add missing fields
 * tune existing fields
 * convert wkt geometries
 * @param {Object} itemFromServer - item object from server
 */
export function sanitizeMapRequests(itemFromServer) {
  try {
    const { creationTime, lastModificationTime, location } = itemFromServer;
    const lastModified = new Date(lastModificationTime ? lastModificationTime : creationTime);
    const properties = { ...itemFromServer, itemType, lastModified };
    return feature(wellknown.parse(location), properties);
  } catch (err) {
    logSevereWarningWk(
      `Cannot remap map request with id ${itemFromServer.id}:`,
      err,
      itemFromServer
    );
    return null;
  }
}

export function getMapRequestsFromAxiosResponse(axiosResponse) {
  if (axiosResponse.data && axiosResponse.data.success === true) {
    return axiosResponse.data.result;
  } else {
    return [];
  }
}
