function _defaultDeleteFn(/* oldFeat, newFeat */) {
    // Should return a comparison result that determine if the update is a DELETE
    // e.g. oldFeat.properties.some !== newFeat.properties.some
    return false;
}

// Default Merge strategy
export function mergeData(oldData, newData, deleteFn = _defaultDeleteFn) {
    if (newData.length === 0) {
        return oldData;
    }
    // Copy for immutability
    const data = [...oldData];
    // it is fine to push/splice, it is not ok to manipulate element refs 
    // e.g. data[i].properties.something = 'value' will affect oldData
    for (const newFeat of newData) {
        const indexOfOldVersion = data.findIndex(
            f => f.properties.id === newFeat.properties.id /* && f.properties.itemType === newFeat.properties.itemType */
        );
        if (indexOfOldVersion > -1) {
            const oldFeat = data[indexOfOldVersion];
            const mustRemove = deleteFn(oldFeat, newFeat);
            if (mustRemove) {
                // Delete
                data.splice(indexOfOldVersion, 1);
            }
            else {
                // Update
                data.splice(indexOfOldVersion, 1, newFeat);
            }
        }
        else // push
        {
            data.push(newFeat);
        }
    }
    return data;
}
