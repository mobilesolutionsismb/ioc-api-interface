import { logMain, logSevereWarning } from '../../utils/log';

/* eslint-disable */
// ESLint complains about !
import ApiWorker from 'worker-loader?inline&fallback=false!./worker';
// on Windows won't work with import
// const ApiWorker = require('worker-loader?name=api&inline=true&fallback=false!./worker');
/* eslint-enable */

const defaultWorkerErrorHandler = e => {
  logSevereWarning('Worker error', e);
  throw e;
};

class WorkerWrapper {
  constructor({
    baseURL,
    socialURL,
    clientPresets,
    culture,
    token = null,
    user = null,
    handlersMap = {},
    onError = defaultWorkerErrorHandler
  }) {
    this._worker = new ApiWorker();
    this._handlersMap = handlersMap;

    this._onWorkerMessage = e => {
      const messageType = e.data.messageType;
      if (!messageType) {
        logMain(`WorkerWrapper - Unknown message: "${e.data}"`);
        return;
      }
      const handler = this._handlersMap[messageType];
      if (!handler) {
        logMain(`WorkerWrapper - Unhandled messageType: "${messageType}"`);
        return;
      }
      handler(e.data.payload);
    };
    this._worker.onerror = onError;
    this._worker.onmessage = this._onWorkerMessage;

    this.send('init', { baseURL, socialURL, clientPresets, culture, token, user });
  }

  send(messageType, payload) {
    this._worker.postMessage({
      messageType,
      payload
    });
  }

  terminate() {
    this._worker.terminate();
  }
}

export default WorkerWrapper;
