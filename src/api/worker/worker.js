import { logWorker } from '../../utils/log';
import IreactFeatureLoader from './IreactFeatureLoader';

/**
 * Send a message
 * @param {string} messageType
 * @param {Object} payload
 */
function send(messageType, payload = {}) {
  self.postMessage({ messageType, payload });
}

let featureLoader = null;

const onItemTypeLoaded = result => send('featuresLoaded', result);

async function initLoader({ baseURL, socialURL, clientPresets, culture, token, user }) {
  featureLoader = new IreactFeatureLoader(baseURL, socialURL, clientPresets, culture, token, user);
  await featureLoader.init(onItemTypeLoaded); // load features from localforage
  send('ready');
}

function setToken({ token, user }) {
  featureLoader.setToken(token, user);
  if (token) {
    send('tokenSet');
  } else {
    clearToken();
  }
}

async function clearToken() {
  await featureLoader.clearToken(onItemTypeLoaded);
  send('tokenClear');
}

function setCulture({ culture }) {
  featureLoader.setCulture(culture);
  send('cultureSet');
}

async function loadFeatures({ forceReload, ...params }) {
  await featureLoader.loadFeatures(params, onItemTypeLoaded, forceReload);
}

const workerHandlersMap = {
  init: initLoader,
  setToken: setToken,
  clearToken: clearToken,
  setCulture: setCulture,
  loadFeatures: loadFeatures
};

function onMessage(e) {
  const { messageType, payload } = e.data;
  const handler = workerHandlersMap[messageType];
  if (handler) {
    handler(payload);
  } else {
    logWorker(
      messageType ? `Unknown message type "${messageType}"` : `Unknown message "${e.data}"`
    );
  }
}

self.onmessage = onMessage;
