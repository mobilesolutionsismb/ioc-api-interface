function getTweetFeedbackById(tweetId, userId) {
  return {
    url: '/api/services/app/SocialMedia/GetTweetFeedbackList',
    params: {
      tweetId,
      userId
    }
  };
}

function updateFeedbackById(tweetId, userId, properties, feedbackValues) {
  const data = feedbackValues
    ? {
        tweetId,
        userId,
        properties,
        ...feedbackValues
      }
    : {
        tweetId,
        userId,
        properties
      };

  return {
    url: '/api/services/app/SocialMedia/CreateOrUpdateTweetFeedback',
    method: 'post',
    data
  };
}

module.exports = { getTweetFeedbackById, updateFeedbackById };
