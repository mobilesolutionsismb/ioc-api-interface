function register(userObject) {
  const defaults = {
    userId: null,
    captchaResponse: '',
    bypassCaptchaToken: 'bypass'
  };

  let data = {
    ...defaults,
    ...userObject
  };

  return {
    url: '/api/services/app/Account/Register',
    method: 'post',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      Accept: 'application/json; charset=UTF-8'
    },
    data
  };
}

function abortRegistration(userId) {
  return {
    url: '/api/services/app/Account/AbortRegistration',
    method: 'post',
    data: {
      userId
    }
  };
}

function isUserFullyRegistered(userId) {
  return {
    url: '/api/services/app/Account/IsUserFullyRegistered',
    method: 'post',
    data: {
      userId
    }
  };
}

function passwordForgot(emailAddress) {
  return {
    url: '/api/services/app/Account/SendPasswordResetCode',
    method: 'post',
    data: {
      emailAddress
    }
  };
}

function resetPassword(userId, resetCode, password) {
  return {
    url: '/api/services/app/Account/ResetPassword',
    method: 'post',
    data: {
      userId,
      resetCode,
      password
    }
  };
}

// Send or resend email activation code to emailAddress
function sendEmailActivationLink(emailAddress, redirectURL) {
  return {
    url: '/api/services/app/Account/SendEmailActivationLink',
    method: 'post',
    data: {
      emailAddress,
      redirectURL
    }
  };
}

// Send or resend email activation code to emailAddress
function sendPhoneActivationSMS(emailAddress, phoneNumber) {
  return {
    url: '/api/services/app/Account/SendPhoneActivationSms',
    method: 'post',
    data: {
      emailAddress,
      phoneNumber
    }
  };
}

// Finalize registration by sending confirmationCode received via SMS
function finalizeRegistration(emailAddress, confirmationCode) {
  return {
    url: '/api/services/app/Account/FinalizeRegistration',
    method: 'post',
    data: {
      emailAddress,
      confirmationCode
    }
  };
}

function activatePhone(emailAddress, confirmationCode) {
  return {
    url: '/api/services/app/Account/ActivatePhone',
    method: 'post',
    data: {
      emailAddress,
      confirmationCode
    }
  };
}

function activateEmail(userId, confirmationCode) {
  return {
    url: '/api/services/app/Account/ActivateEmail',
    method: 'post',
    data: {
      userId,
      confirmationCode
    }
  };
}

module.exports = {
  register,
  passwordForgot,
  resetPassword,
  sendEmailActivationLink,
  sendPhoneActivationSMS,
  finalizeRegistration,
  activateEmail,
  activatePhone,
  isUserFullyRegistered,
  abortRegistration
};
