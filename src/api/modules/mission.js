function getList(dateStart, dateEnd, Culture = 'en', AreaOfInterest = null, missionStatus = null, lastModificationTime = null) {
    const params = {
        dateStart,
        dateEnd,
        Culture
    };
    if (typeof AreaOfInterest === 'string') {
        params['AreaOfInterest'] = AreaOfInterest;
    }
    if (missionStatus !== null) {
        params['MissionStatus'] = missionStatus;
    }
    if (lastModificationTime) {
        params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
    }
    return {
        url: '/api/services/app/Mission/GetList',
        params
    };
}

// Same as above, but with arguments sorted right
function getAll(dateStart, dateEnd, AreaOfInterest = null, lastModificationTime = null, Culture = 'en', presets = {}, otherArguments = {}) {
    const missionStatus = otherArguments.missionStatus || null;
    const params = {
        dateStart,
        dateEnd,
        Culture
    };
    if (typeof AreaOfInterest === 'string') {
        params['AreaOfInterest'] = AreaOfInterest;
    }
    if (missionStatus !== null) {
        params['MissionStatus'] = missionStatus;
    }
    if (lastModificationTime) {
        params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
    }
    return {
        url: '/api/services/app/Mission/GetList',
        params
    };
}

function get(missionId) {
    return {
        url: '/api/services/app/Mission/Get',
        params: {
            id: missionId
        }
    };
}

function create(mission) {
    return {
        url: '/api/services/app/Mission/Create',
        method: 'post',
        data: mission
    };
}

function addSingleTaskToMission(task) {
    return {
        url: '/api/services/app/Mission/AddSingleTaskToMission',
        method: 'post',
        data: task
    };
}

// function update(mission) {
//     return {
//         url: '/api/services/app/Mission/Update',
//         method: 'put',
//         data: mission
//     };
// }

function deleteMission(missionId) {
    return {
        url: '/api/services/app/Mission/Abort',
        method: 'put',
        // url: '/api/services/app/Mission/Delete',
        // method: 'delete',
        params: {
            id: missionId
        }
    };
}

// FE only
function deleteTask(taskId) {
    return {
        url: '/api/services/app/Mission/DeleteTask',
        method: 'delete',
        params: {
            id: taskId
        }
    };
}

// App only
function releaseTask(taskId) {
    return {
        url: '/api/services/app/Mission/ReleaseTask',
        method: 'post',
        data: {
            id: taskId
        }
    };
}

// App only
function accomplishTask(taskId) {
    return {
        url: '/api/services/app/Mission/AccomplishTask',
        method: 'post',
        data: {
            id: taskId
        }
    };
}

function takeChargeOfTask(taskId) {
    return {
        url: '/api/services/app/Mission/TakeChargeOfTask',
        method: 'post',
        data: {
            id: taskId
        }
    };
}

module.exports = {
    getList,
    getAll,
    get,
    create,
    deleteMission,
    deleteTask,
    releaseTask,
    addSingleTaskToMission,
    accomplishTask,
    takeChargeOfTask
};