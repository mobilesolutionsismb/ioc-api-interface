function getOrganizationUnitTeams(ids) {
    return {
        url: '/api/services/app/OrganizationUnit/GetOrganizationUnitTeams',
        params: {
            ids
        }
    };
}

module.exports = {
    getOrganizationUnitTeams
};