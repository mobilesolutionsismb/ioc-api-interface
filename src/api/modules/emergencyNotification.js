function create(emergencyNotification) {
    return {
        url: '/api/services/app/EmergencyEvent/Create',
        method: 'post',
        data: emergencyNotification
    };
}

module.exports = { create };