// TODO add methods for mapRequest
function getAll(
  datestart = null,
  dateend = null,
  areaOfInterest = null,
  lastModificationTime = null
) {
  const params = {
    areaOfInterest,
    datestart
  };
  if (dateend) {
    params['dateend'] = dateend;
  }
  if (lastModificationTime) {
    params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
  }
  // TODO CHECK THE URL IS CORRECT
  return {
    url: '/api/services/app/MapRequest/GetList',
    params
  };
}

function get(mapRequestId) {
  return {
    url: '/api/services/app/MapRequest/Get',
    params: {
      id: mapRequestId
    }
  };
}

function create(mapRequest) {
  return { url: '/api/services/app/MapRequest/Create', method: 'post', data: mapRequest };
}
function reImport(mapRequest) {
  return {
    url: '/api/services/app/MapRequest/Reimport',
    method: 'post',
    data: mapRequest
  };
}
function deleteMapReq(id) {
  return {
    url: '/api/services/app/MapRequest/Delete',
    method: 'delete',
    params: {
      id
    }
  };
}

module.exports = {
  getAll,
  get,
  create,
  reImport,
  deleteMapReq
};
