function getCurrentUserProfileForEdit() {
  return {
    url: '/api/services/app/Profile/GetCurrentUserProfileForEdit'
  };
}

function getUserPicture() {
  return {
    url: '/api/services/app/Profile/GetUserPicture'
  };
}

function getPasswordComplexitySetting() {
  return {
    url: '/api/services/app/Profile/GetPasswordComplexitySetting'
  };
}

function updateCurrentUserProfile(data) {
  const { name, surname, userName, emailAddress, phoneNumber, timezone, userPicture } = data;
  return {
    url: '/api/services/app/Profile/updateCurrentUserProfile',
    method: 'put',
    data: { name, surname, userName, emailAddress, phoneNumber, timezone, userPicture }
  };
}

function updateUserPicture(userPicture) {
  return {
    url: '/api/services/app/Profile/UpdateUserPicture',
    method: 'put',
    data: {
      userPicture
    }
  };
}

function changePassword(currentPassword, newPassword) {
  return {
    url: '/api/services/app/Profile/ChangePassword',
    method: 'post',
    data: {
      currentPassword,
      newPassword
    }
  };
}

module.exports = {
  getCurrentUserProfileForEdit,
  updateUserPicture,
  getUserPicture,
  changePassword,
  getPasswordComplexitySetting,
  updateCurrentUserProfile
};
