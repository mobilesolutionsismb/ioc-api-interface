
function getAllSettings() {
    return {
        url: '/api/services/app/UserSettings/GetAllSettings'
    };
}

function getDefinitionGroupTree() {
    return {
        url: '/api/services/app/UserSettings/GetDefinitionGroupTree'
    };
}

function getSettingDefinitionsByGroup() {
    return {
        url: '/api/services/app/UserSettings/GetSettingDefinitionsByGroup?IncludeUserValues=true'
    };
}

function updateSettings(settings = []) {
    return {
        url: '/api/services/app/UserSettings/UpdateSettings',
        method: 'put',
        data: {
            settings
        }
    };
}

module.exports = { getAllSettings, getDefinitionGroupTree, getSettingDefinitionsByGroup, updateSettings };
