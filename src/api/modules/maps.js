function geocode(address, culture) {
  return {
    url: '/api/services/app/Maps/Geocode',
    params: {
      address,
      culture
    }
  };
}

function reverseGeocode(culture, latitude, longitude) {
  let queryString = `position=POINT(${longitude} ${latitude})`;

  queryString += culture ? '&culture=' + culture : '';
  return {
    url: '/api/services/app/Maps/ReverseGeocode?' + queryString
  };
}

//OLD VERSION
// function getLayers(startTime = null, endTime = null, areaOfInterest = null) {
//     const params = {
//         areaOfInterest,
//         startTime
//     };
//     if (endTime) {
//         params['endTime'] = endTime;
//     }
//     return {
//         url: '/api/services/app/Maps/GetLayers',
//         params
//     };
// }

function getLayers(startTime = null, endTime = null, areaOfInterest = null) {
  const params = {
    areaOfInterest,
    startTime
  };
  if (endTime) {
    params['endTime'] = endTime;
  }
  return {
    url: '/api/services/app/Maps/v2/GetLayers',
    params
  };
}

function getLayerInfo(id) {
  return {
    url: '/api/services/app/Maps/GetLayerInfo',
    params: { id }
  };
}

module.exports = {
  geocode,
  reverseGeocode,
  getLayers,
  getLayerInfo
};
