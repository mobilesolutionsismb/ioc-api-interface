function getLeaderBoard(SkipCount = 0, MaxResultCount = 10) {
  return {
    url: '/api/services/app/Gamification/Leaderboard',
    params: {
      SkipCount,
      MaxResultCount
    }
  };
}

function getMonthlyAwards() {
  return { url: '/api/services/app/Gamification/MonthAwards' };
}

function getMySkillsAndAchievements() {
  return {
    url: '/api/services/app/Gamification/MySkillsAndAchievements'
  };
}

function getMyScoresAndRanks() {
  return { url: '/api/services/app/Gamification/Me' };
}

function getTipOfQuiz(Id) {
  return {
    url: '/api/services/app/Gamification/GetTipOfQuiz',
    params: {
      Id
    }
  };
}

function getQuizzesByHazard(SkipCount = 0, MaxResultCount = 10, Hazard = 0, Completed = false) {
  return {
    url: '/api/services/app/Gamification/v2/GetQuizzesByHazard',
    params: {
      Hazard,
      Completed,
      SkipCount,
      MaxResultCount
    }
  };
}

function getTipsByHazard(SkipCount = 0, MaxResultCount = 10, Hazard = 0, Completed = false) {
  return {
    url: '/api/services/app/Gamification/v2/GetTipsByHazard',
    params: {
      Hazard,
      Completed,
      SkipCount,
      MaxResultCount
    }
  };
}

function getTipsProgress() {
  return { url: '/api/services/app/Gamification/GetTipsProgress' };
}

function getQuizzesProgress() {
  return { url: '/api/services/app/Gamification/GetQuizzesProgress' };
}

function setTipAsRead(id) {
  return {
    url: '/api/services/app/Gamification/SetTipAsRead',
    method: 'post',
    data: {
      id
    }
  };
}

function setQuizAsAnswered(id) {
  return {
    url: '/api/services/app/Gamification/SetQuizAsAnswered',
    method: 'post',
    data: {
      id
    }
  };
}

module.exports = {
  getLeaderBoard,
  getMonthlyAwards,
  getMySkillsAndAchievements,
  getMyScoresAndRanks,
  getTipOfQuiz,
  getQuizzesByHazard,
  getTipsByHazard,
  getTipsProgress,
  getQuizzesProgress,
  setTipAsRead,
  setQuizAsAnswered
};
