function getEmergencyCommunicationById(emcommId) {
  return {
    url: '/api/services/app/EmergencyCommunication/Get',
    params: {
      id: emcommId
    }
  };
}

function getMeasuresByCategory(category) {
  return {
    url: '/api/services/app/EmergencyCommunication/GetMeasuresByCategory',
    params: {
      category
    }
  };
}

function getReportRequestList(SkipCount = 0, MaxResultCount = 10, filters) {
  return {
    url: '/api/services/app/EmergencyCommunication/GetReportRequestList',
    params: {
      SkipCount,
      MaxResultCount,
      Status: filters.status,
      ContentType: filters.contentType,
      OrganizationUnitId: filters.organizationUnitId,
      MeasureCategory: filters.measureCategory
    }
  };
}

function getList(SkipCount = 0, MaxResultCount = 10, Sorting = null, filters = {}) {
  return {
    url: '/api/services/app/EmergencyCommunication/GetList',
    params: {
      SkipCount,
      MaxResultCount,
      Sorting,
      type: filters.type,
      dateStart: filters.start,
      status: filters.status
    }
  };
}

// function getAllEmergencyCommunications(type, datestart = null, dateend = null, areaOfInterest = null, lastModificationTime = null) {
function getAllEmergencyCommunications(
  datestart = null,
  dateend = null,
  areaOfInterest = null,
  lastModificationTime = null,
  Culture = 'en',
  presets = {},
  type = ['alert', 'warning', 'reportRequest']
) {
  const params = {
    type,
    areaOfInterest,
    datestart
  };
  if (dateend) {
    params['dateend'] = dateend;
  }
  if (lastModificationTime) {
    params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
  }
  if (!presets['frontend']) {
    params['status'] = 'ongoing';
  }

  return {
    url: '/api/services/app/EmergencyCommunication/GetList',
    params
  };
}

function createReportRequest(reportRequest) {
  return {
    url: '/api/services/app/EmergencyCommunication/CreateReportRequest',
    method: 'post',
    data: {
      ...reportRequest
    }
  };
}

function updateReportRequest(reportRequest) {
  return {
    url: '/api/services/app/EmergencyCommunication/UpdateReportRequest',
    method: 'put',
    data: {
      ...reportRequest
    }
  };
}

function createEmergencyCommunication(emComm) {
  return {
    url: '/api/services/app/EmergencyCommunication/CreateAlertAndWarning',
    method: 'post',
    data: {
      ...emComm
    }
  };
}

function updateEmergencyCommunication(emComm) {
  return {
    url: '/api/services/app/EmergencyCommunication/UpdateAlertAndWarning',
    method: 'put',
    data: {
      ...emComm
    }
  };
}

module.exports = {
  getEmergencyCommunicationById,
  getMeasuresByCategory,
  getReportRequestList,
  getList,
  getAllEmergencyCommunications,
  createReportRequest,
  updateReportRequest,
  createEmergencyCommunication,
  updateEmergencyCommunication
};
