function getList() {
    return {
        url: '/api/services/app/Measure/GetList'
    };
}

module.exports = { getList };