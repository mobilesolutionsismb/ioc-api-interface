function sendTestNotification(text, customProp) {
    return {
        url: '/api/services/app/Test/SendTestNotification',
        method: 'post',
        data: { text, customProp }
    };
}

// Get paginated user notifications: State = null --> any, State = 0 -> unread, State = 1 -> read
// State = null must be omitted
function getUserNotifications(MaxResultCount = 10, SkipCount = 0, State = null) {
    const params = {
        MaxResultCount, SkipCount
    };
    if (typeof State === 'number') {
        params['State'] = State;
    }
    return {
        url: '/api/services/app/Notification/GetUserNotifications',
        params
    };
}

function setAllNotificationsAsRead() {
    return {
        url: '/api/services/app/Notification/SetAllNotificationsAsRead',
        method: 'post'
    };
}

function setNotificationAsRead(id) {
    return {
        url: '/api/services/app/Notification/SetNotificationAsRead',
        method: 'post',
        data: { id }
    };
}

module.exports = {
    sendTestNotification,
    getUserNotifications,
    setAllNotificationsAsRead,
    setNotificationAsRead
};