function getEnums() {
    return {
        url: '/api/services/app/Enums/GetEnums'
    };
}

function get(enumName) {
    return {
        url: '/api/services/app/Enums/Get',
        params: {
            Name: enumName
        }
    };
}

function getRoles() {
    return {
        url: '/api/services/app/Role/GetRoles',
    };
}

module.exports = { getEnums, get, getRoles };