function createEmergencyCommunication(emCommFormData) {
    if(!(emCommFormData instanceof FormData)){
        throw new Error('Data must be wrapped in a FormData');
    }
    return {
        url: '/api/services/app/EmergencyCommunication/v2/CreateAlertAndWarning',
        method: 'post',
        data: emCommFormData
    };
}

function updateEmergencyCommunication(emCommFormData) {
    if(!(emCommFormData instanceof FormData)){
        throw new Error('Data must be wrapped in a FormData');
    }
    return {
        url: '/api/services/app/EmergencyCommunication/v2/UpdateAlertAndWarning',
        method: 'put',
        data: emCommFormData
    };
}


module.exports = {
    createEmergencyCommunication,
    updateEmergencyCommunication
};