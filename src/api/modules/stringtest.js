function getString() {
    return {
        url: '/api/services/app/Test/GetString'
    };
}

function getAuthString() {
    return {
        url: '/api/services/app/Test/GetAuthString'
    };
}

module.exports = { getString, getAuthString };