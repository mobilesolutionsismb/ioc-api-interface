/**
 * Each modules has one or more methods returning the corresponding
 * axios config for its APIs calls
 * @see https://github.com/mzabriskie/axios#request-config
 */

import * as test from './stringtest';
import * as enums from './enum';

import * as account from './account';
import * as profile from './profile';
import * as maps from './maps';
import * as report from './report';
import * as emergencyEvent from './emergencyEvent';
import * as emergencyNotification from './emergencyNotification';
import * as mission from './mission';
import * as measure from './measure';
import * as notifications from './notifications';
import * as pushNotifications from './pushNotifications';
import * as gamification from './gamification';
import * as emergencyCommunication from './emergencyCommunication';
import * as emergencyCommunicationV2 from './emergencyCommunication.v2';
import * as generalSettings from './generalSettings';
import * as applicationSettings from './applicationSettings';
import * as organizationUnit from './organizationUnit';

import * as mapRequest from './mapRequest';
import * as agentLocation from './agentLocation';
import * as tweetFeedback from './tweetFeedback';

module.exports = {
  test,
  enums,
  account,
  profile,
  maps,
  report,
  emergencyEvent,
  emergencyNotification,
  mission,
  measure,
  pushNotifications,
  notifications,
  gamification,
  emergencyCommunication,
  emergencyCommunicationV2,
  generalSettings,
  applicationSettings,
  organizationUnit,
  mapRequest,
  agentLocation,
  tweetFeedback
};
