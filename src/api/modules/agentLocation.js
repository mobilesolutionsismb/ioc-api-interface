// TODO add methods for retrieving agent positions
/**
 *
 * @param {String} AreaOfInterest - filter with area of interest
 * @param {String} LastModificationTime - isostring of the date: get positions since this date
 */
export function getInformation(
  datestart = null,
  dateend = null,
  AreaOfInterest = null,
  lastModificationTime = null
) {
  const params = {
    AreaOfInterest
  };
  if (lastModificationTime) {
    params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
  }
  // TODO CHECK THE URL IS CORRECT
  return {
    url: '/api/services/app/ProAgent/GetInformation',
    params
  };
}

export function updateUserLocation(location, deviceData) {
  const data = deviceData
    ? {
        location,
        ...deviceData
      }
    : {
        location
      };
  return {
    url: '/api/services/app/ProAgent/UpdateInformation',
    method: 'post',
    data
  };
}
