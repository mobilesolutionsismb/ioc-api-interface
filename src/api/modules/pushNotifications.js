function installation(platform, pushChannel, templates = {}) {
    return {
        url: '/api/services/app/PushNotification/Installation',
        method: 'post',
        data: {
            platform,
            pushChannel,
            templates
        }
    };
}

// function send(message, roles = [], organizations = [], usernames = []) {
//     return {
//         url: '/api/services/app/PushNotification/Send',
//         method: 'post',
//         data: {
//             message,
//             roles,
//             organizations,
//             usernames
//         }
//     };
// }

module.exports = {
    installation/* ,
    send */
};