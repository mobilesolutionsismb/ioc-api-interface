function getList(Start, End, Culture = 'en', AreaOfInterest = null, Status = 'all', lastModificationTime = null) {
    const params = {
        Start,
        End,
        Culture,
        Status
    };
    if (typeof AreaOfInterest === 'string') {
        params['AreaOfInterest'] = AreaOfInterest;
    }
    if (lastModificationTime) {
        params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
    }
    return {
        url: '/api/services/app/Report/GetList',
        params
    };
}

// Same as above, but with arguments sorted right
function getAll(Start, End, AreaOfInterest = null, lastModificationTime = null, Culture = 'en', presets = {}) {
    const Status = presets.Status || 'all';
    const params = {
        Start,
        End,
        Culture,
        Status
    };
    if (typeof AreaOfInterest === 'string') {
        params['AreaOfInterest'] = AreaOfInterest;
    }
    if (lastModificationTime) {
        params['lastModificationTime'] = new Date(lastModificationTime).toISOString();
    }
    return {
        url: '/api/services/app/Report/GetList',
        params
    };
}

function get(id) {
    return {
        url: '/api/services/app/Report/Get',
        params: {
            id
        }
    };
}

function getUIConfig() {
    return {
        url: '/api/services/app/Report/GetUI'
    };
}

function deleteReport(id) {
    return {
        url: '/api/services/app/Report/Delete',
        method: 'delete',
        params: {
            id
        }
    };
}

function uploadReport(reportFormData) {
    return {
        url: '/api/services/app/Report/FormdataCreate',
        method: 'post',
        data: reportFormData
    };
}

function upVote(Id) {
    return {
        url: '/api/services/app/Report/Upvote',
        method: 'post',
        params: {
            Id
        }
    };
}

function downVote(Id) {
    return {
        url: '/api/services/app/Report/Downvote',
        method: 'post',
        params: {
            Id
        }
    };
}

function validate(Id) {
    return {
        url: '/api/services/app/Report/Validate',
        method: 'post',
        params: {
            Id
        }
    };
}

function setInappropriate(Id) {
    return {
        url: '/api/services/app/Report/SetInappropriate',
        method: 'post',
        params: {
            Id
        }
    };
}

function setInaccurate(Id) {
    return {
        url: '/api/services/app/Report/SetInaccurate',
        method: 'post',
        params: {
            Id
        }
    };
}

function getReportExtensionData(reportIds) {
    return {
        url: '/api/services/app/Report/GetReportExtensionData',
        params: {
            reportIds
        }
    };
}

module.exports = {
    getList,
    getAll,
    get,
    getUIConfig,
    deleteReport,
    uploadReport,
    upVote,
    downVote,
    validate,
    setInappropriate,
    setInaccurate,
    getReportExtensionData
};