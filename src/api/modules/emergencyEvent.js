function getAll(AreaOfInterest = '', DateStart = null, DateEnd = null, SkipCount = 0, MaxResultCount = 10, Sorting = 'start DESC', Status = 'all') {
    const params = {
        AreaOfInterest,
        MaxResultCount,
        SkipCount,
        Status
    };
    if (typeof DateStart === 'string') {
        params['DateStart'] = DateStart;
    }
    if (typeof DateEnd === 'string') {
        params['DateEnd'] = DateEnd;
    }
    if (typeof Sorting === 'string') {
        params['Sorting'] = Sorting;
    }

    if (MaxResultCount == null) 
        params['MaxResultCount'] = 10;
    
    if (SkipCount == null) 
        params['SkipCount'] = 0;
    
    if (Status == null) 
        params['Status'] = 'all';
    
    return {url: '/api/services/app/EmergencyEvent/GetAll', params};
}

function get(id) {
    return {url: '/api/services/app/EmergencyEvent/Get', params: {
            id
        }};
}

function create(emergencyEvent) {
    return {url: '/api/services/app/EmergencyEvent/Create', method: 'post', data: emergencyEvent};
}

function update(emergencyEvent) {
    return {
        url: '/api/services/app/EmergencyEvent/Update',
        method: 'put',
        data: {
            ...emergencyEvent
        }
    };
}

function deleteEmergencyEvent(id) {
    return {url: '/api/services/app/EmergencyEvent/Delete', method: 'delete', params: {
            id
        }};
}

// function getLayersByEventId(EmergencyEventId) {
//     return {url: '/api/services/app/EmergencyEvent/GetLayersByEventId', params: {
//             EmergencyEventId
//         }};
// }

module.exports = {
    getAll,
    get,
    create,
    update,
    deleteEmergencyEvent,
    // getLayersByEventId
};