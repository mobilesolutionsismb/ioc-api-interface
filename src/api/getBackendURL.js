const isNode = typeof window === 'undefined' && typeof process !== 'undefined';

function getBackendURL() {
  if (isNode) {
    if (typeof process.env.IREACT_BACKEND === 'undefined') {
      throw new Error('Missing IREACT_BACKEND in env variables');
    } else {
      return process.env.IREACT_BACKEND;
    }
  } else {
    if (typeof window.IREACT_BACKEND === 'undefined') {
      throw new Error('Missing window.IREACT_BACKEND as global variable');
    } else {
      return window.IREACT_BACKEND;
    }
  }
}

function getSocialBackendURL() {
  if (isNode) {
    if (typeof process.env.SOCIAL_BACKEND === 'undefined') {
      throw new Error('Missing SOCIAL_BACKEND in env variables');
    } else {
      return process.env.IREACT_BACKEND;
    }
  } else {
    if (typeof window.SOCIAL_BACKEND === 'undefined') {
      throw new Error('Missing window.SOCIAL_BACKEND as global variable');
    } else {
      return window.SOCIAL_BACKEND;
    }
  }
}

// Valid presets for clients
const VALID_APP_PRESETS = ['frontend', 'mobile-app'];

function getPresets() {
  let presets;
  if (isNode) {
    presets = typeof process.env.CLIENT_TYPE === 'undefined' ? 'frontend' : process.env.CLIENT_TYPE;
  } else {
    presets = typeof window.CLIENT_TYPE === 'undefined' ? 'frontend' : window.CLIENT_TYPE;
  }
  return VALID_APP_PRESETS.indexOf(presets) > -1 ? presets : 'frontend';
}

module.exports = { getBackendURL, getSocialBackendURL, getPresets, VALID_APP_PRESETS };
