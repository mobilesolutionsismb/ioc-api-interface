class APIError extends Error {
	constructor(message, status = 0, statusText = '', serverDetails = {
			message: ''
		}) {
		super(message);
		this.details = {
			status,
			statusText,
			serverDetails
		};
	}
}

/**
 * Private function
 * Checks exception and if it's a network/API error, wraps it into
 * an APIError with optional message. Otherwise returns the Error as it is
 * @see https://github.com/mzabriskie/axios#handling-errors
 * @param {Error} error 
 * @param {String} message
 * @return {APIError|Error} 
 */
function apiFailure(error, message = '_api_failure') {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        return new APIError(message, error.response.status, error.response.statusText, error.response.data);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        return new APIError('_no_response');
    } else {
        // Something happened in setting up the request that triggered an Error
        return error;
    }
}

module.exports = { APIError, apiFailure };