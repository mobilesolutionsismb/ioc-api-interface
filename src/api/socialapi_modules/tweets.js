/**
 *
 *
 * @param {string} dateFrom - date start in YYYYMMDDHHmm format
 * @param {string} dateTo - date end in YYYYMMDDHHmm format
 * @param {array} bbox - array representing a bounding box for limiting results in WSEN order (min lon, min lat, max lon, max lat)
 * @param {string} lastUpdate - date of last update in YYYYMMDDHHmm format
 * @param {string|null} [user=null] - user id of this request
 * @param {string} [language='any'] - tweets language filter
 * @param {number} [limit=100] - max results
 * @returns
 */
function getTweets(dateFrom, dateTo, bbox, lastUpdate, user = null, language = 'any', limit = 100) {
  const params = {
    dateFrom,
    dateTo,
    bbox: bbox.join(';'),
    limit,
    retweet: false
  };
  if (user) {
    params['user'] = user;
  }
  if (language && language !== 'any') {
    params['language'] = language;
  }
  if (lastUpdate) {
    params['lastUpdate'] = lastUpdate;
  }
  return {
    url: '/api/select',
    params
  };
}

module.exports = { getTweets };
