import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getEnumByName, getEnums } from '../modules/enums';
import { getRoles } from '../modules/roles';

import { API } from '../api';
const api = API.getInstance();

const selector = {
  enums: state => state.api_enums.enums
};

const enumsAndRolesActionCreators = {
  getEnums,
  getEnumByName // CHECK IF NEEDED
};

if (api.presets !== 'mobile-app') {
  selector['roles'] = state => state.api_roles.roles;
  enumsAndRolesActionCreators['getRoles'] = getRoles;
}

const select = createStructuredSelector(selector);
export { select as enumsSelector };
export { enumsAndRolesActionCreators };
export default connect(
  select,
  enumsAndRolesActionCreators
);
