import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { updateIREACTFeatures } from '../modules/CommonActionCreators';

const select = createStructuredSelector({
  ireactFeaturesLastUpdated: state =>
    Math.max(
      state.api_reports.reportsLastUpdated,
      state.api_emergencyCommunications.emergencyCommunicationsLastUpdated,
      state.api_missions.missionsLastUpdated,
      state.api_agentLocations.agentLocationsLastUpdated,
      state.api_mapRequests.mapRequestsLastUpdated
    ),
  ireactFeaturesLeastUpdated: state =>
    Math.min(
      state.api_reports.reportsLastUpdated,
      state.api_emergencyCommunications.emergencyCommunicationsLastUpdated,
      state.api_missions.missionsLastUpdated,
      state.api_agentLocations.agentLocationsLastUpdated,
      state.api_mapRequests.mapRequestsLastUpdated
    ),
  ireactFeaturesIsUpdating: state =>
    state.api_reports.reportsUpdating ||
    state.api_emergencyCommunications.emergencyCommunicationsUpdating ||
    state.api_missions.missionsUpdating ||
    state.api_agentLocations.agentLocationsUpdating ||
    state.api_mapRequests.mapRequestsUpdating
});

export { select as ireactFeaturesSelector };
export const ireactFeaturesActionCreators = { updateIREACTFeatures };
export default connect(
  select,
  ireactFeaturesActionCreators
);
