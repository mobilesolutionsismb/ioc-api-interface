import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getLayers } from '../modules/layers';

// TODO add copernicus stuff
const select = createStructuredSelector({
  copernicusHazardNames: state => state.api_layers.copernicusHazardNames,
  mapLayers: state => state.api_layers.mapLayers,
  mapLayersUpdating: state => state.api_layers.mapLayersUpdating
});

export { select as mapLayersSelector };

export const mapLayersActionCreators = { getLayers };

export default connect(
  select,
  mapLayersActionCreators
);
