import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  updateScores,
  updateSkillsAndAchievements,
  updateMonthlyAwards,
  updateLeaderBoard
} from '../modules/gamification';

const select = createStructuredSelector({
  gamificationProfile: state => state.api_gamification.gamification.profile,
  gamificationMonthlyAwards: state => state.api_gamification.gamification.monthlyAwards,
  gamificationSkillsAndAchievements: state =>
    state.api_gamification.gamification.skillsAndAchievements,
  gamificationLeaderboard: state => state.api_gamification.gamification.leaderboard
});

export { select as gamificationSelector };

export const gamificationActionCreators = {
  updateScores,
  updateSkillsAndAchievements,
  updateMonthlyAwards,
  updateLeaderBoard
};

export default connect(
  select,
  gamificationActionCreators
);
