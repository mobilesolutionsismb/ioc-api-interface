import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { updateAgentLocations } from '../modules/agentLocations';

const select = createStructuredSelector({
  agentLocationFeaturesURL: state => state.api_agentLocations.agentLocationFeaturesURL, // blob url
  agentLocationFeaturesStats: state => state.api_agentLocations.agentLocationFeaturesStats,
  agentLocations: state => state.api_agentLocations.agentLocations,
  agentLocationsLastUpdated: state => state.api_agentLocations.agentLocationsLastUpdated,
  agentLocationsUpdating: state => state.api_agentLocations.agentLocationsUpdating
});

export const agentLocationsActionCreators = {
  updateAgentLocations
};
export { select as agentLocationsSelector };

export default connect(
  select,
  agentLocationsActionCreators
);
