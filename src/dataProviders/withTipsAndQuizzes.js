import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  getTipOfQuiz,
  getQuizzesByHazard,
  getTipsByHazard,
  getTipsProgress,
  getQuizzesProgress,
  setTipAsRead,
  setQuizAsAnswered
} from '../modules/tipsAndQuizzes';

const select = createStructuredSelector({
  tips: state => state.api_tipsAndQuizzes.tipsAndQuizzes.tips,
  quizzes: state => state.api_tipsAndQuizzes.tipsAndQuizzes.quizzes,
  tipsProgress: state => state.api_tipsAndQuizzes.tipsAndQuizzes.tipsProgress,
  quizzesProgress: state => state.api_tipsAndQuizzes.tipsAndQuizzes.quizzesProgress,
  selectedQuizAnswers: state => state.api_tipsAndQuizzes.tipsAndQuizzes.selectedQuizAnswers
});

export { select as tipsAndQuizzesSelector };
export const tipsAndQuizzesActionCreators = {
  getTipOfQuiz,
  getQuizzesByHazard,
  getTipsByHazard,
  getTipsProgress,
  getQuizzesProgress,
  setTipAsRead,
  setQuizAsAnswered
};

export default connect(
  select,
  tipsAndQuizzesActionCreators
);
