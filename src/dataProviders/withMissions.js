import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  updateMissions, // GET + diff update
  createMission,
  updateMission, // update single mission
  getMissionById,
  deleteMission,
  deleteTaskOfMission,
  releaseTaskOfMission,
  addSingleTaskToMission,
  accomplishTask,
  takeChargeOfTask
} from '../modules/missions';

const select = createStructuredSelector({
  missionFeaturesURL: state => state.api_missions.missionFeaturesURL, // blob url
  missionTaskFeaturesURL: state => state.api_missions.missionTaskFeaturesURL, // blob url
  missionFeaturesStats: state => state.api_missions.missionFeaturesStats,
  missions: state => state.api_missions.missions,
  missionsUpdating: state => state.api_missions.missionsUpdating,
  missionTasks: state => state.api_missions.missionTasks,
  missionsLastUpdated: state => state.api_missions.missionsLastUpdated
});

export { select as missionsSelector };
export const missionsActionCreators = {
  updateMissions,
  createMission,
  updateMission,
  getMissionById,
  deleteMission,
  deleteTaskOfMission,
  releaseTaskOfMission,
  addSingleTaskToMission,
  accomplishTask,
  takeChargeOfTask
};

export default connect(
  select,
  missionsActionCreators
);
