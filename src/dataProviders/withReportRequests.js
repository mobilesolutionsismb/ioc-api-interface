import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getMeasuresByCategory } from '../modules/categories';
import {
  isRR,
  updateEmergencyCommunications as updateReportRequests,
  createReportRequest,
  updateReportRequest
} from '../modules/emergencyCommunications';

const categoriesSelector = state => state.api_categories.categories;

const reportRequestSelector = state =>
  state.api_emergencyCommunications.emergencyCommunications.filter(isRR);
const reportRequestLastUpdatedSelector = state =>
  state.api_emergencyCommunications.emergencyCommunicationsLastUpdated;

const select = createStructuredSelector({
  categories: categoriesSelector,
  reportRequestFeaturesURL: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesURL, // blob url
  reportRequestFeaturesStats: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesStats,
  reportRequestsUpdating: state =>
    state.api_emergencyCommunications.emergencyCommunicationsUpdating,
  reportRequests: reportRequestSelector,
  reportRequestsLastUpdated: reportRequestLastUpdatedSelector
});

export { select as reportRequestSelector };

export const reportRequestActionCreators = {
  getMeasuresByCategory,
  updateReportRequests,
  createReportRequest,
  updateReportRequest
};
export default connect(
  select,
  reportRequestActionCreators
);
