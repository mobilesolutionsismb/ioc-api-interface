import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  getAllUserSettings,
  getDefinitionGroupTreeSettings,
  getSettingDefinitionsByGroup,
  updateUserSettings
} from '../modules/settings';
// TODO tree stuff
const select = createStructuredSelector({
  generalSettings: state => state.api_settings.generalSettings,
  generalSettingsUpdating: state => state.api_settings.generalSettingsUpdating
});

export { select as generalSettingsSelector };
export const generalSettingsActionCreators = {
  getAllUserSettings,
  getDefinitionGroupTreeSettings,
  getSettingDefinitionsByGroup,
  updateUserSettings
};

export default connect(
  select,
  generalSettingsActionCreators
);
