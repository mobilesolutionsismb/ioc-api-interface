import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  isAoW,
  updateEmergencyCommunications as updateAlertsAndWarnings,
  createEmergencyCommunication,
  updateEmergencyCommunication
} from '../modules/emergencyCommunications';

const categoriesSelector = state => state.api_categories.categories;

const alertsAndWarningsSelector = state =>
  state.api_emergencyCommunications.emergencyCommunications.filter(isAoW);
const alertsAndWarningsLastUpdatedSelector = state =>
  state.api_emergencyCommunications.emergencyCommunicationsLastUpdated;

const select = createStructuredSelector({
  categories: categoriesSelector,
  emergencyCommunicationFeaturesURL: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesURL, // blob url
  emergencyCommunicationFeaturesStats: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesStats,

  alertsAndWarningsUpdating: state =>
    state.api_emergencyCommunications.emergencyCommunicationsUpdating,
  alertsAndWarnings: alertsAndWarningsSelector,
  alertsAndWarningsLastUpdated: alertsAndWarningsLastUpdatedSelector
});

export { select as alertsAndWarningsSelector };
export const alertsAndWarningsActionCreators = {
  updateAlertsAndWarnings,
  createEmergencyCommunication,
  updateEmergencyCommunication
};

export default connect(
  select,
  alertsAndWarningsActionCreators
);
