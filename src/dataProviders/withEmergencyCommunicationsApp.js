import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { updateEmergencyCommunications } from '../modules/emergencyCommunications';

const emergencyCommunicationsSelector = state =>
  state.api_emergencyCommunications.emergencyCommunications;
const emergencyCommunicationsLastUpdatedSelector = state =>
  state.api_emergencyCommunications.emergencyCommunicationsLastUpdated;

const select = createStructuredSelector({
  emergencyCommunicationFeaturesURL: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesURL, // blob url
  emergencyCommunicationFeaturesStats: state =>
    state.api_emergencyCommunications.emergencyCommunicationFeaturesStats,

  emergencyCommunicationsUpdating: state =>
    state.api_emergencyCommunications.emergencyCommunicationsUpdating,
  emergencyCommunications: emergencyCommunicationsSelector,
  emergencyCommunicationsLastUpdated: emergencyCommunicationsLastUpdatedSelector
});

export { select as emcommsSelector };

export const emcommsActionCreators = { updateEmergencyCommunications };

export default connect(
  select,
  emcommsActionCreators
);
