import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  // getEmergencyEventById,
  getEmergencyEvents,
  createEmergencyEvent,
  updateEmergencyEvent,
  AVAILABLE_SORTERS,
  AVAILABLE_SORTERS_VALUES,
  AVAILABLE_STATUS_FILTERS,
  AVAILABLE_STATUS_FILTERS_VALUES,
  setEventFilter,
  setEventSorter,
  selectEvent,
  deselectEvent,
  activateEvent,
  deactivateEvent,
  mouseEnterEvent,
  mouseLeaveEvent
} from '../modules/emergencyEvents';

const select = createStructuredSelector({
  eventSorter: state =>
    AVAILABLE_SORTERS[
      AVAILABLE_SORTERS_VALUES.indexOf(state.api_emergencyEvents.emergencyEventsSorting)
    ],
  eventFilter: state =>
    AVAILABLE_STATUS_FILTERS[
      AVAILABLE_STATUS_FILTERS_VALUES.indexOf(state.api_emergencyEvents.emergencyEventsStatusFilter)
    ],
  availableEventSorters: () => AVAILABLE_SORTERS,
  availableEventFilters: () => AVAILABLE_STATUS_FILTERS,
  emergencyEvents: state => state.api_emergencyEvents.emergencyEvents,
  emergencyEventsLastUpdated: state => state.api_emergencyEvents.emergencyEvents.lastUpdated,
  emergencyEventFeatures: state => state.api_emergencyEvents.emergencyEvents.items,
  emergencyEventFeatureAOIs: state => state.api_emergencyEvents.emergencyEvents.itemPolygons,
  // Hovered event
  hoveredEventId: state =>
    state.api_emergencyEvents.hoveredEvent
      ? state.api_emergencyEvents.hoveredEvent.properties.id
      : -1,
  hoveredEvent: state => state.api_emergencyEvents.hoveredEvent,
  hoveredEventBounds: state => state.api_emergencyEvents.hoveredEventBounds,
  // Selected event
  selectedEventId: state =>
    state.api_emergencyEvents.selectedEvent
      ? state.api_emergencyEvents.selectedEvent.properties.id
      : -1,
  selectedEvent: state => state.api_emergencyEvents.selectedEvent,
  selectedEventBounds: state => state.api_emergencyEvents.selectedEventBounds,
  // Active event
  activeEventId: state =>
    state.api_emergencyEvents.activeEvent
      ? state.api_emergencyEvents.activeEvent.properties.id
      : -1,
  activeEvent: state => state.api_emergencyEvents.activeEvent,
  activeEventBounds: state => state.api_emergencyEvents.activeEventBounds,
  activeEventTW: state => state.api_emergencyEvents.activeEventTW,
  activeEventAOI: state => state.api_emergencyEvents.activeEventAOI
});

export { select as emergencyEventsSelector };

export const emergencyEventActionCreators = {
  getEmergencyEvents,
  createEmergencyEvent,
  updateEmergencyEvent,
  setEventSorter,
  setEventFilter,
  // Selection
  selectEvent,
  deselectEvent,
  // Activation
  activateEvent,
  deactivateEvent,
  //Hovering
  mouseEnterEvent,
  mouseLeaveEvent
};

export default connect(
  select,
  emergencyEventActionCreators
);
