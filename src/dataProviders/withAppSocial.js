import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  updateTweets,
  setTweetFetchingTimespan,
  setTweetFetchingLimit,
  setTweetFetchingLanguage,
  supportedLanguages,
  sendTweetFeedback
} from '../modules/mobileAppTweets';

const select = createStructuredSelector({
  tweetFeaturesURL: state => state.api_mobileApp_tweets.tweetFeaturesURL, // blob url
  tweetFeaturesStats: state => state.api_mobileApp_tweets.tweetFeaturesStats,
  tweets: state => state.api_mobileApp_tweets.tweets,
  tweetsLastUpdated: state => state.api_mobileApp_tweets.tweetsLastUpdated,
  tweetsUpdating: state => state.api_mobileApp_tweets.tweetsUpdating,
  tweetsFetchingTimespan: state => state.api_mobileApp_tweets.tweetsFetchingTimespanHours,
  tweetsFetchingLimit: state => state.api_mobileApp_tweets.tweetsFetchingLimit,
  tweetsFetchingLanguage: state => state.api_mobileApp_tweets.tweetsFetchingLanguage,
  tweetsSupportedLanguages: () => supportedLanguages
});

export { select as appSocialSelector };

export const appSocialActionCreators = {
  updateTweets,
  setTweetFetchingTimespan,
  setTweetFetchingLimit,
  setTweetFetchingLanguage,
  supportedLanguages,
  sendTweetFeedback
};

export default connect(
  select,
  appSocialActionCreators
);
