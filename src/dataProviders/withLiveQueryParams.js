import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

// Expose only the live TW and live AOI
const select = createStructuredSelector({
  liveAOI: state => state.api_liveParams.liveAOI,
  liveTW: state => state.api_liveParams.liveTW
});

export { select as liveQueryParamsSelector };

export default connect(select);
