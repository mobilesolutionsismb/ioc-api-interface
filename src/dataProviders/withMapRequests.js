import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { updateMapRequests } from '../modules/mapRequests';

const mapRequestsSelector = state => state.api_mapRequests.mapRequests;
const mapRequestsLastUpdatedSelector = state => state.api_mapRequests.mapRequestsLastUpdated;
const mapRequestsUpdatingSelector = state => state.api_mapRequests.mapRequestsUpdating;

const mapRequestFeaturesURL = state => state.api_mapRequests.mapRequestFeaturesURL; // blob url
const mapRequestFeaturesStats = state => state.api_mapRequests.mapRequestFeaturesStats;

const select = createStructuredSelector({
  mapRequestFeaturesURL,
  mapRequestFeaturesStats,
  mapRequests: mapRequestsSelector,
  mapRequestsLastUpdated: mapRequestsLastUpdatedSelector,
  mapRequestsUpdating: mapRequestsUpdatingSelector
});

export { select as mapRequestSelector };
export const mapRequestActionCreators = {
  updateMapRequests
};

export default connect(
  select,
  mapRequestActionCreators
);
