import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  updateReports,
  upVoteReport,
  downVoteReport,
  validateReport,
  setInappropriateReport,
  setInaccurateReport,
  getReportExtensionData
} from '../modules/reports';

const select = createStructuredSelector({
  reportFeaturesURL: state => state.api_reports.reportFeaturesURL, // blob url
  reportFeaturesStats: state => state.api_reports.reportFeaturesStats,
  reports: state => state.api_reports.reports,
  reportsLastUpdated: state => state.api_reports.reportsLastUpdated,
  reportsUpdating: state => state.api_reports.reportsUpdating
});

export { select as reportsSelector };
export const reportsActionCreators = {
  updateReports,
  upVoteReport,
  downVoteReport,
  validateReport,
  setInappropriateReport,
  setInaccurateReport,
  getReportExtensionData
};
export default connect(
  select,
  reportsActionCreators
);
