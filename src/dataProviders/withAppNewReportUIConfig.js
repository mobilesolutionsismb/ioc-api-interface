import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getAppNewReportUiConfig } from '../modules/mobileApp';

const select = createStructuredSelector({
  newReportUiConfig: state => state.api_mobileApp.newReportUiConfig,
  newReportUiConfigUpdating: state => state.api_mobileApp.newReportUiConfigUpdating
});

export { select as newReportUISelector };
export const newReportUIActionCreators = {
  getAppNewReportUiConfig
};
export default connect(
  select,
  newReportUIActionCreators
);
