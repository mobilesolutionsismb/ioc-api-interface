import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  selectEvent,
  deselectEvent,
  activateEvent,
  deactivateEvent,
  mouseEnterEvent,
  mouseLeaveEvent
} from '../modules/emergencyEvents';

// Subset of selectedEmergency event, for backward compatibility or lighter components
const select = createStructuredSelector({
  emergencyEventsLastUpdated: state => state.api_emergencyEvents.emergencyEvents.lastUpdated,
  emergencyEventFeatures: state => state.api_emergencyEvents.emergencyEvents.items,
  emergencyEventFeatureAOIs: state => state.api_emergencyEvents.emergencyEvents.itemPolygons,
  // Hovered event
  hoveredEventId: state =>
    state.api_emergencyEvents.hoveredEvent
      ? state.api_emergencyEvents.hoveredEvent.properties.id
      : null,
  hoveredEvent: state => state.api_emergencyEvents.hoveredEvent,
  // Selected event
  selectedEventId: state =>
    state.api_emergencyEvents.selectedEvent
      ? state.api_emergencyEvents.selectedEvent.properties.id
      : null,
  selectedEvent: state => state.api_emergencyEvents.selectedEvent,
  // Active event
  activeEventId: state =>
    state.api_emergencyEvents.activeEvent
      ? state.api_emergencyEvents.activeEvent.properties.id
      : null,
  activeEvent: state => state.api_emergencyEvents.activeEvent,
  activeEventTW: state => state.api_emergencyEvents.activeEventTW,
  activeEventAOI: state => state.api_emergencyEvents.activeEventAOI
});

export { select as selectedEventSelector };

export const selectedEmergencyEventActionCreators = {
  selectEvent,
  deselectEvent,
  activateEvent,
  deactivateEvent,
  mouseEnterEvent,
  mouseLeaveEvent
};

export default connect(
  select,
  selectedEmergencyEventActionCreators
);
