import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getOrganizationUnitTeams } from '../modules/organizationUnits';

const select = createStructuredSelector({
  teams: state => state.api_organizationUnits.teams
});

export { select as organizationUnitsSelector };
export const organizationUnitsActionCreators = {
  getOrganizationUnitTeams
};

export default connect(
  select,
  organizationUnitsActionCreators
);
