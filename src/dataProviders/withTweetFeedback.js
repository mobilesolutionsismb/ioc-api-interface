import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as ActionCreators from '../modules/tweetFeedback';

const select = createStructuredSelector({
  tweetFeedbackList: state => state.api_tweetFeedback.tweetFeedbackList,
  generalSettingsUpdating: state => state.api_tweetFeedback.generalSettingsUpdating
});

export { select as tweetFeedbackSelector };
export const tweetFeedbackActionCreators = ActionCreators;
export default connect(
  select,
  tweetFeedbackActionCreators
);
