import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as ActionCreators from '../modules/liveParams';

const select = createStructuredSelector({
  liveAOI: state => state.api_liveParams.liveAOI,
  liveTW: state => state.api_liveParams.liveTW,
  liveTWPreferences: state => state.api_liveParams.liveTWPreferences,
  activeEventAOI: state => state.api_emergencyEvents.activeEventAOI,
  activeEventTW: state => state.api_emergencyEvents.activeEventTW,
  eventFetchingTW: state => state.api_liveParams.eventFetchingTW,
  eventFetchingTWPreferences: state => state.api_liveParams.eventFetchingTWPreferences
});

export { select as apiSelector };
export const apiSettingsActionCreators = ActionCreators;
export default connect(
  select,
  apiSettingsActionCreators
);
