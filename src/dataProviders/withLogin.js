import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  storeCredentials,
  login,
  logout,
  passwordForgot,
  register,
  updateUserProfilePicture
} from '../modules/authentication';

const select = createStructuredSelector({
  rememberCredentials: state => state.api_authentication.rememberCredentials,
  loggedIn: state => state.api_authentication.logged_in,
  user: state => state.api_authentication.user
});

export const loginActionCreators = {
  storeCredentials,
  login,
  logout,
  passwordForgot,
  register,
  updateUserProfilePicture
};

export { select as loginSelector };

export default connect(
  select,
  loginActionCreators
);
