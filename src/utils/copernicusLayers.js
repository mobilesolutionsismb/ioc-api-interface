import groupBy from 'lodash.groupby';
import wellknown from 'wellknown';
import bbox from '@turf/bbox';
import bboxPolygon from '@turf/bbox-polygon';
import { feature, featureCollection } from '@turf/helpers';

export const COPERNICUS_TASK_IDS = {
  delineation: [3102, 3104, 3111],
  grading: [3132, 3134, 3139, 3140, 3141],
  reference: [3164, 3167, 3171]
};
const COPERNICUS_TASK_IDS_LIST = [
  ...COPERNICUS_TASK_IDS.delineation,
  ...COPERNICUS_TASK_IDS.grading,
  ...COPERNICUS_TASK_IDS.reference
];
export const COPERNICUS_TYPES = Object.keys(COPERNICUS_TASK_IDS);

// -- Supposed to be + Database crap
export const COPERNICUS_HAZARD_DISPLAY_NAMES = [
  'Drought',
  'Epidemic',
  'Extreme temperature',
  'Humanitarian',
  'Infestation',
  'Mass movement',
  'Storm',
  'Transportation accident',
  'Volcanic activity',
  'Wildfire',
  'Forest fire, wild fire',
  'Flood',
  'Flood (Riverine flood)',
  'Wind storm',
  'Earthquake',
  'Industrial accident',
  'Other'
];

// function leadTimeSort(lcfg1, lcfg2) {
//   const l1 = new Date(lcfg1.leadTime);
//   const l2 = new Date(lcfg2.leadTime);
//   return l1 > l2;
// }

/**
 * Returns true if taskId is in the range of IDs of type
 * @param {string} type - (delineation|gradient|reference)
 * @param {string} taskId - task id
 * @returns boolean
 */
function _isCopernicusType(type, taskId) {
  const taskIdsSet = COPERNICUS_TASK_IDS[type];
  if (Array.isArray(taskIdsSet) && taskIdsSet.length > 0) {
    return taskIdsSet.indexOf(taskId) > -1;
  } else {
    return false;
  }
}

/**
 * Returns true if layer belongs to hazardLabel (has Hazard_Type === hazardLabel in extension data)
 * @param {string} hazardLabel - Copernicus hazard name
 * @param {Object} layer - layer config coming from API
 * @return boolean
 */
function _filterLayerByHazard(hazardLabel, layer) {
  return (
    layer.extensionData &&
    layer.extensionData.attributes &&
    layer.extensionData.attributes['Hazard_Type'] === hazardLabel
  );
}

/**
 * Groups layers by mapType given the taskId (which means type in delineation, grading, reference)
 * @param {Object} groupedByMapType - layerConfig to be generated (reduced)
 * @param {string} nextLayer - task ID (key of allLayers)
 * @returns reduced groupedByMapType
 */

function _groupCopernicusLayers(groupedByMapType, nextLayer) {
  for (const type of COPERNICUS_TYPES) {
    if (_isCopernicusType(type, nextLayer.taskId)) {
      groupedByMapType.children[type].layers = [
        ...groupedByMapType.children[type].layers,
        ...[nextLayer]
      ];
    }
  }

  return groupedByMapType;
}

/**
 * Define a primary object with each MapType classification
 * @param {string} hazardLabel - Copernicus hazard name
 * @param {Object} activationId - Code or activation id of a group of layers
 * @returns mapTypeGroup
 */
function _defineMapTypeGroups(hazardLabel, activationId) {
  return {
    delineation: {
      id: `${hazardLabel}.${activationId}.delineation`,
      hazard: hazardLabel,
      layerType: 'Delineation',
      code: activationId,
      layers: []
    },
    grading: {
      id: `${hazardLabel}.${activationId}.grading`,
      hazard: hazardLabel,
      layerType: 'Grading',
      code: activationId,
      layers: []
    },
    reference: {
      id: `${hazardLabel}.${activationId}.reference`,
      hazard: hazardLabel,
      layerType: 'Reference',
      code: activationId,
      layers: []
    }
  };
}

/**
 * Groups layers by hazard and then by code/`Activation_Id`
 * @param {string} hazardLabel - Copernicus hazard name
 * @param {Object} groupedByActivationId - map <code: string>: <Object<mapTypeGroup>>
 * @param {Object} nextLayer - <LayerConfig>
 * @returns reduced groupedByActivationId
 */
function _reduceByActivationId(hazardLabel, groupedByActivationId, nextLayer) {
  const filtered = _filterLayerByHazard(hazardLabel, nextLayer);
  if (filtered) {
    const activationId = nextLayer.extensionData.attributes.Activation_Id;
    if (!groupedByActivationId[activationId]) {
      //initial object with definition info
      const primaryObject = {
        displayName: nextLayer.extensionData.attributes.Activation_Title,
        children: _defineMapTypeGroups(hazardLabel, activationId)
      };
      groupedByActivationId[activationId] = _groupCopernicusLayers(primaryObject, nextLayer);
    }
    // when the object is already defined we'll only add items to layers array
    else {
      groupedByActivationId[activationId] = _groupCopernicusLayers(
        groupedByActivationId[activationId],
        nextLayer
      );
    }
  }
  return groupedByActivationId;
}

/**
 * Returns true if remappedLayerConfig contains no information
 * @param {Object} remappedLayerConfig - object with layers divided into delineation, gradient, reference
 * @returns {boolean}
 */
function _isEmptyLayers(remappedLayerConfig) {
  return Object.keys(remappedLayerConfig).length === 0;
}

/**
 * Map layers from GetLayers with Hazard labels
 * @param {Object} allLayers - map <taskId: string>: <Array<LayerConfig>>
 * @param {string} hazardDisplayName
 * @returns <Array<CopernicusGroup>> with meaningful information
 */
function _mapCopernicusLayersByHazard(allLayers, hazardDisplayName) {
  // A single group
  const groupedCopernicusLayersInitial = {};
  const layers = allLayers.reduce(
    _reduceByActivationId.bind(null, hazardDisplayName),
    groupedCopernicusLayersInitial
  ); //.sort((a, b) => a.metadataId - b.metadataId).sort(leadTimeSort);

  const isEmpty = _isEmptyLayers(layers); //clean out crap

  return isEmpty
    ? null
    : {
        displayName: hazardDisplayName,
        children: layers
      };
}

// Group by Activation_id, Location and Map_Type
function _groupByExtensionDataAttributes(layers) {
  return groupBy(
    layers,
    l =>
      l.extensionData
        ? `${l.extensionData.attributes.Activation_Id}-${l.extensionData.attributes.Location}-${
            l.extensionData.attributes.Map_Type
          }`
        : l.taskId
  );
}

// Merge several AOIs and return the bounding box including all of them
function _mergeAOIs(aoiPolygonArray) {
  return wellknown.stringify(bboxPolygon(bbox(featureCollection(aoiPolygonArray))));
}

// Merge grouped layer configurations, concatenating names and merging aoi
function _mergeGroupedLayerConfigurations(groupedLayersConfig) {
  const { taskId, start, end, leadTime, timespan, url, extensionData } = groupedLayersConfig[0]; // will be returned as taskId
  return {
    taskId,
    name: groupedLayersConfig.map(lc => lc.name).join(','),
    areaOfInterest: _mergeAOIs(
      groupedLayersConfig.map(lc => feature(wellknown.parse(lc.areaOfInterest)))
    ),
    metadataId: groupedLayersConfig.map(lc => lc.metadataId),
    metadataFileId: groupedLayersConfig.map(lc => lc.metadataFileId),
    start,
    end,
    leadTime,
    timespan,
    url,
    extensionData
  };
}

/**
 * Regroup copernicus layers by extension data merging those who match
 * Activation_id, Location e Publication_Date
 * @param {Array} copernicusLayers
 */
function _regroupByExtensionData(copernicusLayers) {
  return copernicusLayers.map(copernicusLayersItem => {
    const { displayName, children } = copernicusLayersItem;
    return {
      displayName,
      children: Object.keys(children).map(activation => {
        const activationChild = children[activation];
        const { displayName } = activationChild;
        const layerTypes = activationChild.children; // delineation, grading, reference
        return {
          displayName,
          children: Object.keys(layerTypes).reduce((prev, layerType) => {
            const grouped = _groupByExtensionDataAttributes(prev[layerType].layers);
            prev[layerType].layers = Object.keys(grouped).map(groupName =>
              _mergeGroupedLayerConfigurations(grouped[groupName])
            );
            return prev;
          }, layerTypes)
        };
      })
    };
  });
}

/**
 * Regroup Copernicus layer in a structure which is
 * <Array<CopernicusGroup>>
 * CopernicusGroup = {
 *    displayName: string // cop
 *    children: {
 *          code: {
 *              displayName: string
 *              children: {
 *                  delineation: {
 *                      name: string,
 *                      hazardType: string,
 *                      mapType: string,
 *                      code: string,
 *                      layers: <Array<LayerDefinition>
 *                  },
 *                  grading: {
 *                      name: string,
 *                      hazardType: string,
 *                      mapType: string,
 *                      code: string,
 *                      layers: <Array<LayerDefinition>
 *                  },
 *                  reference: {
 *                      name: string,
 *                      hazardType: string,
 *                      mapType: string,
 *                      code: string,
 *                      layers: <Array<LayerDefinition>
 *                  },
 *              }
 *          },
 *          code: {...},
 *          .
 *          .
 *          .
 *     }
 * }
 *
 * LayerDefinition = original layer definition from GetLayers API
 * @param {Object} layers - map <taskId: string>: <Array<LayerConfig>>
 * @returns <Array<CopernicusGroup>>
 */
function _groupCopernicus(layers) {
  const copernicusLayers = COPERNICUS_HAZARD_DISPLAY_NAMES.map(
    _mapCopernicusLayersByHazard.bind(null, layers)
  ).filter(layerConfig => layerConfig !== null);
  return _regroupByExtensionData(copernicusLayers);
}

/*
{
    "location":"Melsungen",
    "activation_title":"[EMSR276] Wind Storm Friederike in Central West Germany II",
    "activation_id":"EMSR276",
    "hazard_type":"Storm",
    "map_type":"Delineation Map",
    "publication_date":"2018-04-24 16:16:34",
 }
 */
// delineation, grading, reference (ireacttask), location, activation_id, publication_date

/**
 * LayerDefinition = original layer definition from GetLayers API
 * @param {Object} mapLayers - map <taskId: string>: <Array<LayerConfig>>
 * @returns {Object} mapLayers with copernicus layers separated in copernicusLayers variable
 */
export function separateCopernicusLayers(mapLayers) {
  return {
    layers: mapLayers.layers.filter(a => COPERNICUS_TASK_IDS_LIST.indexOf(a.taskId) === -1),
    copernicusLayers: _groupCopernicus(
      mapLayers.layers.filter(a => COPERNICUS_TASK_IDS_LIST.indexOf(a.taskId) > -1)
    ),
    availableTaskIds: mapLayers.availableTaskIds,
    baseUrl: mapLayers.baseUrl
  };
}
