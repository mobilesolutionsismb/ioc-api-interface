const mainStyle = 'color:  #00ACC1;';
const workerStyle = 'color:  #673AB7;';

const warnStyle = 'background: gold; color: black; font-weight: bold';
const severeWarningStyle = 'background: orangered; color: gold; font-weight: bold';
// type ConsoleType = 'log' | 'warn' | 'error' | 'debug';
const ConsoleTypes = ['log', 'warn', 'error', 'debug'];
/**
 *
 * @param {String} initialString
 * @param {String} style
 * @param {String} type value must be in ConsoleTypes
 */
function _logStyle(initialString, style, type = 'log') {
  if (ConsoleTypes.indexOf(type) === -1) {
    return () => {};
  }
  if (ENVIRONMENT === 'production') {
    return () => {};
  } else {
    return (function() {
      return Function.prototype.bind.call(console[type], console, initialString, style);
    })();
  }
}
// either lose log style or line no. - we choose the latter by using the code above

// function _logStyle(initialString, style, type = 'log') {
//   if (ConsoleTypes.indexOf(type) === -1) {
//     return;
//   }
//   if (ENVIRONMENT === 'production') {
//     return () => {};
//   } else {
//  return (...args) => {
//   let i = 0;
//   let initial = initialString;
//   while (i < args.length) {
//     if (typeof args[i] === 'string') {
//       initial += ` ${args[i]}`;
//       i++;
//     } else break;
//   }
//   const otherArgs = i > 0 ? args.slice(i) : args;
//    console[type].apply(console, [initial, style, ...otherArgs]);
//     };
//   }
// }

const logWorker = _logStyle('%c API [⛑️ Worker]:', workerStyle);
const logWarningWk = _logStyle('%c API [⛑️ Worker]:', warnStyle, 'warn');
const logSevereWarningWk = _logStyle('%c API [⛑️ Worker]:', severeWarningStyle, 'error');

const logMain = _logStyle('%c API [👑 Main]:', mainStyle);
const logWarning = _logStyle('%c API [👑 Main] Warning:', warnStyle, 'warn');
const logSevereWarning = _logStyle('%c API [👑 Main] ⚠️ Warning ⚠️:', severeWarningStyle, 'error');

export { logMain, logWarning, logSevereWarning, logWorker, logWarningWk, logSevereWarningWk };
