export function getJSONBlob(data) {
    return new Blob([JSON.stringify(data)], { type: 'application/json' });
}

export function clearDataURL(urlPointer) {
    if (urlPointer !== null) {
        URL.revokeObjectURL(urlPointer);
        urlPointer = null;
    }
    return urlPointer;
}

export function makeDataURL(urlPointer, data) {
    if (urlPointer) {
        urlPointer = clearDataURL(urlPointer);
    }
    if (data) {
        const blob = getJSONBlob(data);
        urlPointer = URL.createObjectURL(blob);
    }
    return urlPointer;
}