import { API } from './../api';
const path = require('path');
const lib = path.join(path.dirname(require.resolve('axios')), 'lib/adapters/http');
const http = require(lib);
// In JEST/Node
API.setAdapter(http);

const backend = process.env.IREACT_BACKEND;
const culture = 'en';

const credentials = {
  username: process.env.USERNAME,
  password: process.env.PASSWORD
};

function printError(error) {
  console.error(
    'FAIL',
    error.message,
    error.stack,
    error.details ? error.details.status : '',
    error.details ? error.details.statusText : ''
  );
}

const api = API.getInstance();
//const api = new API(backend, culture);

test('Constructor', () => {
  expect(api.baseURL).toBe(backend);
  expect(api.culture).toBe(culture);
  expect(api.token).toBe(null);
});

test('Test service unauthorized', async () => {
  try {
    const data = await api.test.getString();
    expect(data.result).toBe('not authenticated');
  } catch (error) {
    printError(error);
  }
});

test('Login & logout', async () => {
  try {
    const user = await api.login(credentials.username, credentials.password);
    expect(api.token).not.toBe(null);
    // console.log('User', user);

    const result = await api.logout();
    expect(api.token).toBe(null);
  } catch (error) {
    printError(error);
  }
});

test('Test service authorized', async () => {
  try {
    const user = await api.login(credentials.username, credentials.password);
    const data = await api.test.getAuthString();
    expect(data.result).toBe('authenticated');
  } catch (error) {
    printError(error);
  }
});

test('Get Enum by Name', async () => {
  try {
    const data = await api.enums.get('reportType');
    expect(data.result.values.damage).toBe(1);
  } catch (error) {
    printError(error);
  }
});

test('Get Enum List', async () => {
  try {
    const data = await api.enums.getEnums();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Report List and single report', async () => {
  try {
    const data = await api.report.getList({});
    expect(data.success).toBe(true);
    const firstReportFeature = data.result.featureCollection.features[0];
    expect(firstReportFeature.type).toBe('Feature');
    const reportData = await api.report.get(firstReportFeature.properties.id);
    expect(reportData.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Emergency Event List', async () => {
  try {
    const data = await api.emergencyEvent.getAll();

    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Create and Delete Emergency Event', async () => {
  try {
    const newEvent = {
      displayName: 'Test1',
      countryCodes: ['IT'],
      start: '2017-09-01T15:34:00+02:00',
      end: '2017-09-30T18:46:00+02:00',
      hazard: 'Flood',
      areaOfInterest: 'aaa',
      coordinates: 'aaaa',
      comment: 'Comment'
    };

    const data = await api.emergencyEvent.create(newEvent);

    const eventId = data.result.id;
    expect(eventId).toBeGreaterThan(0);

    const deleteData = await api.emergencyEvent.deleteEmergencyEvent(eventId);
    expect(deleteData.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Mission List', async () => {
  try {
    const data = await api.mission.getList();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Create and Delete Mission', async () => {
  try {
    const mission = {
      title: 'Mission test',
      start: '2017-06-07T08:40:47.421Z',
      end: '2017-06-08T08:40:47.422Z',
      status: 'inProgress',
      areaOfInterest: {
        north: 1,
        south: 2,
        east: 3,
        west: 4
      },
      emergencyEventId: 1,
      tasks: [
        {
          description: 'Task 1 of mission test',
          start: '2017-06-07T08:40:47.422Z',
          end: '2017-06-08T08:40:47.422Z',
          numberOfNecessaryPeople: 4,
          tools: [
            {
              description: 'tool description',
              quantity: 1
            }
          ]
        }
      ]
    };
    const data = await api.mission.create(mission);
    const missionId = data.result;
    expect(missionId).toBeGreaterThan(0);

    const deleteData = await api.mission.deleteMission(missionId);
    expect(deleteData.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Geocoding', async () => {
  const address = 'Via Roma';
  const culture = 'en';
  try {
    const data = await api.maps.geocode(address, culture);
    expect(data.result.resources[0].name).toBe('Via Roma, 43840 Salou (Tarragona), Spain');
  } catch (error) {
    printError(error);
  }
});

test('Reverse Geocoding', async () => {
  const latitude = 45;
  const longitude = 7;
  const culture = 'it';
  api.culture = culture;
  try {
    const data = await api.maps.reverseGeocode(latitude, longitude, culture);
    expect(data.result.resources[0].name).toBe('Fenestrelle, Piemonte, Italia');
  } catch (error) {
    printError(error);
  }
});

//Gamification
test('Leaderboard', async () => {
  try {
    const data = await api.gamification.getLeaderBoard();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Month Awards', async () => {
  try {
    const data = await api.gamification.getMonthlyAwards();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get My Skills And Achievements', async () => {
  try {
    const data = await api.gamification.getMySkillsAndAchievements();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get My Gamification Info', async () => {
  try {
    const data = await api.gamification.getMyScoresAndRanks();
    expect(data.success).toBe(false);
  } catch (error) {
    printError(error);
  }
});

test('Get Tip Of Quiz', async () => {
  try {
    const id = 30;
    const data = await api.gamification.getTipOfQuiz(id);
    expect(data.result.id).toBe(90);
  } catch (error) {
    printError(error);
  }
});

// test('Get Quiz Answers', async() => {
//     try {
//         const id = 2;
//         const data = await api
//             .gamification
//             .getQuizAnswers(id);
//         expect(data.result.items.length).toBe(4);
//     } catch (error) {
//         printError(error);
//     }
// });

test('Get Quizzes By Hazard', async () => {
  try {
    const data = await api.gamification.getQuizzesByHazard(5, 6, 1, false);
    expect(data.result.totalCount).toBe(6);
  } catch (error) {
    printError(error);
  }
});

test('Get Tips By Hazard', async () => {
  try {
    const data = await api.gamification.getTipsByHazard(6, 7, 'fire', true);
    expect(data.result.totalCount).toBe(7);
  } catch (error) {
    printError(error);
  }
});

test('Get Tips Progress', async () => {
  try {
    const data = await api.gamification.getTipsProgress();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Quizzes Progress', async () => {
  try {
    const data = await api.gamification.getQuizzesProgress();
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Set Tip As Read', async () => {
  try {
    const data = await api.gamification.setTipAsRead(5);
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Set Quiz As Answered', async () => {
  try {
    const data = await api.gamification.setQuizAsAnswered(12);
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Measures By Category', async () => {
  try {
    const data = await api.emergencyCommunication.getMeasuresByCategory('all');
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Report Request List', async () => {
  try {
    const filters = {
      status: 'closed',
      measureCategory: 'fire',
      contentType: 'people'
    };
    const data = await api.emergencyCommunication.getReportRequestList(6, 7, filters);
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});

test('Get Emergency Communications List', async () => {
  try {
    const filters = {
      status: 'closed',
      measureCategory: 'fire',
      contentType: 'people',
      type: 'reportRequest'
    };
    const data = await api.emergencyCommunication.getList(6, 7, null, filters);
    expect(data.success).toBe(true);
  } catch (error) {
    printError(error);
  }
});
