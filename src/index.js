export {
  API,
  DEFAULT_TIME_WINDOW,
  DEFAULT_PADDING_IN_FUTURE,
  VALID_TIME_UNITS,
  getLocalDate,
  localizeDate,
  hasPermissions,
  getUserTypeString,
  APIError,
  getBackendURL,
  getPresets,
  makeBoundsObject,
  padBoundingBox,
  TimeWindow
} from './api';

// TO BE REMOVED ASAP
export {
  apiCallActionCreatorFactory,
  dispatchIfAction,
  // Some inner functions that can be used inside some app or fe redux modules
  _refreshData,
  _updateReportsInner,
  _updateEmCommsInner,
  _updateMissionsInner,
  _updateAgentLocationsInner,
  _updateMapRequestsInner,
  _getLayersInner,
  setLiveAreaOfInterestFromUserPosition,
  setLiveTimeWindowExtent,
  setLiveTimeWindowWithDates,
  // MODULES - import all items of only one of these
  allModules,
  frontendModules,
  mobileAppModules
} from './modules';

export { makeDataURL, clearDataURL, getJSONBlob } from './utils/dataURL';

export * from './dataProviders';
