const NS = 'api:common';

// Common Actions that can be shared by all api reducers and AC
export const RESET_ALL = `${NS}@RESET_ALL`;