import {
    GET_CATEGORIES_SUCCESS
} from './Actions';
import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

export function getMeasuresByCategory(category = 'all', onLoadStart = null, onLoadEnd = null) {
    return apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyCommunication',
        'getMeasuresByCategory',
        '_get_measure_category_failure',
        (data) => {
            const categories = data.result.items;
            return Promise.resolve({
                type: GET_CATEGORIES_SUCCESS,
                categories
            });
        },
        true,
        category
    );
}