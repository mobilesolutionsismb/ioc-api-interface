import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { GET_CATEGORIES_SUCCESS } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [GET_CATEGORIES_SUCCESS]: {
    categories: action => action.categories
  }
};

export default reduceWith(mutator, State);
