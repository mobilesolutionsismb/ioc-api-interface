const state = {
  // (Point) features with itemType = mission
  missionFeaturesURL: null, // blob url
  missionTaskFeaturesURL: null, // blob url
  missionFeaturesStats: null,
  missions: [],
  // (Polygon/MultiPolygon) features with itemType = missionAOI
  missionPolygons: [],
  // (Point) features with itemType = missionTask
  missionTasks: [],
  // Last mission api call timestamp
  missionsLastUpdated: 0,
  // Set true when waiting for server response
  missionsUpdating: false,

  // Tasks are treated differently and separately from mission
  // Howevere deselecting a mission also deselect its selected task
  selectedMissionTask: null,
  // selectedMissionTaskId: -1,
  hoveredMissionTask: null
  // hoveredMissionTaskId: -1
};

export default state;
