import {
  MISSIONS_GETLIST_SUCCESS,
  MISSIONS_GETLIST_UPDATING,
  MISSIONS_GETLIST_FAIL,
  SELECT_TASK,
  DESELECT_TASK,
  MOUSEENTER_TASK,
  MOUSELEAVE_TASK
} from './Actions';

import {
  dispatchIfAction,
  apiCallActionCreatorFactory,
  attemptRelogin
} from '../../CommonActionCreators';

import { API } from '../../../api';
import { logSevereWarning } from '../../../utils/log';

const itemType = 'mission';

export function isMission(feature) {
  return feature && feature.properties.itemType === itemType;
}

export function isMissionTask(feature) {
  return feature && feature.properties.itemType === `${itemType}Task`;
}

const api = API.getInstance();

function _missionsUpdating() {
  return {
    type: MISSIONS_GETLIST_UPDATING
  };
}

function _missionsUpdated(featuresURL, missionTaskFeaturesURL, stats) {
  return {
    type: MISSIONS_GETLIST_SUCCESS,
    featuresURL,
    missionTaskFeaturesURL,
    stats
  };
}

function _missionsUpdateFailure() {
  return {
    type: MISSIONS_GETLIST_FAIL
  };
}

function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_missionsUpdateFailure());
  } else {
    const { featuresURL, missionTaskFeaturesURL, ...stats } = data;
    api.reduxStore.dispatch(_missionsUpdated(featuresURL, missionTaskFeaturesURL, stats));
  }
}

api.registerHandlerForItemType(itemType, _itemTypeHandler);

/**
 * Handle API Call
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {boolean} forceReload
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _updateMissionsInner(
  dispatch,
  getState,
  forceReload,
  onLoadStart,
  onLoadEnd
) {
  try {
    dispatch(_missionsUpdating());

    const state = getState();
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI) {
      dispatchIfAction(dispatch, onLoadEnd);
      return [];
    }
    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    const dateEnd = timeWindow.dateEnd;
    const lastUpdated = forceReload === true ? null : state.api_missions.missionsLastUpdated;

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      itemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated
    );

    return []; // for backward comp
  } catch (err) {
    dispatch(_missionsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `Cannot update ${itemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

/**
 * Action creator for reloading missions
 * @param {Boolean} forceReload - if false, differential update (default)
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function updateMissions(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateMissionsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}

/**
 * Action creator
 * Dispatch action for mission creation
 *
 * @param {String|Number} mission mission to create
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 */
export function createMission(mission, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          false,
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'create',
        '_mission_create_failure',
        done,
        true,
        mission
      )(dispatch, getState);
      return result;
    } catch (err) {
      throw err;
    }
  };
}

/**
 * Action creator
 * Dispatch action for mission creation
 *
 * @param {String|Number} mission mission to create
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 */
export function updateMission(mission, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          false,
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'update',
        '_mission_update_failure',
        done,
        true,
        mission
      )(dispatch, getState);
      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function deleteMission(missionId, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'deleteMission',
        '_mission_delete_failure',
        done,
        true,
        missionId
      )(dispatch, getState);
    } catch (err) {
      console.warn('Delete mission Error', err);
      throw err;
    }
    return response;
  };
}

export function deleteTaskOfMission(taskId, missionId, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'deleteTask',
        '_mission_delete_task_failure',
        done,
        true,
        taskId,
        missionId
      )(dispatch, getState);
    } catch (err) {
      console.warn('Delete task Error', err);
      throw err;
    }
    return response;
  };
}

export function releaseTaskOfMission(taskId, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'releaseTask',
        '_mission_release_task_failure',
        done,
        true,
        taskId
      )(dispatch, getState);
    } catch (err) {
      console.warn('Release task Error', err);
      throw err;
    }
    return response;
  };
}

export function addSingleTaskToMission(task, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'addSingleTaskToMission',
        '_add_single_task_to_mission_failure',
        done,
        true,
        task
      )(dispatch, getState);
    } catch (err) {
      console.warn('Add single task Error', err);
      throw err;
    }
    return response;
  };
}

export function accomplishTask(taskId, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'accomplishTask',
        '_accomplish_task_failure',
        done,
        true,
        taskId
      )(dispatch, getState);
    } catch (err) {
      console.warn('Accomplish task Error', err);
      throw err;
    }
    return response;
  };
}

export function takeChargeOfTask(taskId, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const done = async () => {
        // Force reload
        const result = await _updateMissionsInner(
          dispatch,
          getState,
          true, // quick and dirty workaround for updates
          onLoadStart,
          onLoadEnd
        );
        return result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'mission',
        'takeChargeOfTask',
        '_take_charge_of_task_failure',
        done,
        true,
        taskId
      )(dispatch, getState);
    } catch (err) {
      console.warn('Take charge of task Error', err);
      throw err;
    }
    return response;
  };
}
