import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  MISSIONS_GETLIST_SUCCESS,
  MISSIONS_GETLIST_UPDATING,
  MISSIONS_GETLIST_FAIL,
  SELECT_TASK,
  DESELECT_TASK,
  MOUSEENTER_TASK,
  MOUSELEAVE_TASK
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [MISSIONS_GETLIST_SUCCESS]: {
    missionFeaturesURL: action => action.featuresURL,
    missionTaskFeaturesURL: action => action.missionTaskFeaturesURL,
    missionFeaturesStats: action => action.stats,
    missions: action => action.items,
    missionPolygons: action => action.itemAOIs,
    missionTasks: action => action.tasks, // peculiarity!
    missionsLastUpdated: () => Date.now(),
    missionsUpdating: false
  },
  [MISSIONS_GETLIST_UPDATING]: {
    missionsUpdating: true
  },
  [MISSIONS_GETLIST_FAIL]: {
    missionsUpdating: false
  },
  [SELECT_TASK]: {
    // Mission of the selected task, if any
    selectedMissionTask: action => action.task
  },
  [DESELECT_TASK]: {
    // Mission of the selected task, if any
    selectedMissionTask: null
  },
  [MOUSEENTER_TASK]: {
    // Mission of the hovered task, if any
    hoveredMissionTask: action => action.task
  },
  [MOUSELEAVE_TASK]: {
    // Mission of the hovered task, if any
    hoveredMissionTask: null
  }
};

export default reduceWith(mutator, State);
