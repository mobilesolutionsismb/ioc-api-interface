import Reducer from './dataSources/Reducer';
export default Reducer;

export {
    _updateAgentLocationsInner, updateAgentLocations
} from './dataSources/ActionCreators';