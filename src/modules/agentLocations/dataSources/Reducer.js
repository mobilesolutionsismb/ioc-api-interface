import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  AGENT_LOCATIONS_GETLIST_SUCCESS,
  AGENT_LOCATIONS_GETLIST_UPDATING,
  AGENT_LOCATIONS_GETLIST_FAIL
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [AGENT_LOCATIONS_GETLIST_SUCCESS]: {
    agentLocationFeaturesURL: action => action.featuresURL,
    agentLocationFeaturesStats: action => action.stats,
    agentLocations: action => action.items,
    agentLocationsLastUpdated: () => Date.now(),
    agentLocationsUpdating: false
  },
  [AGENT_LOCATIONS_GETLIST_UPDATING]: {
    agentLocationsUpdating: true
  },
  [AGENT_LOCATIONS_GETLIST_FAIL]: {
    agentLocationsUpdating: false
  }
};

export default reduceWith(mutator, State);
