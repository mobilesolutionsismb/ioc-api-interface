const state = {
  agentLocationFeaturesURL: null, // blob url
  agentLocationFeaturesStats: null,
  // (Point) features with itemType = agentLocation
  agentLocations: [],
  // Last agent locations api call timestamp
  agentLocationsLastUpdated: 0,
  // Set true when waiting for server response
  agentLocationsUpdating: false
};

export default state;
