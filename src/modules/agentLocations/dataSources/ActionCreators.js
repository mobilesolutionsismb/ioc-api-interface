import { feature } from '@turf/helpers';
import { dispatchIfAction, attemptRelogin } from '../../CommonActionCreators';

import {
  AGENT_LOCATIONS_GETLIST_SUCCESS,
  AGENT_LOCATIONS_GETLIST_UPDATING,
  AGENT_LOCATIONS_GETLIST_FAIL
} from './Actions';
import { logSevereWarning } from '../../../utils/log';
import { API } from '../../../api';
import moment from 'moment';

const itemType = 'agentLocation';

export function isAgentLocation(feature) {
  return feature && feature.properties.itemType === itemType;
}

const api = API.getInstance();

function _agentLocationsUpdating() {
  return {
    type: AGENT_LOCATIONS_GETLIST_UPDATING
  };
}

function _agentLocationsUpdated(featuresURL, stats) {
  return {
    type: AGENT_LOCATIONS_GETLIST_SUCCESS,
    featuresURL,
    stats
  };
}

function _agentLocationsUpdateFailure() {
  return {
    type: AGENT_LOCATIONS_GETLIST_FAIL
  };
}

function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_agentLocationsUpdateFailure());
  } else {
    const { featuresURL, ...stats } = data;
    api.reduxStore.dispatch(_agentLocationsUpdated(featuresURL, stats));
  }
}

api.registerHandlerForItemType(itemType, _itemTypeHandler);

/**
 * Handle API Call
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {boolean} forceReload
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _updateAgentLocationsInner(
  dispatch,
  getState,
  forceReload,
  onLoadStart,
  onLoadEnd
) {
  try {
    dispatch(_agentLocationsUpdating());

    const state = getState();
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI) {
      dispatchIfAction(dispatch, onLoadStart);
      return [];
    }

    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    const dateEnd = timeWindow.dateEnd;
    const lastUpdated = forceReload
      ? null
      : moment()
          .subtract(DEFAULT_DIFF.amount, DEFAULT_DIFF.unit)
          .toISOString();

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      itemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated
    );

    return []; // for backward comp
  } catch (err) {
    dispatch(_agentLocationsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `Cannot update ${itemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

const DEFAULT_DIFF = {
  // get locations in the last 6 hours
  amount: 6,
  unit: 'months'
};

/**
 * Transform server items to GeoJSON features if needed
 * add missing fields
 * tune existing fields
 * convert wkt geometries
 * @param {Object} itemFromServer - item object from server
 */
function _remapAgentLocations(itemFromServer) {
  // Only constraints are feature.property.itemType MUST be itemType and geometry MUST be a Point
  try {
    // Rename locationLastModificationTime to lastModified
    const { locationLastModificationTime, ...properties } = itemFromServer.properties;
    const lastModified = new Date(locationLastModificationTime);

    return feature(itemFromServer.geometry, { ...properties, lastModified, itemType });
  } catch (err) {
    logSevereWarning(
      `Cannot remap Agent Location with id ${itemFromServer.id}:`,
      err,
      itemFromServer
    );
    // console.log(`%c(!) Cannot remap Agent Location with id ${itemFromServer.id}:`, 'color: gold; background-color: orangered', err, itemFromServer);
    return null;
  }
}

/**
 * Format items and merge them if needed
 * @param {boolean} forceReload
 * @param {Array} agentLocationsFromServer
 * @param {Array} currentAgentLocations
 */
// function _preprocessAgentLocations(forceReload, agentLocationsFromServer, currentAgentLocations) {
//   const nextAgentLocations = agentLocationsFromServer
//     .map(_remapAgentLocations)
//     .filter(it => it !== null); // avoid surprises (e.g. dirty data)

//   return forceReload ? nextAgentLocations : mergeData(currentAgentLocations, nextAgentLocations);
// }

/**
 * Action creator for reloading agent locations
 * @param {Boolean} forceReload - if false, differential update (default)
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function updateAgentLocations(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateAgentLocationsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}
