import {
    ROLES_GETLIST_SUCCESS
} from './Actions';

import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

export function getRoles(onLoadStart = null, onLoadEnd = null) {
    return async (dispatch, getState) => {
        let response = null;
        try {
            response = await apiCallActionCreatorFactory(
                onLoadStart,
                onLoadEnd,
                'enums',
                'getRoles',
                '_enums_getRoles_failure',
                (data) => {
                    const roles = data.result.items;
                    return Promise.resolve({
                        type: ROLES_GETLIST_SUCCESS,
                        roles
                    });
                },
                true
            )(dispatch, getState);
        }
        catch (err) {
            console.warn('Get Roles error', err);
        }
        return response;
    };
}