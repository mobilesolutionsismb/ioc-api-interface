import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { ROLES_GETLIST_SUCCESS } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [ROLES_GETLIST_SUCCESS]: {
    roles: action => action.roles
  }
};

export default reduceWith(mutator, State);
