import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { TWEET_FEEDBACK_UPDATING, TWEET_FEEDBACK_SUCCESS, TWEET_FEEDBACK_ERROR } from './Actions';

const mutator = {
  // Event Hovering
  [TWEET_FEEDBACK_UPDATING]: {
    generalSettingsUpdating: true
  },
  [TWEET_FEEDBACK_SUCCESS]: {
    tweetFeedbackList: (action, state) => {
      let tweetList = state.tweetFeedbackList;
      let newTweet = {
        properties: action.properties,
        tweetId: action.tweetId,
        userId: action.userId
      };
      tweetList[action.tweetId] = newTweet;
      return tweetList;
    }
  },
  // Event Selection
  [TWEET_FEEDBACK_ERROR]: {
    generalSettingsUpdating: false
  }
};

export default reduceWith(mutator, State);
