const NS = 'api::tweetFeedback';

// Emergency Events Loading and Selection
export const TWEET_FEEDBACK_UPDATING = `${NS}@TWEET_FEEDBACK_UPDATING`;
export const TWEET_FEEDBACK_SUCCESS = `${NS}@TWEET_FEEDBACK_SUCCESS`;
export const TWEET_FEEDBACK_ERROR = `${NS}@TWEET_FEEDBACK_ERROR`;
