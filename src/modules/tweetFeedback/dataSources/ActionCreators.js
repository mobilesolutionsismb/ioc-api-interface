import { TWEET_FEEDBACK_UPDATING, TWEET_FEEDBACK_SUCCESS, TWEET_FEEDBACK_ERROR } from './Actions';

import { apiCallActionCreatorFactory } from '../../CommonActionCreators';
import { API } from '../../../api';
const api = API.getInstance();

export function getTweetFeedback(onLoadStart = null, onLoadEnd = null, tweetId, userId) {
  return async (dispatch, getState) => {
    dispatch({
      type: TWEET_FEEDBACK_UPDATING
    });
    try {
      const res = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'tweetFeedback',
        'getTweetFeedbackById',
        '_tweet_feedback_get_tweet_feedback_failure',
        data => {
          const tweet = data.result;
          return Promise.resolve({
            type: TWEET_FEEDBACK_SUCCESS,
            tweet
          });
        },
        true,
        tweetId,
        userId
      )(dispatch, getState);

      return res;
    } catch (err) {
      dispatch({
        type: TWEET_FEEDBACK_ERROR
      });
      throw err;
    }
  };
}

export function updateTweetFeedback(
  tweetId,
  userId,
  properties = {},
  feedbackValues = null,
  onLoadStart = null,
  onLoadEnd = null
) {
  return async (dispatch, getState) => {
    dispatch({
      type: TWEET_FEEDBACK_UPDATING
    });
    try {
      const response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'tweetFeedback',
        'updateFeedbackById',
        '_tweet_feedback_update_feedback_by_id',
        _updateTweetFeedBackList(userId, tweetId, properties),
        true,
        tweetId,
        userId,
        properties,
        feedbackValues
      )(dispatch, getState);
      return response;
    } catch (err) {
      dispatch({
        type: TWEET_FEEDBACK_ERROR
      });
      console.warn('Update report Request Error', err);
      throw err;
    }
  };
}

function _updateTweetFeedBackList(userId, tweetId, properties) {
  return async () => {
    console.log(properties);
    return {
      type: TWEET_FEEDBACK_SUCCESS,
      userId,
      tweetId,
      properties
    };
  };
}
