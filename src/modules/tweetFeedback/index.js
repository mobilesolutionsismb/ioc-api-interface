import Reducer from './dataSources/Reducer';
export default Reducer;

export { getTweetFeedback, updateTweetFeedback } from './dataSources/ActionCreators';
