import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  REMEMBER_CREDENTIALS,
  CLEAR_CREDENTIALS,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTRATION_SUCCESS,
  UPDATE_PROFILE_PICTURE
} from './Actions';
import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [REMEMBER_CREDENTIALS]: {
    rememberCredentials: true
  },
  [CLEAR_CREDENTIALS]: {
    rememberCredentials: false,
    user: (action, state) => {
      const u = { ...state.user };
      delete u.password;
      return u;
    }
  },
  [LOGIN_SUCCESS]: {
    logged_in: true,
    user: action => action.user
  },
  [LOGOUT_SUCCESS]: {
    logged_in: false,
    user: null
  },
  [REGISTRATION_SUCCESS]: {
    logged_in: true,
    user: action => action.user
  },
  [UPDATE_PROFILE_PICTURE]: {
    user: (action, state) => ({ ...state.user, userPicture: action.userPicture })
  }
};

export default reduceWith(mutator, State);
