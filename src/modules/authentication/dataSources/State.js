const state = {
    // User authentication settings
    rememberCredentials: true,
    logged_in: false,
    user: null,
    // Test auth type
    // test_string: ''
};

export default state;