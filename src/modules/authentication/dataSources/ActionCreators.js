import {
  REMEMBER_CREDENTIALS,
  CLEAR_CREDENTIALS,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTRATION_SUCCESS,
  UPDATE_PROFILE_PICTURE
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

import { dispatchIfAction, apiCallActionCreatorFactory } from '../../CommonActionCreators';
import { API } from '../../../api';
const api = API.getInstance();

// ------------------------------------------------------------------------------------------------------------------------------
// AUTHENTICATION, LOGIN, REGISTRATION
// ------------------------------------------------------------------------------------------------------------------------------

/**
 * Action creator
 * Dispatch action for remembering or storing user credentials (password)
 * @param {Boolean} storeCredentials (default false)
 */
export function storeCredentials(storeCredentials = false) {
  api.rememberCredentials = storeCredentials;
  return storeCredentials
    ? {
        type: REMEMBER_CREDENTIALS
      }
    : {
        type: CLEAR_CREDENTIALS
      };
}

/**
 * Action creator
 * Dispatch action for login
 *
 * @param {String} username username (email)
 * @param {String} password password
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 */
export function login(username, password, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      dispatchIfAction(dispatch, onLoadStart);
      const state = getState();
      if (state.locale && state.locale.locale) {
        api.culture = state.locale.locale;
      }
      const user = await api.login(username, password, '_login_failure');
      api.rememberCredentials = state.api_authentication.rememberCredentials;

      dispatch({
        type: LOGIN_SUCCESS,
        user
      });

      dispatchIfAction(dispatch, onLoadEnd);
      return user;
    } catch (apiError) {
      //The only two different are login and logout
      dispatch({
        type: LOGOUT_SUCCESS
      });
      dispatchIfAction(dispatch, onLoadEnd);
      throw apiError;
    }
  };
}

/**
 * Action creator
 * Dispatch action for logout
 *
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 */
export function logout(onLoadStart = null, onLoadEnd = null) {
  return async dispatch => {
    try {
      dispatchIfAction(dispatch, onLoadStart);
      const result = await api.logout();
      dispatch({
        type: LOGOUT_SUCCESS
      });
      dispatchIfAction(dispatch, onLoadEnd);
      dispatch({
        type: RESET_ALL
      });
      return result;
    } catch (apiError) {
      //The only two different are login and logout
      dispatchIfAction(dispatch, onLoadEnd);
      throw apiError;
    }
  };
}

/**
 *
 * @param {*} name
 * @param {*} surname
 * @param {*} user
 * @param {*} email
 * @param {*} phone
 * @param {*} password
 * @param {*} captchaResponse
 * @param {*} onLoadStart
 * @param {*} onLoadEnd
 */
export function register(
  name,
  surname,
  user,
  email,
  phone,
  password,
  captchaResponse,
  onLoadStart = null,
  onLoadEnd = null
) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'account',
    'register',
    '_registration_failure',
    response => {
      return Promise.resolve({
        type: REGISTRATION_SUCCESS,
        user: response.data
      });
    },
    false,
    name,
    surname,
    user,
    email,
    phone,
    password,
    captchaResponse
  );
}

/**
 *
 * @param {*} email
 * @param {*} onLoadStart
 * @param {*} onLoadEnd
 */
export function passwordForgot(email, onLoadStart = null, onLoadEnd = null) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'account',
    'passwordForgot',
    '_password_forgot_failure',
    response => {
      return Promise.resolve(response);
    },
    false,
    email
  );
}

/**
 * Changes user profile picture
 * @param {Number} userPicture - int representing an avatar index
 * @param {*} onLoadStart
 * @param {*} onLoadEnd
 */
export function updateUserProfilePicture(userPicture, onLoadStart = null, onLoadEnd = null) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'profile',
    'updateUserPicture',
    '_update_picture_failure',
    () => {
      return Promise.resolve({
        type: UPDATE_PROFILE_PICTURE,
        userPicture
      });
    },
    false,
    userPicture
  );
}
