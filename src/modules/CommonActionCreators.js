import isPojo from 'is-pojo';
import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from './authentication/dataSources/Actions';
import { API, APIError } from '../api';
import { logMain, logWarning, logSevereWarning } from '../utils/log';
import { _refreshData } from './_refreshData';

const api = API.getInstance();

//---- HELPERS (private)

/**
 * Dispatch action only if action is a function or a pojo
 *
 * @param {Function} dispatch function from redux
 * @param {Function|Object} action redux action
 */
export function dispatchIfAction(dispatch, action) {
  if (typeof action === 'function') {
    const resultAction = action();
    // Try to unwrap
    dispatchIfAction(dispatch, resultAction);
  } else if (isPojo(action) && typeof action.type === 'string') {
    dispatch(action);
  }
}

/**
 * Private function
 * Attempt relogin using async/await, gathering credentials from current state
 * (if they were stored setting rememberCredentials = true)
 *
 * @param {Function} getState function from redux which returns state
 * @return {Object} if rememberCredentials, result of login (user), else result of logout
 */
async function _relogin(getState) {
  const authState = getState().api_authentication;
  if (authState.user === null) {
    // we did logout intentionally
    return null;
  }
  console.warn('Relogin attempt...');
  let user = null;
  try {
    if (authState.rememberCredentials) {
      const { emailAddress, password } = authState.user;
      logWarning('Attempt relogin because of 401 (potential session expired)');
      // console.warn('%c Attempt relogin because of 401 (potential session expired)', 'background: gold; color: black');
      user = await api.login(emailAddress, password, '_relogin_failure');
      //			user['password'] = password;
    } else {
      logSevereWarning('Logout because of 401 (potential session expired)');
      // console.warn('%c Logout because of 401 (potential session expired)', 'background: orangered; color: gold');
      user = await api.logout();
    }
    return user;
  } catch (apiError) {
    throw apiError;
  }
}

const MAX_ATTEMPTS = 4;

export async function attemptRelogin(/* error,  */ dispatch, getState) {
  let user = null;
  try {
    user = await _relogin(getState);
  } catch (ex) {
    logWarning('Relogin attempt failed', ex);
    user = null;
  }
  if (user === null) {
    dispatch({
      type: LOGOUT_SUCCESS
    });
    return false;
  } else if (user instanceof APIError) {
    dispatch({
      type: LOGOUT_SUCCESS
    });
    return false;
  } else {
    dispatch({
      type: LOGIN_SUCCESS,
      user
    });
    return true;
  }
}

/**
 * Factory function for action creators making an API call.
 * If authenticated is true, it extracts the token from the state,
 * attempting relogin if state.api_authentication.storeCredentials = true and a 401 is returned from the backend
 *
 * IMPORTANT
 * getAction is a (optionally declared async) callback which takes the api success response as input,
 * and the token (if it needs to perform more authenticated calls)
 * and MUST return the proper action wrapped in a Promise, e.g. Promise.resolve
 *
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 * @param {String} apiName api module to be called (e.g. report)
 * @param {String} apiMethodName api method to be called (e.g. getList)
 * @param {String} failureMessage optional failure message
 * @param {Function} getAction resolves the action from the api response
 * @param {Boolean} authenticated (default false)
 * @param {...} args other optional arguments that will be passed to the apiMethodName. If authenticated, token is appended at the end and relogin can be performed
 */
export function apiCallActionCreatorFactory(
  onLoadStart,
  onLoadEnd,
  apiName,
  apiMethodName,
  failureMessage,
  getAction,
  authenticated = false,
  ...args
) {
  return async (dispatch, getState) => {
    let count = 0;
    dispatchIfAction(dispatch, onLoadStart);
    const callApi = async count => {
      if (count > MAX_ATTEMPTS) {
        throw new Error(
          `Maximum consecutive attempts (${MAX_ATTEMPTS}) for api.${apiName}.${apiMethodName} reached.`
        );
      } else {
        try {
          logMain(`attempt ${count} to call: api.${apiName}.${apiMethodName}`);
          // console.log(`%c attempt ${count} to call: api.${apiName}.${apiMethodName}`, 'color: black; background: gold');
          const apiMethod = api[apiName][apiMethodName];
          const state = getState();
          if (state.locale && state.locale.locale) {
            api.culture = state.locale.locale;
          }
          const apiResponse = await apiMethod.apply(this, [...args, failureMessage]);
          const action = await getAction(apiResponse);
          dispatchIfAction(dispatch, action);
          return apiResponse.result;
        } catch (e) {
          throw e;
        }
      }
    };

    let response = null;
    try {
      response = await callApi(count++);
      dispatchIfAction(dispatch, onLoadEnd);
    } catch (error) {
      let failure = error;
      if (authenticated && failure.details && failure.details.status === 401) {
        // console.warn(`api.${apiName}.${apiMethodName}: got 401! call relogin`, failure);
        let user = null;
        try {
          user = await _relogin(getState);
        } catch (ex) {
          logWarning('Relogin attempt failed', ex);
          // console.warn('Relogin attempt failed', ex);
          failure = ex;
          user = null;
        }
        if (user === null) {
          dispatch({
            type: LOGOUT_SUCCESS
          });
        } else if (user instanceof APIError) {
          failure = user;
          dispatch({
            type: LOGOUT_SUCCESS
          });
        } else {
          failure = null;
          dispatch({
            type: LOGIN_SUCCESS,
            user
          });
          // retry - should have a max attempts
          try {
            response = await callApi(count++);
            dispatchIfAction(dispatch, onLoadEnd);
          } catch (error) {
            failure = error;
            dispatch({
              type: LOGOUT_SUCCESS
            });
            dispatchIfAction(dispatch, onLoadEnd);
            throw failure;
          }
        }
      }
      if (failure) {
        // console.warn(`api.${apiName}.${apiMethodName}: API REQUEST FAILED`, failure);
        dispatchIfAction(dispatch, onLoadEnd);
        throw failure;
      }
    }
    return response;
  };
}

// Update all IREACT features, optionally forcing reload - much like _refreshData but only for features
export function updateIREACTFeatures(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    await _refreshData(dispatch, getState, onLoadStart, onLoadEnd, forceReload, false); //do not get layers
  };
}
