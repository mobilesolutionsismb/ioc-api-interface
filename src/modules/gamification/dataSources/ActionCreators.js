import {
	GAMIFICATION_UPDATE_PROFILE,
	GAMIFICATION_UPDATE_AWARDS,
	GAMIFICATION_UPDATE_LEADERBOARD,
	GAMIFICATION_UPDATE_SKILLS,
} from './Actions';
import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

// ------------------------------------------------------------------------------------------------------------------------------
// GAMIFICATION
// ------------------------------------------------------------------------------------------------------------------------------

export function updateScores(onLoadStart = null, onLoadEnd = null) {
	return apiCallActionCreatorFactory(
		onLoadStart,
		onLoadEnd,
		'gamification',
		'getMyScoresAndRanks',
		'_gamification_scores_update_failure',
		(data) => {
			const gamificationProfile = { profile: data.result };
			return Promise.resolve({
				type: GAMIFICATION_UPDATE_PROFILE,
				gamificationProfile
			});
		},
		true
	);
}

export function updateSkillsAndAchievements(onLoadStart = null, onLoadEnd = null) {
	return apiCallActionCreatorFactory(
		onLoadStart,
		onLoadEnd,
		'gamification',
		'getMySkillsAndAchievements',
		'_gamification_skills_and_achievements_update_failure',
		(data) => {
			const gamificationSkills = { skillsAndAchievements: data.result };
			return Promise.resolve({
				type: GAMIFICATION_UPDATE_SKILLS,
				gamificationSkills
			});
		},
		true
	);
}

export function updateMonthlyAwards(onLoadStart = null, onLoadEnd = null) {
	return apiCallActionCreatorFactory(
		onLoadStart,
		onLoadEnd,
		'gamification',
		'getMonthlyAwards',
		'_gamification_monthly_awards_update_failure',
		(data) => {
			const gamificationAwards = { monthlyAwards: data.result };
			return Promise.resolve({
				type: GAMIFICATION_UPDATE_AWARDS,
				gamificationAwards
			});
		},
		true
	);
}

/**
 * Update leaderboard
 * @param {Number} SkipCount - page index
 * @param {Number} MaxResultCount - page size
 * @param {Boolean} loadMore - if false, override the current state, else add topContributors
 * @param {Function} onLoadStart 
 * @param {Function} onLoadEnd 
 */
export function updateLeaderBoard(SkipCount = 0, MaxResultCount = 10, loadMore = true, onLoadStart = null, onLoadEnd = null) {
	return async (dispatch, getState) => {
		const state = getState();
		const currentLB = state.api_gamification.gamification.leaderboard;
		// console.warn('CURRENT LEADERBOARD', currentLB);
		let response = null;
		try {
			response = await apiCallActionCreatorFactory(
				onLoadStart,
				onLoadEnd,
				'gamification',
				'getLeaderBoard',
				'_gamification_leaderboard_update_failure',
				(data) => {
					const newLBData = data.result;
					const gamificationLeaderboard = {
						leaderboard: loadMore ? {
							players: newLBData.players,
							topContributors: {
								items: [...currentLB.topContributors.items, ...newLBData.topContributors.items],
								totalCount: newLBData.topContributors.totalCount + currentLB.topContributors.totalCount
							},
							aroundMyRank: newLBData.aroundMyRank
						} : newLBData
					};

					return Promise.resolve({
						type: GAMIFICATION_UPDATE_LEADERBOARD,
						gamificationLeaderboard
					});
				},
				true,
				SkipCount,
				MaxResultCount
			)(dispatch, getState);
		}
		catch (err) {
			console.warn('Cannot update leaderboard', err);
		}
		return response;
	};
}
