import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  GAMIFICATION_UPDATE_PROFILE,
  GAMIFICATION_UPDATE_AWARDS,
  GAMIFICATION_UPDATE_SKILLS,
  GAMIFICATION_UPDATE_LEADERBOARD
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [GAMIFICATION_UPDATE_PROFILE]: {
    gamification: (action, state) => ({ ...state.gamification, ...action.gamificationProfile })
  },
  [GAMIFICATION_UPDATE_AWARDS]: {
    gamification: (action, state) => ({ ...state.gamification, ...action.gamificationAwards })
  },
  [GAMIFICATION_UPDATE_SKILLS]: {
    gamification: (action, state) => ({ ...state.gamification, ...action.gamificationSkills })
  },
  [GAMIFICATION_UPDATE_LEADERBOARD]: {
    gamification: (action, state) => ({ ...state.gamification, ...action.gamificationLeaderboard })
  }
};

export default reduceWith(mutator, State);
