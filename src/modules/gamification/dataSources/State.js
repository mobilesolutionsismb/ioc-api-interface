const state = {
  gamification: {
    profile: {
      rank: -1,
      score: -1,
      currentLevel: '',
      nextLevel: '',
      pointsToNextLevel: -1
    },
    monthlyAwards: {
      socializer: [],
      reporter: [],
      reviewer: []
    },
    skillsAndAchievements: {
      skills: {},
      achievements: {},
      unlockedContents: []
    },
    leaderboard: {
      players: 0,
      topContributors: {
        items: [],
        totalCount: 0
      },
      aroundMyRank: {
        items: [],
        totalCount: 0
      }
    }
  }
};

export default state;
