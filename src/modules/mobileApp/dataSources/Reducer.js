import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  NEWREPORT_UI_GET_SUCCESS,
  NEWREPORT_UI_GET_UPDATING,
  NEWREPORT_UI_GET_UPDATE_ERROR
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [NEWREPORT_UI_GET_SUCCESS]: {
    newReportUiConfig: action => action.newReportUiConfig,
    newReportUiConfigUpdating: false
  },
  [NEWREPORT_UI_GET_UPDATING]: {
    newReportUiConfigUpdating: true
  },
  [NEWREPORT_UI_GET_UPDATE_ERROR]: {
    newReportUiConfigUpdating: false
  }
};

export default reduceWith(mutator, State);
