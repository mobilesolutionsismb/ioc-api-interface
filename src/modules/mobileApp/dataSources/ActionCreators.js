import {
    NEWREPORT_UI_GET_SUCCESS,
    NEWREPORT_UI_GET_UPDATE_ERROR,
    NEWREPORT_UI_GET_UPDATING
}from './Actions';

import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

export function getAppNewReportUiConfig(onLoadStart = null, onLoadEnd = null) {
	return async (dispatch, getState) => {
		dispatch({ type: NEWREPORT_UI_GET_UPDATING });
		try {
			const response = await apiCallActionCreatorFactory(
				onLoadStart,
				onLoadEnd,
				'report',
				'getUIConfig',
				'_app_newreport_ui_getconfig_failure',
				(data) => {
					const newReportUiConfig = data.result;
					return Promise.resolve({
						type: NEWREPORT_UI_GET_SUCCESS,
						newReportUiConfig
					});
				},
				true
			)(dispatch, getState);
			return response;
		} catch (err) {
			dispatch({ type: NEWREPORT_UI_GET_UPDATE_ERROR });
			throw err;
		}
	};

}