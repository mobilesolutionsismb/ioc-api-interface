import Reducer from './dataSources/Reducer';
export default Reducer;

export {
  getQuizzesByHazard,
  getQuizzesProgress,
  getTipOfQuiz,
  getTipsByHazard,
  getTipsProgress,
  setTipAsRead,
  setQuizAsAnswered
} from './dataSources/ActionCreators';
