const state = {
    tipsAndQuizzes: {
        tips: [],
        quizzes: [],
        tipsProgress: [],
        quizzesProgress: [],
        selectedQuizAnswers: []
    }
};

export default state;