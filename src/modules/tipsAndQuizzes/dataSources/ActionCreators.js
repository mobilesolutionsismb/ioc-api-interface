import {
  TIPS_QUIZZES_GETTIP_SUCCESS,
  TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS,
  TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS,
  TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS,
  TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS
} from './Actions';
import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

// ------------------------------------------------------------------------------------------------------------------------------
// TIPS AND QUIZZES - from push and/or signalR
// ------------------------------------------------------------------------------------------------------------------------------

export function getTipOfQuiz(Id, onLoadStart = null, onLoadEnd = null) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'gamification',
    'getTipOfQuiz',
    '_tips_quizzes_getTip_failure',
    data => {
      const tip = data.result;
      return Promise.resolve({
        type: TIPS_QUIZZES_GETTIP_SUCCESS,
        tip
      });
    },
    true,
    Id
  );
}

export function getQuizzesByHazard(
  SkipCount,
  MaxResultCount,
  Hazard,
  Completed,
  onLoadStart = null,
  onLoadEnd = null
) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'gamification',
    'getQuizzesByHazard',
    '_tips_quizzes_getQuizzesByHazard_failure',
    data => {
      const quizzes = data.result.items;
      return Promise.resolve({
        type: TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS,
        quizzes
      });
    },
    true,
    SkipCount,
    MaxResultCount,
    Hazard,
    Completed
  );
}

export function getTipsByHazard(
  SkipCount,
  MaxResultCount,
  Hazard,
  Completed,
  onLoadStart = null,
  onLoadEnd = null
) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'gamification',
    'getTipsByHazard',
    '_tips_quizzes_getTipsByHazard_failure',
    data => {
      const tips = data.result.items;
      return Promise.resolve({
        type: TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS,
        tips
      });
    },
    true,
    SkipCount,
    MaxResultCount,
    Hazard,
    Completed
  );
}

export function getTipsProgress(onLoadStart = null, onLoadEnd = null) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'gamification',
    'getTipsProgress',
    '_tips_quizzes_getTipsProgress_failure',
    data => {
      const tipsProgress = data.result.items;
      return Promise.resolve({
        type: TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS,
        tipsProgress
      });
    },
    true
  );
}

export function getQuizzesProgress(onLoadStart = null, onLoadEnd = null) {
  return apiCallActionCreatorFactory(
    onLoadStart,
    onLoadEnd,
    'gamification',
    'getQuizzesProgress',
    '_tips_quizzes_getQuizzesProgress_failure',
    data => {
      const quizzesProgress = data.result.items;
      return Promise.resolve({
        type: TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS,
        quizzesProgress
      });
    },
    true
  );
}

export function setTipAsRead(id, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = await apiCallActionCreatorFactory(
      onLoadStart,
      onLoadEnd,
      'gamification',
      'setTipAsRead',
      '_tips_quizzes_setTipAsRead_failure',
      data => {
        return data.success;
      },
      true,
      id
    )(dispatch, getState);
    return response;
  };
}

export function setQuizAsAnswered(id, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = await apiCallActionCreatorFactory(
      onLoadStart,
      onLoadEnd,
      'gamification',
      'setQuizAsAnswered',
      '_tips_quizzes_setQuizAsAnswered_failure',
      data => {
        return data.success;
      },
      true,
      id
    )(dispatch, getState);
    return response;
  };
}
