import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  TIPS_QUIZZES_GETTIP_SUCCESS,
  TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS,
  TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS,
  TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS,
  TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [TIPS_QUIZZES_GETTIP_SUCCESS]: {
    tipsAndQuizzes: (action, state) => {
      let tips = [];
      tips.push(action.tip);
      return {
        ...state.tipsAndQuizzes,
        tips
      };
    }
  },
  [TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS]: {
    tipsAndQuizzes: (action, state) => {
      let quizzes = [];
      quizzes = action.quizzes;
      return {
        ...state.tipsAndQuizzes,
        quizzes
      };
    }
  },
  [TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS]: {
    tipsAndQuizzes: (action, state) => {
      let tips = [];
      tips = action.tips;
      return {
        ...state.tipsAndQuizzes,
        tips
      };
    }
  },
  [TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS]: {
    tipsAndQuizzes: (action, state) => {
      let tipsProgress = [];
      tipsProgress = action.tipsProgress;
      return {
        ...state.tipsAndQuizzes,
        tipsProgress
      };
    }
  },
  [TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS]: {
    tipsAndQuizzes: (action, state) => {
      let quizzesProgress = [];
      quizzesProgress = action.quizzesProgress;
      return {
        ...state.tipsAndQuizzes,
        quizzesProgress
      };
    }
  }
};

export default reduceWith(mutator, State);
