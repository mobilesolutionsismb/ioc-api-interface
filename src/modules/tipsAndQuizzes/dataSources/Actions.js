const NS = 'api:tips';

export const TIPS_QUIZZES_GETTIP_SUCCESS = `${NS}@TIPS_QUIZZES_GETTIP_SUCCESS`;
export const TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS = `${NS}@TIPS_QUIZZES_GETQUIZZESBYHAZARD_SUCCESS`;
export const TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS = `${NS}@TIPS_QUIZZES_GETTIPSBYHAZARD_SUCCESS`;
export const TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS = `${NS}@TIPS_QUIZZES_GETTIPSPROGRESS_SUCCESS`;
export const TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS = `${NS}@TIPS_QUIZZES_GETQUIZZESPROGRESS_SUCCESS`;
