import Reducer from './dataSources/Reducer';
export default Reducer;

export {
    _getLayersInner,
    getLayers
} from './dataSources/ActionCreators';