import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { MAP_GET_LAYERS_SUCCESS, MAP_GET_LAYERS_UPDATING, MAP_GET_LAYERS_ERROR, GET_COPERNICUS_HAZARD } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [MAP_GET_LAYERS_SUCCESS]: {
    mapLayers: action => action.mapLayers,
    mapLayersUpdating: false
  },
  [MAP_GET_LAYERS_UPDATING]: {
    mapLayersUpdating: true
  },
  [MAP_GET_LAYERS_ERROR]: {
    mapLayersUpdating: false
  },
  [GET_COPERNICUS_HAZARD]: {
    copernicusHazardNames: action => action.names
  }
};

export default reduceWith(mutator, State);
