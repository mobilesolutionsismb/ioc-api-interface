import {
  // EMERGENCY_EVENT_GETLAYERS_SUCCESS,
  MAP_GET_LAYERS_SUCCESS,
  MAP_GET_LAYERS_UPDATING,
  MAP_GET_LAYERS_ERROR,
  GET_COPERNICUS_HAZARD
} from './Actions';
import { apiCallActionCreatorFactory, dispatchIfAction } from '../../CommonActionCreators';
import {
  separateCopernicusLayers,
  COPERNICUS_HAZARD_DISPLAY_NAMES
} from '../../../utils/copernicusLayers';
// ------------------------------------------------------------------------------------------------------------------------------
// MAP LAYERS
// ------------------------------------------------------------------------------------------------------------------------------

export function getLayers(onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    const response = await _getLayersInner(dispatch, getState, onLoadStart, onLoadEnd);
    return response;
  };
}

export async function _getLayersInner(dispatch, getState, onLoadStart, onLoadEnd) {
  dispatch({
    type: MAP_GET_LAYERS_UPDATING
  });
  dispatch({
    type: GET_COPERNICUS_HAZARD,
    names: COPERNICUS_HAZARD_DISPLAY_NAMES
  });
  const state = getState();
  const hasAOI =
    state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
  if (!hasAOI) {
    dispatchIfAction(dispatch, onLoadEnd);
    return Promise.resolve({
      mapLayers: {
        availableTaskIds: [],
        layers: [],
        copernicusLayers: []
      }
    });
  }

  try {
    // Get aoi and time window from event if selected, otherwise from "live" settings
    const bboxString = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI.wktString
      : state.api_liveParams.liveAOI.wktString;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    // There are no layers in the future and the BE returns inconsistent data, so we limit the dataEnd to now()
    const dateEnd = new Date(Math.min(new Date(), new Date(timeWindow.dateEnd))).toISOString(); // timeWindow.dateEnd;
    const response = await apiCallActionCreatorFactory(
      onLoadStart,
      onLoadEnd,
      'maps',
      'getLayers',
      '_map_layer_get_layers_failure',
      data => {
        const mapLayers = separateCopernicusLayers(data.result);
        return Promise.resolve({
          type: MAP_GET_LAYERS_SUCCESS,
          mapLayers
        });
      },
      true,
      dateStart,
      dateEnd,
      bboxString
    )(dispatch, getState);
    return response;
  } catch (err) {
    dispatch({
      type: MAP_GET_LAYERS_ERROR
    });
    console.warn('Cannot load Layers', err);
    throw err;
  }
}
