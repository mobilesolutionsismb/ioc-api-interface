const state = {
	// layers: [],
	mapLayers: {
		availableTaskIds: [],
		layers: [],
		copernicusLayers: []
	},
	copernicusHazardNames: [],
	// Set true when waiting for server response
	mapLayersUpdating: false
};

export default state;