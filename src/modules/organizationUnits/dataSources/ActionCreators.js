import {
    ORG_UNIT_GET_TEAMS  
 } from './Actions';
import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

export function getOrganizationUnitTeams(orgUnitIds, onLoadStart = null, onLoadEnd = null) {
    return apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'organizationUnit',
        'getOrganizationUnitTeams',
        '_orgUnit_getTeams_failure',
        (data) => {
            const teams = data.result;
            return Promise.resolve({
                type: ORG_UNIT_GET_TEAMS,
                teams
            });
        },
        true,
        orgUnitIds
    );
}