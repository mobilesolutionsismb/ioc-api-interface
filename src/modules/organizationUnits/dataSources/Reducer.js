import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { ORG_UNIT_GET_TEAMS } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [ORG_UNIT_GET_TEAMS]: {
    teams: action => action.teams
  }
};

export default reduceWith(mutator, State);
