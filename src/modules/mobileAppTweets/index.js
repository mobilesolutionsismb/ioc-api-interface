import Reducer from './dataSources/Reducer';
export default Reducer;
export { SUPPORTED_LANGUAGES as supportedLanguages } from './dataSources/State';
export {
  _updateTweetsInner,
  updateTweets,
  setTweetFetchingLimit,
  setTweetFetchingTimespan,
  setTweetFetchingLanguage,
  sendTweetFeedback
} from './dataSources/ActionCreators';
