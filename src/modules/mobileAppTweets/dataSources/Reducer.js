import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  TWEETS_GETLIST_FAIL,
  TWEETS_GETLIST_UPDATING,
  TWEETS_GETLIST_SUCCESS,
  SET_TWEETS_TIME_FETCHING_TIMESPAN,
  SET_TWEETS_FETCHING_LIMIT,
  SET_TWEETS_FETCHING_LANGUAGE
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [TWEETS_GETLIST_SUCCESS]: {
    tweetFeaturesURL: action => action.featuresURL,
    tweetFeaturesStats: action => action.stats,
    tweets: action => action.items,
    tweetsLastUpdated: () => Date.now(),
    tweetsUpdating: false
  },
  [TWEETS_GETLIST_UPDATING]: {
    tweetsUpdating: true
  },
  [TWEETS_GETLIST_FAIL]: {
    tweetsUpdating: false
  },
  [SET_TWEETS_TIME_FETCHING_TIMESPAN]: {
    tweetsFetchingTimespanHours: action => action.timespan
  },
  [SET_TWEETS_FETCHING_LIMIT]: {
    tweetsFetchingLimit: action => action.limit
  },
  [SET_TWEETS_FETCHING_LANGUAGE]: {
    tweetsFetchingLanguage: action => action.language
  }
};

export default reduceWith(mutator, State);
