// APP ONLY
const DEFAULT_TWEETS_LIMIT = 300;
const DEFAULT_HOURS_BACK = 24;
export const SUPPORTED_LANGUAGES = ['any', 'en', 'it', 'es', 'de', 'fr', 'fi'];
// const browserLanguage = navigator.language.substr(0, 2);
// const DEFAULT_LANGUAGE = SUPPORTED_LANGUAGES.includes(browserLanguage) ? browserLanguage : 'en';
const DEFAULT_LANGUAGE = 'any';

const state = {
  tweetsFeaturesURL: null, // blob url
  tweetsFeaturesStats: null,
  // (Point) features with itemType = tweets
  tweets: [],
  // Last agent locations api call timestamp
  tweetsLastUpdated: 0,
  // Set true when waiting for server response
  tweetsUpdating: false,
  // Get tweets in the latest tweet fetching timespan in hours (e.g 24 -> 24hours)
  tweetsFetchingTimespanHours: DEFAULT_HOURS_BACK,
  // up to max tweetsFetchingLimit tweets
  tweetsFetchingLimit: DEFAULT_TWEETS_LIMIT,
  tweetsFetchingLanguage: DEFAULT_LANGUAGE
};

export default state;
