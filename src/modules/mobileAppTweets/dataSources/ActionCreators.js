import {
  TWEETS_GETLIST_FAIL,
  TWEETS_GETLIST_UPDATING,
  TWEETS_GETLIST_SUCCESS,
  SET_TWEETS_TIME_FETCHING_TIMESPAN,
  SET_TWEETS_FETCHING_LIMIT,
  SET_TWEETS_FETCHING_LANGUAGE
} from './Actions';
import { logSevereWarning } from '../../../utils/log';
import { API } from '../../../api';
import { dispatchIfAction, attemptRelogin } from '../../CommonActionCreators';
import moment from 'moment';

const itemType = 'tweet';

const SOCIAL_API_DATETIME_FORMAT = 'YYYYMMDDHHmm';

const api = API.getInstance();

function _tweetsUpdating() {
  return {
    type: TWEETS_GETLIST_UPDATING
  };
}

function _tweetsUpdated(featuresURL, stats) {
  return {
    type: TWEETS_GETLIST_SUCCESS,
    featuresURL,
    stats
  };
}

function _tweetsUpdateFailure() {
  return {
    type: TWEETS_GETLIST_FAIL
  };
}

function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_tweetsUpdateFailure());
  } else {
    const { featuresURL, ...stats } = data;
    api.reduxStore.dispatch(_tweetsUpdated(featuresURL, stats));
  }
}

api.registerHandlerForItemType(itemType, _itemTypeHandler);

export async function _updateTweetsInner(
  dispatch,
  getState,
  forceReload = false,
  onLoadStart = null,
  onLoadEnd = null
) {
  try {
    dispatch(_tweetsUpdating());
    const state = getState();
    const { user, logged_in } = state.api_authentication;
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI || !logged_in) {
      dispatchIfAction(dispatch, onLoadStart);
      return [];
    }

    const {
      tweetsFetchingTimespanHours,
      tweetsFetchingLimit,
      tweetsFetchingLanguage
    } = state.api_mobileApp_tweets;

    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const now = moment();
    const dateEnd = now.format(SOCIAL_API_DATETIME_FORMAT);
    const dateStart = now
      .clone()
      .subtract(tweetsFetchingTimespanHours, 'hours')
      .format(SOCIAL_API_DATETIME_FORMAT);
    const lastUpdated =
      forceReload === true
        ? null
        : moment(state.api_reports.tweetsLastUpdated).format(SOCIAL_API_DATETIME_FORMAT);

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      itemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated,
      `${user.userType === 'citizen' ? user.id : -1}`,
      tweetsFetchingLanguage,
      tweetsFetchingLimit
    );
    return []; // for backward comp
  } catch (err) {
    dispatch(_tweetsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `Cannot update ${itemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

export function updateTweets(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateTweetsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}

/**
 * Send a user feedback to a tweet and reload the tweets list on success
 *
 * @export
 * @param {Object} originalTweet - contains all original tweet properties
 * @param {Object} feedbackValues - contains all changes to the tweet
 * @param {Function} [onLoadStart=null]
 * @param {Function} [onLoadEnd=null]
 * @returns
 */
export function sendTweetFeedback(
  tweetProperties,
  feedbackValues = null,
  onLoadStart = null,
  onLoadEnd = null
) {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const { user, logged_in } = state.api_authentication;
      if (!logged_in) {
        throw new Error('User is not logged in');
      }
      if (!tweetProperties || !tweetProperties.tweetId) {
        throw new Error('Missing tweet properties');
      }
      const { tweetId, tweetOrigin } = tweetProperties;
      if (tweetOrigin === 'professional') {
        throw new Error(`Tweet ${tweetId} has been already validated`);
      }
      const userId = `${user.id}`;
      const feedbackResult = await api.tweetFeedback.updateFeedbackById(
        tweetId,
        userId,
        tweetProperties,
        feedbackValues
      );
      const result = await _updateTweetsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
      return { tweets: result, ...feedbackResult.result };
    } catch (err) {
      throw err;
    }
  };
}

function _setTweetFetchingTimeSpan(timespan) {
  return {
    type: SET_TWEETS_TIME_FETCHING_TIMESPAN,
    timespan
  };
}

function _setTweetFetchingLimit(limit) {
  return {
    type: SET_TWEETS_FETCHING_LIMIT,
    limit
  };
}

function _setTweetFetchingLanguage(language) {
  return {
    type: SET_TWEETS_FETCHING_LANGUAGE,
    language
  };
}

export function setTweetFetchingTimespan(timespan = 24, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    dispatch(_setTweetFetchingTimeSpan(timespan));
    try {
      const result = await _updateTweetsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function setTweetFetchingLimit(limit = 300, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    dispatch(_setTweetFetchingLimit(limit));
    try {
      const result = await _updateTweetsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function setTweetFetchingLanguage(language, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    if (!language) {
      throw new Error(
        'A language must be a 2 character string identifying one of the supported	ISO 639-1 Codes'
      );
    }
    dispatch(_setTweetFetchingLanguage(language));
    try {
      const result = await _updateTweetsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
      return result;
    } catch (err) {
      throw err;
    }
  };
}
