import Reducer from './dataSources/Reducer';
export default Reducer;

export {
    _updateReportsInner, isReport, updateReports,
    deselectReport, selectReport,
    mouseLeaveReport, mouseEnterReport,
    setInaccurateReport, setInappropriateReport, validateReport,
    upVoteReport, downVoteReport,
    getReportExtensionData,
    toggleImageDialog, updateVisiblePoints
} from './dataSources/ActionCreators';