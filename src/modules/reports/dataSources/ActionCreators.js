import {
  REPORTS_GETLIST_SUCCESS,
  REPORT_GETLIST_UPDATING,
  REPORT_GETLIST_FAIL,
  TOGGLE_IMAGE_DIALOG
} from './Actions';

import {
  dispatchIfAction,
  apiCallActionCreatorFactory,
  attemptRelogin
} from '../../CommonActionCreators';
import { API } from '../../../api';
import { logSevereWarning } from '../../../utils/log';

const itemType = 'report';

export function isReport(feature) {
  return feature && feature.properties.itemType === itemType;
}

const api = API.getInstance();

function _reportsUpdating() {
  return {
    type: REPORT_GETLIST_UPDATING
  };
}

function _reportsUpdated(featuresURL, stats) {
  return {
    type: REPORTS_GETLIST_SUCCESS,
    featuresURL,
    stats
    // items: []
  };
}

function _reportsUpdateFailure() {
  return {
    type: REPORT_GETLIST_FAIL
  };
}

function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_reportsUpdateFailure());
  } else {
    const { featuresURL, ...stats } = data;
    api.reduxStore.dispatch(_reportsUpdated(featuresURL, stats));
  }
}

api.registerHandlerForItemType(itemType, _itemTypeHandler);

/**
 * Handle API Call
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {boolean} forceReload
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _updateReportsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd) {
  try {
    dispatch(_reportsUpdating());

    const state = getState();
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI) {
      dispatchIfAction(dispatch, onLoadEnd);
      return [];
    }
    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    const dateEnd = timeWindow.dateEnd;
    const lastUpdated = forceReload === true ? null : state.api_reports.reportsLastUpdated;

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      itemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated
    );

    return []; // for backward comp
  } catch (err) {
    dispatch(_reportsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `Cannot update ${itemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

/**
 * Action creator for reloading reports
 * @param {Boolean} forceReload - if false, differential update (default)
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function updateReports(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateReportsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}

function _actionCreatorForReportVoteOrValidationFactory(
  apiName,
  apiErrorString,
  reportId,
  onLoadStart = null,
  onLoadEnd = null
) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateReportsInner(dispatch, getState, false, onLoadStart, onLoadEnd);
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'report',
        apiName,
        apiErrorString,
        done,
        true,
        reportId
      )(dispatch, getState);
      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function upVoteReport(reportId, onLoadStart = null, onLoadEnd = null) {
  return _actionCreatorForReportVoteOrValidationFactory(
    'upVote',
    '_report_upvote_failure',
    reportId,
    onLoadStart,
    onLoadEnd
  );
}

export function downVoteReport(reportId, onLoadStart = null, onLoadEnd = null) {
  return _actionCreatorForReportVoteOrValidationFactory(
    'downVote',
    '_report_downvote_failure',
    reportId,
    onLoadStart,
    onLoadEnd
  );
}

export function validateReport(reportId, onLoadStart = null, onLoadEnd = null) {
  return _actionCreatorForReportVoteOrValidationFactory(
    'validate',
    '_report_validate_failure',
    reportId,
    onLoadStart,
    onLoadEnd
  );
}

export function setInappropriateReport(reportId, onLoadStart = null, onLoadEnd = null) {
  return _actionCreatorForReportVoteOrValidationFactory(
    'setInappropriate',
    '_report_setInappropiate_failure',
    reportId,
    onLoadStart,
    onLoadEnd
  );
}

export function setInaccurateReport(reportId, onLoadStart = null, onLoadEnd = null) {
  return _actionCreatorForReportVoteOrValidationFactory(
    'setInaccurate',
    '_report_setInaccurate_failure',
    reportId,
    onLoadStart,
    onLoadEnd
  );
}

export function toggleImageDialog(imageDialogVisible) {
  return { type: TOGGLE_IMAGE_DIALOG, imageDialogVisible };
}

export function getReportExtensionData(reportIds, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    let response = null;
    try {
      const onApiCallSuccess = data => {
        return data.result;
      };

      response = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'report',
        'getReportExtensionData',
        '_report_getReportExtensionData_failure',
        onApiCallSuccess,
        true,
        reportIds
      )(dispatch, getState);
    } catch (err) {
      logSevereWarning('Cannot get report extension data', err);
      // console.warn('Cannot get report extension data', err);
    }
    return response;
  };
}
