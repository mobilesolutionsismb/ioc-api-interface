const NS = 'api::reports';

// Fetch reports
export const REPORTS_GETLIST_SUCCESS = `${NS}@REPORTS_GETLIST_SUCCESS`;
export const REPORT_GETLIST_UPDATING = `${NS}@REPORT_GETLIST_UPDATING`;
export const REPORT_GETLIST_FAIL = `${NS}@REPORT_GETLIST_FAIL`;

// Service actions
// export const UPDATE_POINTS = `${NS}@UPDATE_POINTS`;
export const TOGGLE_IMAGE_DIALOG = `${NS}@TOGGLE_IMAGE_DIALOG`;
