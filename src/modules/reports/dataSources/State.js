const state = {
    // (Point) features with itemType = report
    reportFeaturesURL: null, // blob url
    reportFeaturesStats: null,

    reports: [],
    // Last reports api call timestamp
    reportsLastUpdated: 0,
    // Set true when waiting for server response
    reportsUpdating: false,
    // Service properties for map displaying
    // reportSinglePoints: [],
    reportImageDialogVisible: false
};

export default state;