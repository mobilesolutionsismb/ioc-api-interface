import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  REPORTS_GETLIST_SUCCESS,
  REPORT_GETLIST_UPDATING,
  REPORT_GETLIST_FAIL
  // UPDATE_POINTS,
  // TOGGLE_IMAGE_DIALOG
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [REPORTS_GETLIST_SUCCESS]: {
    reportFeaturesURL: action => action.featuresURL,
    reportFeaturesStats: action => action.stats,

    // reports: action => action.items,
    reportsLastUpdated: action => action.stats.lastModificationTime,
    reportsUpdating: false
  },
  [REPORT_GETLIST_UPDATING]: {
    reportsUpdating: true
  },
  [REPORT_GETLIST_FAIL]: {
    reportsUpdating: false
  }
};

export default reduceWith(mutator, State);
