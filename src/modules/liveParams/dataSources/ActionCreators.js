import {
    SET_LIVE_TW,
    SET_LIVE_AOI,
    SET_EVENT_FETCHING_TW
} from './Actions';
import {
    _refreshData
} from '../../_refreshData';
import {
    _getEventsListInner
} from '../../emergencyEvents';

import {
    EUROPE_BBOX_WKT,
    DEFAULT_PADDING_IN_FUTURE,
    TimeWindow,
    makeBoundsObject,
    getFeatureAreaOfInterestFromPointPosition
} from '../../../api';

// ------------------------------------------------------------------------------------------------------------------------------
// API Settings for requesting data from the backend
// ------------------------------------------------------------------------------------------------------------------------------

// LIVE mode - live mode prefs cannot be unset
// APP ONLY - THIS MUST BE SET from the app after first position acquiring

/**
 * @param {Object} userPosition - Geolocation
 * @param {Number} radius - radius in Km of a circle around pointFeatureLike
 * @param {String} units - 'kilometres' or 'miles' (@see turf js doc)
 */
export function setLiveAreaOfInterestFromUserPosition(userPosition, radius, units, onLoadStart = null, onLoadEnd = null) {
    return async(dispatch, getState) => {
        const liveAOI = getFeatureAreaOfInterestFromPointPosition(userPosition, radius, units);
        // Set new AOI
        dispatch({
            type: SET_LIVE_AOI,
            liveAOI
        });
        try {
            await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
        } catch (e) {
            throw e;
        }
        return liveAOI;
    };
}

/**
 * Set authority AOI from server-based params
 */
export function setAuthorityUserAreaOfInterest(onLoadStart, onLoadEnd) {
    return async(dispatch, getState) => {
        const state = getState().api_authentication;
        if (state.user || state.user.userType !== 'authority') {
            // TODO make sure organization is in user when it will be available
            const hasOrganization = Array.isArray(state.user.organizationUnits) && state.user.organizationUnits.length > 0;
            let aoiWKT = null;
            if (hasOrganization) {
                // TODO make sure the field name is correct when it will be available
                aoiWKT = state.user.organizationUnits[0].areaOfInterest;
            } else {
                aoiWKT = EUROPE_BBOX_WKT;
            }
            const liveAOI = makeBoundsObject(aoiWKT);
            dispatch({
                type: SET_LIVE_AOI,
                liveAOI
            });
            try {
                await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
            } catch (e) {
                throw e;
            }
            return liveAOI;
        } else {
            throw new Error('Invalid user: ' + JSON.stringify(state.user));
        }
    };
}

/**
 * Set time window for live events. Must be called at least daily
 * @param {Number} quantity - strictly positive number, amount
 * @param {String} unit - time unit ('days', 'weeks', 'months', 'years'...) @see momentjs
 * @param {Function} onLoadStart - callback on load end
 * @param {Function} onLoadEnd - callback on load end
 */
export function setLiveTimeWindowExtent(quantity, unit, paddingInTheFuture = DEFAULT_PADDING_IN_FUTURE, onLoadStart = null, onLoadEnd = null) {
    return async(dispatch, getState) => {
        const liveTW = (new TimeWindow(null, null, {
            quantity,
            unit
        }, false, paddingInTheFuture)).toPojo();
        dispatch({
            type: SET_LIVE_TW,
            quantity,
            unit,
            liveTW
        });
        try {
            await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
        } catch (e) {
            throw e;
        }
    };
}

/**
 * @param {Date|String|Moment} dateStart - start date of the TW
 * @param {Date|String|Moment} dateEnd - end date of the TW
 * @param {Function} onLoadStart - callback on load end
 * @param {Function} onLoadEnd - callback on load end
 */
export function setLiveTimeWindowWithDates(dateStart = null, dateEnd = null, onLoadStart, onLoadEnd) {
    return async(dispatch, getState) => {
        if (!dateEnd && !dateStart) {
            return;
        }
        const {
            liveTWPreferences,
            liveTW
        } = getState().api_liveParams;
        const {
            quantity,
            unit
        } = liveTWPreferences;
        const dStart = dateStart ? dateStart : liveTW.dateStart;
        const dEnd = dateEnd ? dateEnd : liveTW.dateEnd;
        // Using true means we are setting exact dates
        const _liveTW = (new TimeWindow(dStart, dEnd, {
            quantity,
            unit
        }, true)).toPojo();
        dispatch({
            type: SET_LIVE_TW,
            liveTW: _liveTW
        });

        try {
            await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
        } catch (e) {
            throw e;
        }
    };
}

/**
 * Set preferences for loading the event list (time extend)
 * The AOI depends on the liveAOI
 * @param {*} quantity 
 * @param {*} unit 
 * @param {*} onLoadStart 
 * @param {*} onLoadEnd 
 */
export function setEventSelectionTimeWindowExtent(quantity, unit, onLoadStart, onLoadEnd) {
    return async(dispatch, getState) => {
        const state = getState().api_liveParams;
        const eventFetchingTWPreferences = state.eventFetchingTWPreferences;
        if (eventFetchingTWPreferences.quantity !== quantity || eventFetchingTWPreferences.unit !== unit) {
            const eventFetchingTW = (new TimeWindow(null, null, {
                quantity,
                unit
            })).toPojo();
            dispatch({
                type: SET_EVENT_FETCHING_TW,
                quantity,
                unit,
                eventFetchingTW
            });

            try {
                const status = state.emergencyEventsStatusFilter;
                const sorting = state.emergencyEventsSorting;
                await _getEventsListInner(dispatch, getState, 0, 10, sorting, status, onLoadStart, onLoadEnd); // Reload
            } catch (e) {
                throw e;
            }
        }
    };

}