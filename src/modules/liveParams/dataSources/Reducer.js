import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { SET_LIVE_TW, SET_LIVE_AOI, SET_EVENT_FETCHING_TW } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [SET_LIVE_TW]: {
    liveTWPreferences: (action, state) => {
      const { quantity, unit } = action;
      return { ...state.liveTWPreferences, quantity, unit };
    },
    liveTW: action => action.liveTW
  },
  [SET_LIVE_AOI]: {
    liveAOI: action => action.liveAOI
  },

  // Set time window width for loading events
  // Change event selection tw (dateStart, dateEnd)
  [SET_EVENT_FETCHING_TW]: {
    eventFetchingTWPreferences: (action, state) => {
      const { quantity, unit } = action;
      return { ...state.eventFetchingTWPreferences, quantity, unit };
    },
    eventFetchingTW: action => action.eventFetchingTW
  }
};
export default reduceWith(mutator, State);
