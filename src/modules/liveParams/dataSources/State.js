import { getPresets, DEFAULT_PADDING_IN_FUTURE, TimeWindow } from '../../../api';

const presets = getPresets();

const DEFAULT_LIVE_TW_PREF =
  presets === 'mobile-app' ? { quantity: 1, unit: 'months' } : { quantity: 7, unit: 'days' };
const DEFAULT_LIVE_TW =
  presets === 'mobile-app'
    ? // in the mobile app we have to look a bit into in the future in order to load future alerts
      new TimeWindow(null, null, DEFAULT_LIVE_TW_PREF, false, DEFAULT_PADDING_IN_FUTURE).toPojo()
    : new TimeWindow(null, null, DEFAULT_LIVE_TW_PREF).toPojo();

const DEFAULT_EVT_SEL_TW_PREF = { quantity: 2, unit: 'years' };
const DEFAULT_EVT_TW = new TimeWindow(null, null, DEFAULT_EVT_SEL_TW_PREF).toPojo();

/**
 * API Settings which control the parameters for fetching geospatial data
 */
const state = {
  //Setting that affect almost any api getting data from the server
  // "Live" mode settings
  liveAOI: null,
  liveTW: DEFAULT_LIVE_TW,
  liveTWPreferences: DEFAULT_LIVE_TW_PREF,
  // Event selection settings
  eventFetchingTW: DEFAULT_EVT_TW,
  eventFetchingTWPreferences: DEFAULT_EVT_SEL_TW_PREF
};

export default state;
