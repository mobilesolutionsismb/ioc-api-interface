const NS = 'api:liveParams';

// AOI = AreaOfInterest
// TW = TimeWindow
export const SET_LIVE_TW = `${NS}@SET_LIVE_TW`;
export const SET_LIVE_AOI = `${NS}@SET_LIVE_AOI`;
export const SET_EVENT_FETCHING_TW = `${NS}@SET_EVENT_FETCHING_TW`;