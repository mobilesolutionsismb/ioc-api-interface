import Reducer from './dataSources/Reducer';
export default Reducer;

export {
    setAuthorityUserAreaOfInterest, setEventSelectionTimeWindowExtent,
    setLiveAreaOfInterestFromUserPosition, setLiveTimeWindowExtent, setLiveTimeWindowWithDates
} from './dataSources/ActionCreators';