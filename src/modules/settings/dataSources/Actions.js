const NS = 'api::settings';

export const GENERAL_SETTINGS_GET_USER_SETTINGS = `${NS}@GENERAL_SETTINGS_GET_USER_SETTINGS`;
export const GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE = `${NS}@GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE`;
export const GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION = `${NS}@GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION`;
export const GENERAL_SETTINGS_UPDATING = `${NS}@GENERAL_SETTINGS_UPDATING`;
export const GENERAL_SETTINGS_ERROR = `${NS}@GENERAL_SETTINGS_ERROR`;