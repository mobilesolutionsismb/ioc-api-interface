import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  GENERAL_SETTINGS_GET_USER_SETTINGS,
  GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE,
  GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION,
  GENERAL_SETTINGS_UPDATING,
  GENERAL_SETTINGS_ERROR
} from './Actions';
//TODO tree stuff
import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [GENERAL_SETTINGS_GET_USER_SETTINGS]: {
    generalSettings: (action, state) => {
      let generalSettings = state.generalSettings;
      generalSettings.userSettings = action.userSettings.values;
      return generalSettings;
    },
    generalSettingsUpdating: false
  },
  [GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE]: {
    generalSettings: (action, state) => {
      let generalSettings = state.generalSettings;
      generalSettings.definitionGroupTreeSettings = action.definitionGroupTreeSettings;
      return generalSettings;
    },
    generalSettingsUpdating: false
  },
  [GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION]: {
    generalSettings: (action, state) => {
      let generalSettings = state.generalSettings;
      generalSettings.groupSettingDefinition = action.groupSettingDefinition;
      return generalSettings;
    },
    generalSettingsUpdating: false
  },
  [GENERAL_SETTINGS_UPDATING]: {
    generalSettingsUpdating: true
  },
  [GENERAL_SETTINGS_ERROR]: {
    generalSettingsUpdating: false
  }
};

export default reduceWith(mutator, State);
