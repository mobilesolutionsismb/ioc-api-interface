import {
    GENERAL_SETTINGS_GET_USER_SETTINGS,
    GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE,
    GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION,
    GENERAL_SETTINGS_UPDATING,
    GENERAL_SETTINGS_ERROR
} from './Actions';

import { apiCallActionCreatorFactory } from '../../CommonActionCreators';
import { API } from '../../../api';
const api = API.getInstance();

export function getAllUserSettings(onLoadStart = null, onLoadEnd = null) {
    return async (dispatch, getState) => {
        dispatch({
            type: GENERAL_SETTINGS_UPDATING
        });
        try {
            const res = await apiCallActionCreatorFactory(
                onLoadStart,
                onLoadEnd,
                'generalSettings',
                'getAllSettings',
                '_general_setttings_get_user_settings_failure',
                (data) => {
                    const userSettings = data.result;
                    return Promise.resolve({
                        type: GENERAL_SETTINGS_GET_USER_SETTINGS,
                        userSettings
                    });
                },
                true
            )(dispatch, getState);

            return res;
        } catch (err) {
            dispatch({
                type: GENERAL_SETTINGS_ERROR
            });
            throw err;
        }
    };
}

export function getDefinitionGroupTreeSettings(onLoadStart = null, onLoadEnd = null) {
    return async (dispatch, getState) => {
        dispatch({
            type: GENERAL_SETTINGS_UPDATING
        });
        try {
            const res = await apiCallActionCreatorFactory(
                onLoadStart,
                onLoadEnd,
                'generalSettings',
                'getDefinitionGroupTree',
                '_general_setttings_get_definition_group_settings_failure',
                (data) => {
                    const definitionGroupTreeSettings = data.result;
                    return Promise.resolve({
                        type: GENERAL_SETTINGS_GET_DEFINITION_GROUP_TREE,
                        definitionGroupTreeSettings
                    });
                },
                true
            )(dispatch, getState);

            return res;
        } catch (err) {
            dispatch({
                type: GENERAL_SETTINGS_ERROR
            });
            throw err;
        }
    };
}

export function getSettingDefinitionsByGroup(onLoadStart = null, onLoadEnd = null) {
    return async (dispatch, getState) => {
        dispatch({
            type: GENERAL_SETTINGS_UPDATING
        });
        try {
            const res = await apiCallActionCreatorFactory(
                onLoadStart,
                onLoadEnd,
                'generalSettings',
                'getSettingDefinitionsByGroup',
                '_general_setttings_get_setting_definition_by_group_failure',
                (data) => {
                    const groupSettingDefinition = data.result;
                    return Promise.resolve({
                        type: GENERAL_SETTINGS_GET_GROUP_SETTING_DEFINITION,
                        groupSettingDefinition
                    });
                },
                true
            )(dispatch, getState);

            return res;
        } catch (err) {
            dispatch({
                type: GENERAL_SETTINGS_ERROR
            });
            throw err;
        }
    };
}

export function updateUserSettings(settings, onLoadStart = null, onLoadEnd = null) {
    return async (dispatch, getState) => {
        dispatch({
            type: GENERAL_SETTINGS_UPDATING
        });
        try {
            const response = await apiCallActionCreatorFactory(
                onLoadStart,
                onLoadEnd,
                'generalSettings',
                'updateSettings',
                '_general_setttings_update_failure',
                _updateUserSettingList(),
                true,
                settings
            )(dispatch, getState);
            return response;
        }
        catch (err) {
            dispatch({
                type: GENERAL_SETTINGS_ERROR
            });
            console.warn('Update report Request Error', err);
            throw err;
        }
    };
}

function _updateUserSettingList() {
    return async () => {
        const response = await api.generalSettings.getAllSettings('_general_setttings_get_user_settings_failure');
        const userSettings = response.result;
        return {
            type: GENERAL_SETTINGS_GET_USER_SETTINGS,
            userSettings
        };
    };
}