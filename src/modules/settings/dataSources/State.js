const state = {
    // USER SETTINGS
    generalSettings: {
        userSettings: {},
        definitionGroupTreeSettings: {},
        groupSettingDefinition: {}
    },
    // Set true when waiting for server response
    generalSettingsUpdating: false
};

export default state;