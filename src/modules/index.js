import { default as api_agentLocations } from './agentLocations';
import { default as api_authentication } from './authentication';
import { default as api_categories } from './categories';
import { default as api_emergencyCommunications } from './emergencyCommunications';
import { default as api_emergencyEvents } from './emergencyEvents';
import { default as api_enums } from './enums';
import { default as api_gamification } from './gamification';
// import { default as api_ireactFeatures } from './ireactFeatures';
import { default as api_liveParams } from './liveParams';
import { default as api_layers } from './layers';
import { default as api_mapRequests } from './mapRequests';
import { default as api_missions } from './missions';
import { default as api_mobileApp } from './mobileApp';
import { default as api_organizationUnits } from './organizationUnits';
import { default as api_reports } from './reports';
import { default as api_roles } from './roles';
import { default as api_settings } from './settings';
import { default as api_tipsAndQuizzes } from './tipsAndQuizzes';
import { default as api_tweetFeedback } from './tweetFeedback';

import { default as api_mobileApp_tweets } from './mobileAppTweets';

// All modules  // TO BE REMOVED ASAP
export const allModules = {
  api_agentLocations,
  api_authentication,
  api_categories,
  api_emergencyCommunications,
  api_emergencyEvents,
  api_enums,
  api_gamification,
  // api_ireactFeatures,
  api_liveParams,
  api_layers,
  api_mapRequests,
  api_missions,
  api_mobileApp,
  api_organizationUnits,
  api_reports,
  api_roles,
  api_settings,
  api_tipsAndQuizzes,
  api_tweetFeedback
};

// Frontend only modules
export const frontendModules = {
  api_agentLocations,
  api_authentication,
  api_categories,
  api_emergencyCommunications,
  api_emergencyEvents,
  api_enums,
  // api_ireactFeatures,
  api_liveParams,
  api_layers,
  api_mapRequests,
  api_missions,
  api_organizationUnits,
  api_reports,
  api_roles,
  api_settings,
  api_tweetFeedback
};

// Mobile app only modules
export const mobileAppModules = {
  api_agentLocations,
  api_authentication,
  // api_categories,
  api_emergencyCommunications,
  api_emergencyEvents,
  api_enums,
  api_gamification,
  // api_ireactFeatures,
  api_liveParams,
  api_layers,
  api_mapRequests,
  api_missions,
  api_mobileApp,
  api_organizationUnits,
  api_reports,
  // api_roles,
  // api_settings,
  api_tipsAndQuizzes,
  api_mobileApp_tweets,
  api_tweetFeedback
};

export { apiCallActionCreatorFactory, dispatchIfAction } from './CommonActionCreators';

export { _refreshData } from './_refreshData';

export { _updateReportsInner } from './reports';

export { _updateEmCommsInner } from './emergencyCommunications';

export { _updateMissionsInner } from './missions';

export { _updateAgentLocationsInner } from './agentLocations';

export { _updateMapRequestsInner } from './mapRequests';

export { _updateTweetsInner } from './mobileAppTweets';

export {
  setLiveAreaOfInterestFromUserPosition,
  setLiveTimeWindowExtent,
  setLiveTimeWindowWithDates
} from './liveParams';
