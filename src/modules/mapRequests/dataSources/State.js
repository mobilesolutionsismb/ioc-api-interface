const state = {
  mapRequestFeaturesURL: null, // blob url
  mapRequestFeaturesStats: null,

  // (Point+Polygon) features with itemType = mapRequest
  mapRequests: [],
  mapRequestPolygons: [],
  // Last map request api call timestamp
  mapRequestsLastUpdated: 0,
  // Set true when waiting for server response
  mapRequestsUpdating: false
};

export default state;
