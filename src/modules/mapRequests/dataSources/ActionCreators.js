import { MAP_REQ_GETLIST_SUCCESS, MAP_REQ_GETLIST_UPDATING, MAP_REQ_GETLIST_FAIL } from './Actions';

import { dispatchIfAction, attemptRelogin } from '../../CommonActionCreators';

import { API } from '../../../api';
import { logSevereWarning } from '../../../utils/log';

const itemType = 'mapRequest';

export function isMapRequest(feature) {
  return feature && feature.properties.itemType === itemType;
}

const api = API.getInstance();

function _mapRequestsUpdating() {
  return {
    type: MAP_REQ_GETLIST_UPDATING
  };
}

function _mapRequestsUpdated(featuresURL, stats) {
  return {
    type: MAP_REQ_GETLIST_SUCCESS,
    featuresURL,
    stats
  };
}

function _mapRequestsUpdateFailure() {
  return {
    type: MAP_REQ_GETLIST_FAIL
  };
}

function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_mapRequestsUpdateFailure());
  } else {
    const { featuresURL, ...stats } = data;
    api.reduxStore.dispatch(_mapRequestsUpdated(featuresURL, stats));
  }
}

api.registerHandlerForItemType(itemType, _itemTypeHandler);

/**
 * Handle API Call
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {boolean} forceReload
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _updateMapRequestsInner(
  dispatch,
  getState,
  forceReload,
  onLoadStart,
  onLoadEnd
) {
  try {
    dispatch(_mapRequestsUpdating());

    const state = getState();
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI) {
      dispatchIfAction(dispatch, onLoadEnd);
      return [];
    }

    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    const dateEnd = timeWindow.dateEnd;
    const lastUpdated = forceReload === true ? null : state.api_mapRequests.mapRequestsLastUpdated;

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      itemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated
    );

    return []; // for backward comp
  } catch (err) {
    dispatch(_mapRequestsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `Cannot update ${itemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

/**
 * Action creator for reloading reports
 * @param {Boolean} forceReload - if false, differential update (default)
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function updateMapRequests(forceReload = false, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateMapRequestsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}
