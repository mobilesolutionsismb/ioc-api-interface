import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { MAP_REQ_GETLIST_SUCCESS, MAP_REQ_GETLIST_UPDATING, MAP_REQ_GETLIST_FAIL } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [MAP_REQ_GETLIST_SUCCESS]: {
    mapRequestFeaturesURL: action => action.featuresURL,
    mapRequestFeaturesStats: action => action.stats,
    mapRequests: action => action.items,
    mapRequestPolygons: action => action.itemAOIs,
    mapRequestsLastUpdated: () => Date.now(),
    mapRequestsUpdating: false
  },
  [MAP_REQ_GETLIST_UPDATING]: {
    mapRequestsUpdating: true
  },
  [MAP_REQ_GETLIST_FAIL]: {
    mapRequestsUpdating: false
  }
};

export default reduceWith(mutator, State);
