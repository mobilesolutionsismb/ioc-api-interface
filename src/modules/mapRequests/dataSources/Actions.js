const NS = 'api::map_requests';

// Fetch agents on field
export const MOUSEENTER_MAP_REQ = `${NS}@MOUSEENTER_MAP_REQ`;
export const MOUSELEAVE_MAP_REQ = `${NS}@MOUSELEAVE_MAP_REQ`;
export const MAP_REQ_GETLIST_SUCCESS = `${NS}@MAP_REQ_GETLIST_SUCCESS`;
export const MAP_REQ_GETLIST_UPDATING = `${NS}@MAP_REQ_GETLIST_UPDATING`;
export const MAP_REQ_GETLIST_FAIL = `${NS}@MAP_REQ_GETLIST_FAIL`;
