import Reducer from './dataSources/Reducer';
export default Reducer;

export {
    _updateEmCommsInner, isAoW, isEmComm, isRR,
    updateEmergencyCommunications,
    createEmergencyCommunication, updateEmergencyCommunication,
    mouseEnterEmergencyCommunication, mouseLeaveEmergencyCommunication,
    createReportRequest, updateReportRequest,
    selectEmergencyCommunication, deselectEmergencyCommunication
} from './dataSources/ActionCreators';