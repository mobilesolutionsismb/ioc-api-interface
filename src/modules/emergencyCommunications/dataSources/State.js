const state = {
    emergencyCommunicationFeaturesURL: null, // blob url
    emergencyCommunicationFeaturesStats: null,

    emergencyCommunications: [],
    // (Polygon/MultiPolygon) features with itemType = missionAOI
    emergencyCommunicationPolygons: [],
    // Last emergencyCommunications api call timestamp
    emergencyCommunicationsLastUpdated: 0,
    // Set true when waiting for server response
    emergencyCommunicationsUpdating: false
};

export default state;