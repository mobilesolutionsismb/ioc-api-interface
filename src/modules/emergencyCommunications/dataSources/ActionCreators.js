import {
  EMERGENCY_COMMUNICATIONS_GETLIST_SUCCESS,
  EMERGENCY_COMMUNICATIONS_GETLIST_UPDATING,
  EMERGENCY_COMMUNICATIONS_GETLIST_FAIL
} from './Actions';
import {
  dispatchIfAction,
  apiCallActionCreatorFactory,
  attemptRelogin
} from '../../CommonActionCreators';
import { API } from '../../../api';

import { logSevereWarning } from '../../../utils/log';

const emCommItemType = 'emergencyCommunication';
const reportReqItemType = 'reportRequest';

const api = API.getInstance();

// let _aoiWarningIssued = false; //Temporary until we switch to new AOI format and service

export function isEmComm(feature) {
  return (
    feature &&
    (feature.properties.itemType === emCommItemType ||
      feature.properties.itemType === reportReqItemType)
  );
}

export function isRR(feature) {
  return feature && feature.properties.itemType === reportReqItemType;
}

export function isAoW(feature) {
  return feature && feature.properties.itemType === emCommItemType;
}

function _emcommsUpdating() {
  return {
    type: EMERGENCY_COMMUNICATIONS_GETLIST_UPDATING
  };
}

function _emcommsUpdated(featuresURL, stats) {
  return {
    type: EMERGENCY_COMMUNICATIONS_GETLIST_SUCCESS,
    featuresURL,
    stats
    // items: []
  };
}

function _emcommsUpdateFailure() {
  return {
    type: EMERGENCY_COMMUNICATIONS_GETLIST_FAIL
  };
}

async function _itemTypeHandler(data, err) {
  if (err) {
    api.reduxStore.dispatch(_emcommsUpdateFailure());
    // const reloginSuccess = await attemptRelogin(err, api.reduxStore.dispatch, api.reduxStore.getState);
    // if (reloginSuccess) {
    //     await _updateEmCommsInner(api.reduxStore.dispatch, api.reduxStore.getState, forceReload);
    // }
    // else {
    //     api.reduxStore.dispatch(_emcommsUpdateFailure());
    // }
  } else {
    const { featuresURL, ...stats } = data;
    api.reduxStore.dispatch(_emcommsUpdated(featuresURL, stats));
  }
}

api.registerHandlerForItemType(emCommItemType, _itemTypeHandler);

/**
 * Handle API Call
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {boolean} forceReload
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _updateEmCommsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd) {
  try {
    dispatch(_emcommsUpdating());
    const state = getState();
    const hasAOI =
      state.api_emergencyEvents.activeEventAOI !== null || state.api_liveParams.liveAOI !== null;
    if (!hasAOI) {
      dispatchIfAction(dispatch, onLoadEnd);
      return [];
    }

    const areaOfInterest = state.api_emergencyEvents.activeEventAOI
      ? state.api_emergencyEvents.activeEventAOI
      : state.api_liveParams.liveAOI;
    const timeWindow = state.api_emergencyEvents.activeEventTW
      ? state.api_emergencyEvents.activeEventTW
      : state.api_liveParams.liveTW;
    const dateStart = timeWindow.dateStart;
    const dateEnd = timeWindow.dateEnd;
    const lastUpdated =
      forceReload === true
        ? null
        : state.api_emergencyCommunications.emergencyCommunicationsLastUpdated;

    await api.requestIreactFeaturesUpdate(
      attemptRelogin,
      emCommItemType,
      dateStart,
      dateEnd,
      areaOfInterest,
      lastUpdated
    );
    return [];
  } catch (err) {
    dispatch(_emcommsUpdateFailure());
    dispatchIfAction(dispatch, onLoadEnd);
    logSevereWarning(
      `(!) Cannot update ${emCommItemType}s:`,
      'color: gold; background-color: orangered',
      err
    );
    throw err;
  }
}

/**
 * Action creator for reloading emergency communications
 * @param {Boolean} forceReload - if false, differential update (default)
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function updateEmergencyCommunications(
  forceReload = false,
  onLoadStart = null,
  onLoadEnd = null
) {
  return async (dispatch, getState) => {
    try {
      const result = await _updateEmCommsInner(
        dispatch,
        getState,
        forceReload,
        onLoadStart,
        onLoadEnd
      );
      return result;
    } catch (err) {
      throw err;
    }
  };
}

/**
 *
 * @param {*} reportRequest
 * @param {*} onLoadStart
 * @param {*} onLoadEnd
 */
export function createReportRequest(reportRequest, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateEmCommsInner(dispatch, getState, false, onLoadStart, onLoadEnd);
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyCommunication',
        'createReportRequest',
        '_emergency_communication_createReportRequest_failure',
        done,
        true,
        reportRequest
      )(dispatch, getState);

      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function createEmergencyCommunication(emComm, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateEmCommsInner(dispatch, getState, false, onLoadStart, onLoadEnd);
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyCommunication',
        'createEmergencyCommunication',
        '_emergency_communication_createEmComm_failure',
        done,
        true,
        emComm
      )(dispatch, getState);

      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function updateReportRequest(reportRequest, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateEmCommsInner(dispatch, getState, false, onLoadStart, onLoadEnd);
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyCommunication',
        'updateReportRequest',
        '_emergency_communication_updateReportRequest_failure',
        done,
        true,
        reportRequest
      )(dispatch, getState);

      return result;
    } catch (err) {
      throw err;
    }
  };
}

export function updateEmergencyCommunication(emComm, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    try {
      const done = async () => {
        const result = await _updateEmCommsInner(dispatch, getState, false, onLoadStart, onLoadEnd);
        return result;
      };

      const result = await apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyCommunication',
        'updateEmergencyCommunication',
        '_emergency_communication_update_failure',
        done,
        true,
        emComm
      )(dispatch, getState);

      return result;
    } catch (err) {
      throw err;
    }
  };
}
