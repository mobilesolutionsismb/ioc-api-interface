import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  EMERGENCY_COMMUNICATIONS_GETLIST_SUCCESS,
  EMERGENCY_COMMUNICATIONS_GETLIST_UPDATING,
  EMERGENCY_COMMUNICATIONS_GETLIST_FAIL,
  TOGGLE_IMAGE_DIALOG_REPORT_REQUEST
  // UPDATE_POINTS_REPORT_REQUEST
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  // [UPDATE_POINTS_REPORT_REQUEST]: {
  //     singlePoints: action => action.singlePoints
  // },
  [TOGGLE_IMAGE_DIALOG_REPORT_REQUEST]: {
    isDialogVisible: action => action.isDialogVisible
  },
  [EMERGENCY_COMMUNICATIONS_GETLIST_SUCCESS]: {
    emergencyCommunicationFeaturesURL: action => action.featuresURL, // blob url
    emergencyCommunicationFeaturesStats: action => action.stats,

    // emergencyCommunications: action => action.items,
    // emergencyCommunicationPolygons: action => action.itemAOIs,
    emergencyCommunicationsLastUpdated: action => action.stats.lastModificationTime,
    emergencyCommunicationsUpdating: false
  },
  [EMERGENCY_COMMUNICATIONS_GETLIST_UPDATING]: {
    emergencyCommunicationsUpdating: true
  },
  [EMERGENCY_COMMUNICATIONS_GETLIST_FAIL]: {
    emergencyCommunicationsUpdating: false
  }
};

export default reduceWith(mutator, State);
