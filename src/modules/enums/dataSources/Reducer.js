import { reduceWith } from 'redux-reduce-with';
import State from './State';

import { ENUM_GET_SUCCESS, ENUM_GETLIST_SUCCESS } from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  [ENUM_GET_SUCCESS]: {
    enums: (action, state) => ({ ...state.enums, ...action.enum })
  },
  [ENUM_GETLIST_SUCCESS]: {
    enums: action => action.enums
  }
};

export default reduceWith(mutator, State);
