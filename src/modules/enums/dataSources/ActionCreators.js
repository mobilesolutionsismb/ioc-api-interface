import {
	ENUM_GET_SUCCESS,
	ENUM_GETLIST_SUCCESS
} from './Actions';

import { apiCallActionCreatorFactory } from '../../CommonActionCreators';

export function getEnumByName(enumName, onLoadStart = null, onLoadEnd = null) {
	return apiCallActionCreatorFactory(
		onLoadStart,
		onLoadEnd,
		'enums',
		'get',
		'_enum_get_failure',
		(data) => {
			const enumResult = data.result;
			const _enum = {
				[enumName]: enumResult
			};
			return Promise.resolve({
				type: ENUM_GET_SUCCESS,
				enum: _enum
			});
		},
		false,
		enumName
	);
}

export function getEnums(onLoadStart = null, onLoadEnd = null) {
	return apiCallActionCreatorFactory(
		onLoadStart,
		onLoadEnd,
		'enums',
		'getEnums',
		'_enum_getList_failure',
		(data) => {
			const enums = data.result;
			return Promise.resolve({
				type: ENUM_GETLIST_SUCCESS,
				enums
			});
		},
		false
	);
}