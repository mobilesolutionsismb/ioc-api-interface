const NS = 'api::enums';

export const ENUM_GET_SUCCESS = `${NS}@ENUM_GET_SUCCESS`;
export const ENUM_GETLIST_SUCCESS = `${NS}@ENUM_GETLIST_SUCCESS`;