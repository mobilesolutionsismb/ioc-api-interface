import { reduceWith } from 'redux-reduce-with';
import State from './State';

import {
  MOUSEENTER_EVENT,
  MOUSELEAVE_EVENT,
  SELECT_EVENT,
  DESELECT_EVENT,
  ACTIVATE_EVENT,
  DEACTIVATE_EVENT,
  EMERGENCY_EVENT_REQUEST,
  EMERGENCY_EVENT_FAILURE,
  EMERGENCY_EVENT_GETLIST_SUCCESS
  // Layers
  // EMERGENCY_EVENT_GETLAYERS_SUCCESS,
} from './Actions';

import { RESET_ALL } from '../../CommonActions';

const mutator = {
  [RESET_ALL]: {
    ...State
  },
  // Event Hovering
  [MOUSEENTER_EVENT]: {
    hoveredEvent: action => action.feature,
    hoveredEventBounds: action => action.bounds
  },
  [MOUSELEAVE_EVENT]: {
    hoveredEvent: null,
    hoveredEventBounds: null
  },
  // Event Selection
  [SELECT_EVENT]: {
    selectedEvent: action => action.feature,
    selectedEventBounds: action => action.bounds
  },
  [DESELECT_EVENT]: {
    selectedEvent: null,
    selectedEventBounds: null
  },
  // Event Activation
  [ACTIVATE_EVENT]: {
    activeEvent: action => action.feature,
    activeEventBounds: action => action.bounds,
    activeEventTW: action => action.eventTW,
    activeEventAOI: action => action.eventAOI
  },
  [DEACTIVATE_EVENT]: {
    activeEvent: null,
    activeEventBounds: null,
    activeEventTW: null,
    activeEventAOI: null
  },

  [EMERGENCY_EVENT_REQUEST]: {
    emergencyEvents: (action, state) => {
      const stateSkip = state.emergencyEvents.skip;
      return {
        ...state.emergencyEvents,
        isFetching: true,
        skip: action.skip,
        limit: action.limit,
        hasMore: true,
        errorMessage: '',
        items: stateSkip >= action.skip ? [] : [...state.emergencyEvents.items],
        itemPolygons: stateSkip >= action.skip ? [] : [...state.emergencyEvents.itemPolygons]
      };
    }
  },
  [EMERGENCY_EVENT_FAILURE]: {
    emergencyEvents: (action, state) => ({
      ...state.emergencyEvents,
      isFetching: false,
      errorMessage: action.errorMessage,
      hasMore: false
    })
  },
  [EMERGENCY_EVENT_GETLIST_SUCCESS]: {
    emergencyEventsSorting: action => action.sorting,
    emergencyEventsStatusFilter: action => action.status,
    emergencyEvents: (action, state) => {
      const nextItems = [...state.emergencyEvents.items, ...action.items];
      const nextItemPolygons = [...state.emergencyEvents.itemPolygons, ...action.itemPolygons];
      return {
        ...state.emergencyEvents,
        isFetching: false,
        items: nextItems,
        itemPolygons: nextItemPolygons,
        hasMore: nextItems.length < action.totalCount,
        totalCount: action.totalCount,
        lastUpdated: Date.now()
      };
    }
  }
};

export default reduceWith(mutator, State);
