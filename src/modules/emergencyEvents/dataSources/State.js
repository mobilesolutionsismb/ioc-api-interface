const state = {
    emergencyEventsSorting: 'start DESC',
    emergencyEventsStatusFilter: 'all',
    // Events and selected event settings
    // -- Hovering
    hoveredEvent: null,
    hoveredEventBounds: null, //bbox
    // -- Selection
    selectedEvent: null,
    selectedEventBounds: null, //bbox
    // -- Active Event
    activeEvent: null,
    activeEventBounds: null, //bbox
    // Event Settings
    activeEventAOI: null, // AOI for queries
    activeEventTW: null, // TW for queries
    // --> Emergency Events PAGED list
    emergencyEvents: {
      isFetching: false,
      skip: 0,
      limit: 10,
      hasMore: true,
      items: [], // Point Features
      itemPolygons: [], // Polygon Features
      totalCount: 0,
      lastUpdated: 0
    }
  };
  
  export default state;
  