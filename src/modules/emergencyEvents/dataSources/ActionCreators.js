import {
  EMERGENCY_EVENT_REQUEST,
  EMERGENCY_EVENT_FAILURE,
  EMERGENCY_EVENT_GETLIST_SUCCESS,
  SELECT_EVENT,
  DESELECT_EVENT,
  ACTIVATE_EVENT,
  DEACTIVATE_EVENT,
  MOUSEENTER_EVENT,
  MOUSELEAVE_EVENT
} from './Actions';

import nop from 'nop';
import { apiCallActionCreatorFactory } from '../../CommonActionCreators';
import { feature } from '@turf/helpers';
import centroid from '@turf/centroid';
import turfBuffer from '@turf/buffer';

import wellknown from 'wellknown';
import { _refreshData } from '../../_refreshData';
import { makeBoundsObject, TimeWindow, padBoundingBox } from '../../../api';
import { logWarning, logSevereWarning, logMain } from '../../../utils/log';
// ------------------------------------------------------------------------------------------------------------------------------
// EMERGENCY EVENTS MANAGEMENT
// ------------------------------------------------------------------------------------------------------------------------------
const _emergencyEventsGetListError = error => ({
  type: EMERGENCY_EVENT_FAILURE,
  errorMessage: error
});

const AVAILABLE_SORTERS_MAP = {
  _date_time_asc: 'start ASC',
  _date_time_desc: 'start DESC'
  // TODO add others if needed
};

export const AVAILABLE_SORTERS = Object.keys(AVAILABLE_SORTERS_MAP);
export const AVAILABLE_SORTERS_VALUES = Object.values(AVAILABLE_SORTERS_MAP);

const AVAILABLE_STATUS_FILTERS_MAP = {
  _is_status_all: 'all',
  _is_status_ongoing: 'ongoing',
  _is_status_closed: 'closed'
  // TODO add others if needed
};

export const AVAILABLE_STATUS_FILTERS = Object.keys(AVAILABLE_STATUS_FILTERS_MAP);
export const AVAILABLE_STATUS_FILTERS_VALUES = Object.values(AVAILABLE_STATUS_FILTERS_MAP);
const itemType = 'emergencyEvent';

async function _deactivateEventInner(dispatch, getState, onLoadStart, onLoadEnd) {
  dispatch({ type: DEACTIVATE_EVENT });
  try {
    await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
  } catch (e) {
    throw e;
  }
}

const DEFAULT_RADIUS_FOR_POINT_AOI = 15;
const DEFAULT_UNIT_FOR_POINT_AOI = 'kilometres';
// Some events don't have a Polygon for AOI - we return a circle with 15km radius
function _getCircularAOI(point) {
  return turfBuffer(point, DEFAULT_RADIUS_FOR_POINT_AOI, { units: DEFAULT_UNIT_FOR_POINT_AOI })
    .geometry;
}

function _reshapeEmergencyEventItems(itemsFromServer) {
  let result = {
    points: [],
    polygons: []
  };
  return itemsFromServer.reduce((result, nextEventFromServer /* , index */) => {
    try {
      const {
        id,
        areaOfInterest,
        creationTime,
        lastModificationTime,
        coordinates,
        ...properties
      } = nextEventFromServer;
      const lastModified = new Date(lastModificationTime ? lastModificationTime : creationTime);
      const featureProperties = { id, itemType, lastModified, ...properties };
      const aoiGeometry = wellknown.parse(areaOfInterest);
      const _aoiGeometry =
        aoiGeometry.type === 'Point' ? _getCircularAOI(aoiGeometry) : aoiGeometry;
      const emEvtPolygon = feature(_aoiGeometry, { id, itemType: `${itemType}AOI` });
      let emEvtPoint;
      if (typeof coordinates === 'string' && location.length) {
        emEvtPoint = feature(wellknown.parse(coordinates), featureProperties);
      } else {
        emEvtPoint = centroid(aoiGeometry, { properties: featureProperties });
      }
      result.points.push(emEvtPoint);
      result.polygons.push(emEvtPolygon);
    } catch (err) {
      logSevereWarning(`Cannot reshape Emergency Event with id = ${nextEventFromServer.id}:`, err);
      // console.log(`%c(!) Cannot reshape Emergency Event with id = ${nextEventFromServer.id}:`, 'color: gold; background-color: orangered', err);
    }

    return result;
  }, result);
}

const _emergencyEventsGetListSuccess = (items, totalCount, sorting, status) => {
  const { points, polygons } = _reshapeEmergencyEventItems(items);

  return {
    type: EMERGENCY_EVENT_GETLIST_SUCCESS,
    items: points,
    itemPolygons: polygons,
    // You can either make an additional request to check whether there are
    // still items left to fetch or return a
    // hasMore property with a response from the api to instantly get info about it.
    sorting,
    status,
    totalCount
  };
};

const _requestEmergencyEventsAction = (skip, limit) => ({
  type: EMERGENCY_EVENT_REQUEST,
  skip: skip + limit,
  limit
});

async function _requestEmergencyEvents(dispatch, skip, limit) {
  dispatch(_requestEmergencyEventsAction(skip, limit));
}

async function _getEventsListInner(
  dispatch,
  getState,
  skip,
  limit,
  sorting,
  status,
  onLoadStart,
  onLoadEnd
) {
  let state = getState();
  if (state.api_emergencyEvents.emergencyEvents.isFetching) {
    return;
  }
  await _requestEmergencyEvents(dispatch, skip, limit);

  const onEventListUpdateSuccess = data => {
    const { items, totalCount } = data.result;
    return _emergencyEventsGetListSuccess(items, totalCount, sorting, status);
  };

  try {
    state = getState();
    const eventFetchingTW = state.api_liveParams.eventFetchingTW;
    const dateStart = eventFetchingTW.dateStart;
    const dateEnd = eventFetchingTW.dateEnd;
    const eventSelectionAOI = state.api_liveParams.liveAOI; // Yes, same as the AOI for reports
    const res = await apiCallActionCreatorFactory(
      onLoadStart,
      onLoadEnd,
      'emergencyEvent',
      'getAll',
      '_emergency_event_getList_failure',
      onEventListUpdateSuccess,
      true,
      eventSelectionAOI.wktString,
      dateStart,
      dateEnd,
      skip,
      limit,
      sorting,
      status
    )(dispatch, getState);
    logMain('_getEventsListInner', res);
    // console.debug('_getEventsListInner', res);
    // check if active event exists after update
    if (state.api_emergencyEvents.activeEventId !== null) {
      const eventId = state.api_emergencyEvents.activeEventId;
      const newState = getState().api_emergencyEvents;
      const _event = newState.emergencyEvents.items.find(event => event.id === eventId);
      if (typeof _event === 'undefined') {
        await _deactivateEventInner(dispatch, getState, onLoadStart, onLoadEnd);
      }
    }
  } catch (err) {
    logWarning('Cannot load events list', err);
    dispatch(_emergencyEventsGetListError(err));
    throw err;
  }
}

/**
 *
 * @param {Number} skip (SkipCount) - page index
 * @param {Number} limit (MaxResultCount) - page size
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export function getEmergencyEvents(skip = 0, limit = 10, onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    const state = getState().api_emergencyEvents;
    const sorting = state.emergencyEventsSorting;
    const status = state.emergencyEventsStatusFilter;

    return await _getEventsListInner(
      dispatch,
      getState,
      skip,
      limit,
      sorting,
      status,
      onLoadStart,
      onLoadEnd
    );
  };
}

/**
 *
 * @param {String} sorter
 */
export function setEventSorter(sorter, onLoadStart, onLoadEnd) {
  return async (dispatch, getState) => {
    const _sorter = AVAILABLE_SORTERS_MAP[sorter];
    if (_sorter) {
      const state = getState().api_emergencyEvents;
      const emergencyEventsSorting = state.emergencyEventsSorting;
      if (emergencyEventsSorting !== _sorter) {
        // Perform full update
        const status = state.emergencyEventsStatusFilter;
        await _getEventsListInner(
          dispatch,
          getState,
          0,
          10,
          _sorter,
          status,
          onLoadStart,
          onLoadEnd
        );
      }
    }
  };
}

/**
 *
 * @param {String} filter
 * @param {String} type - on which property - only status supported for now
 */
export function setEventFilter(filter, type = 'status', onLoadStart, onLoadEnd) {
  return async (dispatch, getState) => {
    if (type === 'status') {
      const _filter = AVAILABLE_STATUS_FILTERS_MAP[filter];
      if (_filter) {
        const state = getState().api_emergencyEvents;
        const emergencyEventsStatusFilter = state.emergencyEventsStatusFilter;
        if (emergencyEventsStatusFilter !== _filter) {
          // Perform full update
          const sorting = state.emergencyEventsSorting;
          await _getEventsListInner(
            dispatch,
            getState,
            0,
            10,
            sorting,
            _filter,
            onLoadStart,
            onLoadEnd
          );
        }
      }
    } else {
      logWarning(`Unsupported (for now?) type: ${type}`);
    }
  };
}

/**
 * Action creator
 * Dispatch action for emergency event creation
 *
 * @param {Object} emergencyEvent emergencyEvent to create
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 */
export function createEmergencyEvent(emergencyEvent, onLoadStart = null, onLoadEnd = null) {
  return (dispatch, getState) => {
    try {
      const onApiCallSuccess = () => {
        // on load start and end handled by factory
        return _getEventsListInner(dispatch, getState, 0, 10, null, null, onLoadStart, onLoadEnd);
      };

      return apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyEvent',
        'create',
        '_emergencyEvent_create_failure',
        onApiCallSuccess,
        true,
        emergencyEvent
        // TODO understand if more arguments are needed, such as Status
      )(dispatch, getState);
    } catch (e) {
      throw e;
    }
  };
}

/**
 * Action creator
 * Dispatch action for emergency event update
 *
 * @param {Object} emergencyEvent emergencyEvent to update
 * @param {Function|Object} onLoadStart [optional callback/action to dispatch when ajax call starts]
 * @param {Function|Object} onLoadEnd [optional callback/action to dispatch when ajax call ends]
 * N.B: all the field of the emergency event must be specified
 */
export function updateEmergencyEvent(emergencyEvent, onLoadStart = null, onLoadEnd = null) {
  return (dispatch, getState) => {
    try {
      const onApiCallSuccess = () => {
        // on load start and end handled by factory
        return _getEventsListInner(dispatch, getState, 0, 10, null, null, onLoadStart, onLoadEnd);
      };

      return apiCallActionCreatorFactory(
        onLoadStart,
        onLoadEnd,
        'emergencyEvent',
        'update',
        '_emergencyEvent_update_failure',
        onApiCallSuccess,
        true,
        emergencyEvent
      )(dispatch, getState);
    } catch (e) {
      throw e;
    }
  };
}

function _findEvent(eventId, getState, onFound = nop, onNotFound = nop) {
  const apiState = getState().api_emergencyEvents;
  const emergencyEvents = apiState.emergencyEvents;
  const { items, itemPolygons } = emergencyEvents;
  if (items.length > 0) {
    const featureIndex = items.findIndex(f => f.properties.id === eventId);
    if (featureIndex > -1) {
      const feature = items[featureIndex];
      const featureAOI = itemPolygons[featureIndex];
      const bounds = makeBoundsObject(featureAOI.geometry);
      onFound(feature, bounds);
      return feature;
    } else {
      onNotFound();
      return null;
    }
  } else {
    return null;
  }
}

export function mouseEnterEvent(eventId) {
  return (dispatch, getState) => {
    const onFound = (feature, bounds) => {
      dispatch({
        type: MOUSEENTER_EVENT,
        feature,
        bounds
      });
    };
    const onNotFound = () => dispatch({ type: MOUSELEAVE_EVENT });
    return _findEvent(eventId, getState, onFound, onNotFound);
  };
}

export function mouseLeaveEvent() {
  return {
    type: MOUSELEAVE_EVENT
  };
}

export function selectEvent(eventId) {
  return (dispatch, getState) => {
    const onFound = (feature, bounds) => {
      dispatch({
        type: SELECT_EVENT,
        feature,
        bounds
      });
    };
    const onNotFound = () => dispatch({ type: DESELECT_EVENT });
    return _findEvent(eventId, getState, onFound, onNotFound);
  };
}

export function deselectEvent() {
  return {
    type: DESELECT_EVENT
  };
}

export function activateEvent(eventId, onLoadStart = null, onLoadEnd = null) {
  return (dispatch, getState) => {
    const onFound = async (feature, bounds) => {
      const eventAOI = padBoundingBox(bounds.feature, 200, '%');
      const _etw = TimeWindow.fromEvent(event);
      const eventTW = _etw.toPojo();

      try {
        // Deselect everything
        dispatch({
          type: MOUSELEAVE_EVENT
        });
        dispatch({
          type: DESELECT_EVENT
        });
        dispatch({
          type: ACTIVATE_EVENT,
          feature,
          bounds,
          eventTW,
          eventAOI
        });
        await _refreshData(dispatch, getState, onLoadStart, onLoadEnd);
      } catch (err) {
        logWarning('Deactivating event because of error', err);
        await _deactivateEventInner(dispatch, getState, onLoadStart, onLoadEnd);
        throw err;
      }
    };

    const onNotFound = async () =>
      await _deactivateEventInner(dispatch, getState, onLoadStart, onLoadEnd);

    return _findEvent(eventId, getState, onFound, onNotFound);
  };
}

export function deactivateEvent(onLoadStart = null, onLoadEnd = null) {
  return async (dispatch, getState) => {
    return await _deactivateEventInner(dispatch, getState, onLoadStart, onLoadEnd);
  };
}
