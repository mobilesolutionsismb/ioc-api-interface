import { _updateReportsInner } from './reports';
import { _updateEmCommsInner } from './emergencyCommunications';
import { _updateMissionsInner } from './missions';
import { _updateAgentLocationsInner } from './agentLocations';
import { _updateMapRequestsInner } from './mapRequests';
import { _updateTweetsInner } from './mobileAppTweets';

import { _getLayersInner } from './layers';

import { API } from '../api';
const api = API.getInstance();

/**
 * Refresh all data which depend on event selection time window and aoi
 * @param {Function} dispatch
 * @param {Function} getState
 * @param {Function} onLoadStart
 * @param {Function} onLoadEnd
 */
export async function _refreshData(
  dispatch,
  getState,
  onLoadStart,
  onLoadEnd,
  forceReload = true,
  getLayers = true
) {
  // Reload reports and other data
  try {
    const state = getState();
    if (!state.api_authentication.logged_in) {
      return Promise.resolve();
    }
    const userType = state.api_authentication.user.userType;

    const reloadReports = _updateReportsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
    const reloadEmComms = _updateEmCommsInner(dispatch, getState, true, onLoadStart, onLoadEnd);
    // Authorituy/PRO only
    const reloadMissions =
      userType === 'citizen'
        ? Promise.resolve([])
        : _updateMissionsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd);
    const reloadAgentLocations =
      userType === 'citizen'
        ? Promise.resolve([])
        : _updateAgentLocationsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd);
    // Frontend-only
    const reloadMapRequests =
      api.presets === 'mobile-app'
        ? Promise.resolve([])
        : _updateMapRequestsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd);

    const reloadTweetsApp =
      api.presets === 'mobile-app'
        ? _updateTweetsInner(dispatch, getState, forceReload, onLoadStart, onLoadEnd)
        : Promise.resolve([]);

    // At least for now we don't handle layers in mobile app
    const reloadLayers = getLayers
      ? _getLayersInner(dispatch, getState, onLoadStart, onLoadEnd)
      : Promise.resolve([]);

    const done = await Promise.all([
      // Features
      reloadReports,
      reloadEmComms,
      reloadMissions,
      reloadAgentLocations,
      reloadMapRequests,
      reloadTweetsApp,
      // Non features
      reloadLayers
    ]);
    return done;
  } catch (e) {
    throw e;
  }
}
