# I-REACT Open Core API Module

This module contains JavaScript API implementation used by both [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) front-end [web-tool](https://bitbucket.org/mobilesolutionsismb/ioc-web-tool) and mobile [app](https://bitbucket.org/mobilesolutionsismb/ioc-cordova-app).  
It is a required dependency for both of them.  
It includes several adaptations to make it work with react and redux.

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Important

You must have access to an instance of the **I-REACT Open Core** backend deployed and reachable at a `https` URL.
Please download and deploy the [**I-REACT Open Core Backend**](https://bitbucket.org/mobilesolutionsismb/ioc-backend) from [this link](https://bitbucket.org/mobilesolutionsismb/ioc-backend/raw/997c9d7bf9b5119e113dbd5df132364ef2d71d7e/release/ioc-backend.zip)

In order to have the Social Analysis API working you must also have access to a version of the social analyisis tool from [CELI](https://www.celi.it/). Please [contact CELI](mailto:info@celi.it) and ask for purchasing their solution.

## Usage

Register the reducer

```javascript
// Point the API to a given backend url that you may set from webpack
// e.g. https://url-where-backend-is-deployed
window.IREACT_BACKEND = IREACT_BACKEND;
import { Reducer as ApiReducer } from 'ioc-api-interface';

...

app.registerReducer('api', ApiReducer);

```

Import the providers

```javascript
import { withLogin } from 'ioc-api-interface';
```

## TEST

The default test (api only) use jest.

`IREACT_BACKEND=<backend_url> USERNAME=<username> PASSWORD=<password> npm run test`

On windows use [cross-env](https://www.npmjs.com/package/cross-env)
